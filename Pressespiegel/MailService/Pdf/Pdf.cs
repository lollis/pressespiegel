﻿using System;
using System.Collections.Generic;
using System.Linq;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System.Diagnostics;
using RssService;
using MailService.Email;
using System.IO;
using PdfSharp.Drawing.Layout;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using RssService.SqlLinq;
using Pressespiegel.DataProvider;
using System.Net;
using HtmlAgilityPack;

namespace MailService.Pdf
{
    class Pdf
    {
        static double A4Width = XUnit.FromCentimeter(21).Point;

        static double A4Height = XUnit.FromCentimeter(29.7).Point;


        internal System.IO.Stream CreatePdf(String docTitle, String userName, String ProfilName, List<RssItem> items)
        {

            Font header = new Font("Times New Roman", 18);
            header.Underline = Underline.Single;
            header.Bold = true;

            Document document = new Document();
            document.Info.Author = "";
            document.Info.Keywords = "";

            // Get the A4 page size
            Unit width, height;
            PageSetup.GetPageSize(PageFormat.A4, out width, out height);

            // Add a section to the document and configure it such that it will be in the centre
            // of the page
            Section section = document.AddSection();
            section.PageSetup.PageHeight = height;
            section.PageSetup.PageWidth = width;
            section.PageSetup.LeftMargin = 20;
            section.PageSetup.RightMargin = 20;
            section.PageSetup.TopMargin = 30;
            section.PageSetup.BottomMargin = 30;

            Paragraph para = section.AddParagraph("Persönlicher Pressespiegel von " + userName + "\nfür das Profil " + ProfilName + "\n");
            para.Format.Alignment = ParagraphAlignment.Center;
            para.Format.Font = header;
            para.Format.SpaceAfter = 6;
            List<string> toDelete;
            Table table = CreateTable(items, width, height, out toDelete);

            document.LastSection.Add(table);

            // Create a renderer
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer();

            // Associate the MigraDoc document with a renderer
            pdfRenderer.Document = document;

            // Layout and render document to PDF
            pdfRenderer.RenderDocument();

            // Save and show the document




            //string file = AppDomain.CurrentDomain.GetData("DataDirectory").ToString() + "test.pdf";
            MemoryStream stream = new MemoryStream();
            // pdfRenderer.PdfDocument.Save(file);
            pdfRenderer.PdfDocument.Save(stream, false);
            //doc.Save(stream, false);
            //    byte[] bytes = stream.ToArray();
            //Process.Start(file);
            try
            {
                foreach (string path in toDelete)
                {
                    File.Delete(path);
                }
            }
            catch (Exception ex)
            {

            }
            return stream;
        }

        private Table CreateTable(List<RssItem> items, Unit width, Unit height, out List<string> imagePaths)
        {
            imagePaths = new List<string>();
            // Create a table so that we can draw the horizontal lines
            Table table = new Table();
            table.Borders.Width = 1; // Default to show borders 1 pixel wide Column


            Column column = table.AddColumn(width / 10 * 3 / 2);
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(width / 10 * 8);
            column.Format.Alignment = ParagraphAlignment.Center;
            /*
                        column = table.AddColumn(width / 10 * 2);
                        column.Format.Alignment = ParagraphAlignment.Center;
            */
            double fontHeight = 18;
            Font font = new Font("Times New Roman", fontHeight);
            Font title = new Font("Times New Roman", 12);
            title.Bold = true;
            Font content = new Font("Tiems New Roman", 10);
            Font link = new Font("Times New Roman", 8);
            link.Color = Colors.Blue;
            Font date = new Font("Tiems New Roman", 6);
            date.Italic = true;
            // Add a row with a single cell for the first line
            Row row = table.AddRow();
            row.Borders.Visible = false;
            row = table.AddRow();
            row.Borders.Visible = false;
            row.Borders.Bottom.Visible = true;


            Cell cell = row.Cells[0];

            cell.Format.Font.Color = Colors.Black;
            cell.Format.Alignment = ParagraphAlignment.Left;
            cell.Format.Font.ApplyFont(font);

            //  cell.AddParagraph("Provider");

            // Add a row with a single cell for the second line
            //  row = table.AddRow();
            cell = row.Cells[1];

            cell.Format.Font.Color = Colors.Black;
            cell.Format.Alignment = ParagraphAlignment.Left;
            cell.Format.Font.ApplyFont(font);
            //cell.Borders.Left.Visible = false;
            //cell.Borders.Right.Visible = false;
            //cell.Borders.Top.Visible = false;

            // cell.AddParagraph("Artikel");

            /*           cell = row.Cells[2];

                       cell.Format.Font.Color = Colors.Black;
                       cell.Format.Alignment = ParagraphAlignment.Center;
                       cell.Format.Font.ApplyFont(font);

                       cell.AddParagraph("Datum");
           */


            Dictionary<string, KeyValuePair<string, string>> provDic = LoadProviderImage();
            foreach (RssItem item in items)
            {
                row = table.AddRow();
                row.Format.SpaceAfter = 6;
                row.Format.SpaceBefore = 6;
                row.Borders.Left.Visible = false;
                row.Borders.Right.Visible = false;
                Cells cells = row.Cells;
                cells[0].Format.Font.Color = Colors.Black;
                cells[0].Format.Alignment = ParagraphAlignment.Left;
                cells[0].Format.Font.ApplyFont(content);

                if (item.ServiceName.Length > 11)
                    cells[0].AddParagraph(item.ServiceName.Remove(11));
                else
                    cells[0].AddParagraph(item.ServiceName);


                //Hier ProviderBild einfügen//

                KeyValuePair<string, string> image;
                provDic.TryGetValue(item.ServiceName, out image);
                //key=image, value=path

                if (!string.IsNullOrEmpty(image.Value))
                {
                    var bytes = Convert.FromBase64String(image.Value);
                    using (var imageFile = new FileStream(image.Key, FileMode.Create))
                    {
                        imageFile.Write(bytes, 0, bytes.Length);
                        imageFile.Flush();
                    }


                    cells[0].AddParagraph().AddImage(image.Key).Width = 50;
                }

                cells[1].Format.Font.Color = Colors.Black;
                cells[1].Format.Alignment = ParagraphAlignment.Left;
                cells[1].Format.Font.ApplyFont(content);

                cells[1].AddParagraph().AddFormattedText(item.Title + "\n ", title);
                //
                cells[1].AddParagraph().AddFormattedText(item.Date + "\n", date);
                //hier artikelbild
                string bildPath = Path.Combine(new string[] { MailServiceProgram.TempFolderPath, "ArticleImage" + Guid.NewGuid() + ".png" }).ToString();
                try
                {
                    WebClient webClient = new WebClient();
                    if (!string.IsNullOrEmpty(item.BildUrl))
                    {
                        webClient.DownloadFile(item.BildUrl, bildPath);
                        cells[1].AddParagraph().AddImage(bildPath).Width = 200;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Fehler beim Bild laden: " + ex);
                }




                //Hier ArtikelBild einfügen//
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(item.Content);
                cells[1].AddParagraph(doc.DocumentNode.InnerText + "\n");

                cells[1].AddParagraph().AddHyperlink(item.Url, HyperlinkType.Url).AddFormattedText(item.Url, link);
                /*  
                   cells[2].Format.Font.Color = Colors.Black;
                   cells[2].Format.Alignment = ParagraphAlignment.Left;
                   cells[2].Format.Font.ApplyFont(content);

                   cells[2].AddParagraph(item.Date.ToString());
            */
                imagePaths.Add(image.Key);
                imagePaths.Add(bildPath);
            }
            return table;

        }
        internal Dictionary<string, KeyValuePair<string, string>> LoadProviderImage()
        {
            UserProfileViewDataContext db = new UserProfileViewDataContext(MainWindow.ConnectionString);
            Dictionary<string, KeyValuePair<string, string>> providerDic = new Dictionary<string, KeyValuePair<string, string>>();
            foreach (RssService.SqlLinq.Provider prov in db.Providers)
            {
                string path = Path.Combine(new string[] { MailServiceProgram.TempFolderPath, "ProviderImage" + Guid.NewGuid() + ".png" }).ToString();
                providerDic.Add(prov.Name, new KeyValuePair<string, string>(path, prov.Logo));
            }
            return providerDic;
        }

    }
}
