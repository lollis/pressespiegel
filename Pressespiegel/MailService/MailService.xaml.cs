﻿
using MailService.Email;
using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace EmailService
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private TextWriter _writer = null;
        internal MailServiceProgram notifications { get; private set; }
        public MainWindow()
        {

            try
            {
                InitializeComponent();
                _writer = new TextBoxStreamWriter(txtConsole);
                Console.SetOut(_writer);

                Console.WriteLine("Connection String: " + RssService.MainWindow.ConnectionString);
                notifications = new MailServiceProgram();
                this.Closed += MainWindow_Closed;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }



        private void MainWindow_Closed(object sender, EventArgs e)
        {
            notifications.Dispose();
        }
    }

    /// <summary>
    /// Überschreibt Console.WriteLine für XAML-Anwendung
    /// </summary>
    public class TextBoxStreamWriter : TextWriter
    {
        TextBox _output = null;
        char lastValue;

        public TextBoxStreamWriter(TextBox output)
        {
            _output = output;
        }

        /// <summary>
        /// Override von Console.WriteLine, schreibt char auf txtOut
        /// </summary>
        /// <param name="value">Ausgabeparameter</param>
        public override void Write(char value)
        {
            string text;
            if (lastValue.Equals('\n'))
                text = System.DateTime.Now.ToString("HH:mm:ss") + " |   " + value.ToString();
            else
                text = value.ToString();
            base.Write(value);
            if (Application.Current.Dispatcher.CheckAccess())
            {
                _output.AppendText(text.ToString());
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(
                  DispatcherPriority.Background,
                  new Action(() =>
                  {
                      _output.AppendText(text.ToString());
                  }));
            }
            lastValue = value;
            //_output.AppendText(value.ToString()); // When character data is written, append it to the text box.
        }


        public override Encoding Encoding
        {
            get { return System.Text.Encoding.UTF8; }
        }
    }
}
