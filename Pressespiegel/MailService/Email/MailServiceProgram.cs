﻿using RssService.SqlLinq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Timers;
using TableDependency.SqlClient;
using TableDependency.SqlClient.Exceptions;

namespace MailService.Email
{
    /// <summary>
    /// Enthält die Logik des MailSerivces
    /// </summary>
    class MailServiceProgram : IDisposable
    {
        private BackgroundWorker worker;
        bool firstRun = true;
        Timer timer;
        SortedDictionary<TimeSpan, EmailNotificationTime> emailOrders;
        TableDependency.SqlClient.SqlTableDependency<RssService.SqlLinq.EmailOrder> tdep = null;
        internal static string TempFolderPath { get; private set; }

        internal MailServiceProgram()
        {
            TempFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "temp");
            bool exists = System.IO.Directory.Exists(TempFolderPath);
            if (!exists)
                try
                {
                    System.IO.Directory.CreateDirectory(TempFolderPath);
                    Console.WriteLine("TempPath: " + TempFolderPath);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + "  Please Restart the app and run it with Admin Rights");
                }
            emailOrders = new SortedDictionary<TimeSpan, EmailNotificationTime>();
            worker = new BackgroundWorker();
            worker.DoWork += Worker_DoWork;
            timer = new Timer(1);
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }
        /// <summary>
        /// Started das Programm erneuet und lädt alle einträge von der Datenbank in das Program
        /// </summary>
        private void Restart()
        {
            timer.Stop();
            emailOrders.Clear();
            try
            {
                LoadAllEmailOrdersFromDataSource();
                tdep.Dispose();
                StartDependency();
            }
            catch (Exception ex)
            {
                Console.Write("Neustart von TableDependency fehlgeschlagen, wird in 30 sekunden nochmals versucht");
                System.Threading.Thread.Sleep(30000);
                Restart();
            }
            finally
            {
                timer.Start();
            }
        }

        /// <summary>
        /// Started den Background Worker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!worker.IsBusy)
                worker.RunWorkerAsync();
            else
                Console.WriteLine("WORKER BUSY no mail could be send");
            DateTime now = new DateTime();
            now = DateTime.Now;
            now = now.AddSeconds((30 - now.Second) + 60);
            timer.Interval = (now.Ticks - DateTime.Now.Ticks) / 10000;
        }

        /// <summary>
        /// Beim ersten durchlauf werden alle Mails von der Datenbank in das Programm gepuffert, anschließend werden die E-Mials zur aktellen Uhrzeit versendet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (firstRun)
            {
                LoadAllEmailOrdersFromDataSource();
                StartDependency();
                firstRun = false;
            }
            string time = System.DateTime.Now.ToString("HH:mm");
            int hour = int.Parse(time.Split(':')[0]);
            int minute = int.Parse(time.Split(':')[1]);
            TimeSpan currentTime = new TimeSpan(hour, minute, 0);
            if (emailOrders.ContainsKey(currentTime))
            {
                emailOrders[currentTime].SendMail();
            }
            Console.WriteLine("Worker_DoWork");
        }


        /// <summary>
        /// Startet TableDependency, wird benötigt um änderungen der Datenbank zu bekommen
        /// </summary>
        private void StartDependency()
        {
            string connectionString = RssService.MainWindow.ConnectionString;
            SqlCommand c = new SqlCommand("SELECT * FROM EMailOrder", new SqlConnection(connectionString));
            c.Connection.Open();
            c.ExecuteNonQuery();
            try
            {

                tdep = new SqlTableDependency<RssService.SqlLinq.EmailOrder>(connectionString, "EmailOrder");

            }
            catch (ServiceBrokerNotEnabledException ex)
            {
                SqlConnection con = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand("ALTER DATABASE CURRENT SET ENABLE_BROKER", con);
                con.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                    tdep = new SqlTableDependency<RssService.SqlLinq.EmailOrder>(connectionString, "dbo.EmailOrder");
                    Console.WriteLine("Broker enabled");
                }
                catch (SqlException se)
                {
                    cmd.CommandText = "ALTER DATABASE CURRENT SET NEW_BROKER WITH ROLLBACK IMMEDIATE";
                    cmd.ExecuteNonQuery();
                    tdep = new SqlTableDependency<RssService.SqlLinq.EmailOrder>(connectionString, "dbo.EmailOrder");
                    Console.WriteLine("New Broker");
                }
                catch (Exception exe)
                {
                    Console.WriteLine(ex.Message + "Could not Set Broker, please restart the programm or drink a beer");
                    throw (exe);
                }
            }
            tdep.OnChanged += Tdep_OnChanged;
            tdep.OnError += Tdep_OnError;
            tdep.Start();
            Console.WriteLine("Table Dependency started");
        }

        private void Tdep_OnError(object sender, TableDependency.EventArgs.ErrorEventArgs e)
        {
            Console.WriteLine("!!!ERROR in TableDependency, es wird neu gestartet  \n \n \n \n" + e.Error.Message);
            Restart();
        }
        /// <summary>
        /// Bekommt änderungen der Datenbank und hält Puffer aktuell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Tdep_OnChanged(object sender, TableDependency.EventArgs.RecordChangedEventArgs<RssService.SqlLinq.EmailOrder> e)
        {
            try
            {
                switch (e.ChangeType)
                {
                    case TableDependency.Enums.ChangeType.Insert:
                        AddEmailOrder(e.Entity.Id, e.Entity.BroadcastTime);
                        break;
                    case TableDependency.Enums.ChangeType.Delete:
                        KeyValuePair<string, List<TimeSpan>> times = StringToTimeSpans(e.Entity.BroadcastTime);

                        foreach (TimeSpan t in times.Value)
                        {
                            List<EmailOrder> orderTime = emailOrders[t].orders;
                            int index = orderTime.FindIndex(i => i.EmailOrderId == e.Entity.Id);
                            orderTime.RemoveAt(index);
                            Console.WriteLine("Profile Deleted, ID=" + e.Entity.Id);
                        }
                        break;
                    case TableDependency.Enums.ChangeType.Update:
                        for (int k = 0; k < emailOrders.Count; k++)
                        {
                            KeyValuePair<TimeSpan, EmailNotificationTime> Pair = emailOrders.ElementAt(k);
                            int index = -1;
                            index = Pair.Value.orders.FindIndex(i => i.EmailOrderId == e.Entity.Id);
                            if (index != -1)
                            {
                                Pair.Value.orders.RemoveAt(index);
                                Console.WriteLine("Profile Deleted, ID=" + e.Entity.Id);
                            }
                        }
                        AddEmailOrder(e.Entity.Id, e.Entity.BroadcastTime);
                        break;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("!!!ERROR" + ex.Message);
            }
        }

        /// <summary>
        /// Neue E-Mail Order wird in puffer hinzgefügt
        /// </summary>
        /// <param name="OrderId">EmailOrder.OrderId (Sql)</param>
        /// <param name="BroadcastTime">EmaolOrder.BroadcastTime (SQL)</param>
        private void AddEmailOrder(long OrderId, string BroadcastTime)
        {
            try
            {
                KeyValuePair<string, List<TimeSpan>> times = StringToTimeSpans(BroadcastTime);
                foreach (TimeSpan t in times.Value)
                {
                    if (emailOrders.ContainsKey(t))
                    {
                        emailOrders[t].orders.Add(CreateEmailOrder(OrderId, times.Key));
                        Console.WriteLine("Add BroadcastTime, Time=" + t.ToString() + "  OrderId=" + OrderId);
                    }
                    else
                    {
                        emailOrders.Add(t, new EmailNotificationTime(CreateEmailOrder(OrderId, times.Key)));
                        Console.WriteLine("Append BroadcastTime, Time=" + t.ToString() + "  OrderId=" + OrderId);
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR!!!" + ex.Message);
            }
        }

        /// <summary>
        /// Neue E-MailOrder klasse wird hinzugefügt, davor muss mit emailOrders.ContainsKey(Uhrzeit) abgefragt werden
        /// </summary>
        /// <param name="EmailOrderId"></param>
        /// <param name="BroadcastTime"><param>
        /// <returns></returns>
        private EmailOrder CreateEmailOrder(long EmailOrderId, string BroadcastTime)
        {
            V_EmailOrderPdf order;
            //ProfileName
            using (var db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString))
            {
                List<V_EmailOrderPdf> pdf = new List<V_EmailOrderPdf>();
                pdf.AddRange(from t in db.V_EmailOrderPdfs
                             where t.EmailOrderId == EmailOrderId
                             select t)
                                              ;
                order = pdf[0];
            }
            return new EmailOrder(order.ProfileId, order.Email, order.ProfileName, order.UserName, BroadcastTime, order.EmailOrderId);
        }

        /// <summary>
        /// Füllt puffer zum Programmstart
        /// </summary>
        private void LoadAllEmailOrdersFromDataSource()
        {
            try
            {
                UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
                List<RssService.SqlLinq.EmailOrder> orders = new List<RssService.SqlLinq.EmailOrder>();
                orders.AddRange(from t in db.EmailOrders
                                select t);
                foreach (RssService.SqlLinq.EmailOrder order in orders)
                {
                    AddEmailOrder(order.Id, order.BroadcastTime);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception LoadAllEmailOrdersFromDataSource: " + ex.Message);
                throw (ex);
            }
        }



        /// <summary>
        /// Converts String from DataBase (BroadcastTIme to multiple TimeSpan)
        /// </summary>
        /// <param name="value">days (German Format) ?dayHour(HH.mm), multiple dayHour separated with ';'</param>
        /// <returns>Key=days, value=List of timeSpan</returns>
        private KeyValuePair<string, List<TimeSpan>> StringToTimeSpans(string value)
        {
            string[] dayHour = value.Split('?');
            string[] spans = dayHour[1].Split(';');
            List<TimeSpan> ret = new List<TimeSpan>();
            foreach (string span in spans)
            {
                if (!string.IsNullOrEmpty(span))
                {
                    string[] minHour = span.Split(':');
                    ret.Add(new TimeSpan(int.Parse(minHour[0]), int.Parse(minHour[1]), 0));
                }
            }
            return new KeyValuePair<string, List<TimeSpan>>(dayHour[0], ret);

        }

        public void Dispose()
        {
            tdep.Stop();
            tdep.Dispose();
        }
    }
}
