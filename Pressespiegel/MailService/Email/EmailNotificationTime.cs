﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailService.Email
{
    /// <summary>
    /// Hier sind alle EmailNotifications enthalten, welchen zur selben Zeit abgesendet werden sollen
    /// </summary>
    internal class EmailNotificationTime
    {
        private List<EmailOrder> _orders = new List<EmailOrder>();
        /// <summary>
        /// Enthält alle E-Mali Orders 
        /// </summary>
        internal List<EmailOrder> orders { get { return _orders; } private set { _orders = value; } }
        /// <summary>
        /// Sendet ale Mails für die aktuelle Uhrzeit
        /// </summary>
        internal void SendMail()
        {
            Parallel.ForEach(orders, async currentOrder =>
             await currentOrder.SendEmail()
            );
        }
        internal EmailNotificationTime()
        {

        }
        internal EmailNotificationTime(EmailOrder order)
        {
            orders.Add(order);
        }
    }
}
