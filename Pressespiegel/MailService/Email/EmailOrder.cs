﻿using Pressespiegel.DataProvider;
using System;
using System.Configuration;
using System.Globalization;
using System.Net.Mail;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;

namespace MailService.Email
{
    /// <summary>
    /// Enthält informationen über einen E-Mail auftrag
    /// </summary>
    class EmailOrder
    {
        internal string UserName { get; private set; }
        internal string ProfileName { get; private set; }
        internal string Destination { get; private set; }
        internal long ProfileId { get; private set; }
        internal long EmailOrderId { get; private set; }

        internal string days;
        /// <summary>
        /// Konsturktor, alle Daten müssen von Datenbakn abgefragt werden
        /// </summary>
        /// <param name="ProfileId"></param>
        /// <param name="Destination"></param>
        /// <param name="ProfileName"></param>
        /// <param name="UserName"></param>
        /// <param name="days"></param>
        internal EmailOrder(long ProfileId, string Destination, string ProfileName, string UserName, string days, long EmailOrderId)
        {
            this.ProfileId = ProfileId;
            this.ProfileName = ProfileName;
            this.Destination = Destination;
            this.ProfileId = ProfileId;
            this.UserName = UserName;
            this.EmailOrderId = EmailOrderId;
            this.days = days;
        }

        public async Task SendEmail()
        {
            try
            {

                if (string.IsNullOrEmpty(days) || days.Contains(DateTime.Now.ToString("ddd", CultureInfo.CreateSpecificCulture("de-DE"))))
                {

                    System.IO.Stream pdf = new Pdf.Pdf().CreatePdf("Pressespiegel", UserName, ProfileName, new ElasticSearch().SearchByID(ProfileId));

                    string from = ConfigurationManager.AppSettings["emailServiceUserName"];
                    MailAddress address = new MailAddress(from, "Pressespiegel");
                    MailMessage mail = new MailMessage(address, new MailAddress(Destination));
                    mail.IsBodyHtml = true;
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                    mail.Subject = "Pressespiegel des Profils " + ProfileName;
                    mail.Body = "Hallo " + UserName + "<br/> Anbei ist Ihr Pressespiegel. <br/> <br/> Mit freundlichen Grüßen <br/> Ihr Team 4";


                    System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(pdf, "Pressespiegel " + ProfileName + " " + System.DateTime.Now.ToString("dd.MM.yyyy") + ".pdf");
                    mail.Attachments.Add(attachment);

                    SmtpServer.Port = 587;
                    SmtpServer.Credentials = new System.Net.NetworkCredential(from, ConfigurationManager.AppSettings["emailServicePassword"]);
                    SmtpServer.EnableSsl = true;
                    Console.WriteLine("Email send To " + ProfileId);
                    await SmtpServer.SendMailAsync(mail);
                    new RssService.Archiv.ArchivProfile(ProfileId).writeCurrentIndizes(DateTime.Parse(System.DateTime.Now.ToString("HH:mm dd.MM.yyyy")));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


    }
}
