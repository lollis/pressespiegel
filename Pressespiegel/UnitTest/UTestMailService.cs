﻿using System;
using MailService.Email;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestSuite.UnitTests
{
    [TestClass]
    public class UTestMailService
    {

        /// <summary>
        /// Before you run tests on a new Sytem you have to change the AspNetDevelopmentServerHost string.
        /// For that change the string in the AspNetDevelopmentServerHost-Attribute above a TestCase.
        /// This string hast to point to the Folder with the Pressespiegel Project folder in it 
        /// (the folder for the pressespiegel.csproj project, not the pressespiegel solution)
        /// </summary>


        private TestContext _testContext;
        public TestContext TestContext
        {

            get { return _testContext; }

            set { _testContext = value; }

        }

        /// <summary>
        /// Tests the Mail Service
        /// Before this Tests runs there must be at least one valid Search Profile
        /// in the Database.
        /// </summary>
        [TestMethod]
        public void Test_MailService()
        {
            try
            {
                // Arrange
                MailServiceProgram mailService = new MailServiceProgram();
                PrivateObject p_mailService = new PrivateObject(mailService);

                // Act
                p_mailService.Invoke("Restart", p_mailService.GetField("timer"), EventArgs.Empty);

                // Assert
                Assert.Inconclusive("Bitte überprüfen sie ihr Email Fach ob tatsächlich eine Email angekommmen ist");
                // Da es nicht möglich ist über Code zu überprüfen ob tatsächlich 
                // eine Email ankommt, gilt dieser Test als Bestanden wenn keine Exception ausgelöst wird
            }
            catch (Exception ex)
            {
                Assert.Fail(ErrorLog.formatException(ex));
                ErrorLog.logException(ex);
            }

        }
    }
}
