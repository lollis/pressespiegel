﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSuite
{

    /// <summary>
    /// A Class that provides static methods for documenting and tracking of exceptions and errors
    /// </summary>
    class ErrorLog
    {
        /// <summary>
        /// Writes the Date, the exception message, the exception source and 
        /// the exception stack trace in the file "error_log.txt" in the directory this project runs
        /// </summary>
        /// <param name="ex">the exception to print</param>
        public static void logException(Exception ex)
        {
            StreamWriter writer;
            string filePath = AppDomain.CurrentDomain.BaseDirectory + "\\error_log.txt";

            string header = "--------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n";
            string text = DateTime.Now.ToString() + " > " + ex.Message + "\r\n"
                            + "Source: " + ex.Source + "\r\n" + ex.StackTrace + "\r\n";
            string completeString = header + text + header + "\r\n\r\n";

            if (!File.Exists(filePath))
            {
                File.Create(filePath).Dispose();
            }

            using (writer = new StreamWriter(filePath, true))
            {
                writer.Write(completeString);
            }

        }

        /// <summary>
        /// Writes the Date, the exception message, the exception source, 
        /// the exception stack trace and a custom text in the file "error_log.txt" 
        /// in the directory this project runs
        /// </summary>
        /// <param name="ex">the exception to print</param>
        /// <param name="customText">A custom text that is printed with the exception text</param>
        public static void logException(Exception ex, string customText)
        {
            StreamWriter writer;
            string filePath = AppDomain.CurrentDomain.BaseDirectory + "\\error_log.txt";

            string header = "--------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n";
            string text = DateTime.Now.ToString() + " > " + ex.Message + "\r\n"
                            + "Source: " + ex.Source + "\r\n" + "Additional Info: " + customText + "\r\n"
                            + ex.StackTrace + "\r\n";
            string completeString = header + text + header + "\r\n\r\n";

            if (!File.Exists(filePath))
            {
                File.Create(filePath).Dispose();
            }

            using (writer = new StreamWriter(filePath, true))
            {
                writer.Write(completeString);
            }

        }

        /// <summary>
        /// WARNING: If there is already an existing file in the given file path that is not 
        /// a .txt file this file will be overwitten without any warning and with no option to recover it!
        /// 
        /// Writes the Date, the exception message, the exception source and 
        /// the exception stack trace in the a custom path
        /// </summary>
        /// <param name="ex">The exception to print</param>
        /// <param name="customText">A custom text that is printed with the exception text</param>
        /// <param name="filePath">A custom file path. The path must end with ".txt" e.g. @"C\some folder\custom_error_file.txt"</param>
        public static void logException(Exception ex, string customText, string filePath)
        {
            StreamWriter writer;

            string header = "--------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n";
            string text = DateTime.Now.ToString() + " > " + ex.Message + "\r\n"
                            + "Source: " + ex.Source + "\r\n" + "Additional Info: " + customText + "\r\n"
                            + ex.StackTrace + "\r\n";
            string completeString = header + text + header + "\r\n\r\n";

            if (!File.Exists(filePath))
            {
                File.Create(filePath).Dispose();
            }

            using (writer = new StreamWriter(filePath, true))
            {
                writer.Write(completeString);
            }

        }

        /// <summary>
        /// Writes the Date and a custom text in the file "error_log.txt" 
        /// in the directory this project runs
        /// </summary>
        /// <param name="ex">the exception to print</param>
        /// <param name="customText">A custom text that is printe</param>
        public static void logCustomText(string customText)
        {
            StreamWriter writer;
            string filePath = AppDomain.CurrentDomain.BaseDirectory + "\\error_log.txt";

            string header = "--------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n";
            string text = DateTime.Now.ToString() + " > \r\n" + customText + "\r\n";
            string completeString = header + text + header + "\r\n\r\n";

            if (!File.Exists(filePath))
            {
                File.Create(filePath).Dispose();
            }

            using (writer = new StreamWriter(filePath, true))
            {
                writer.Write(completeString);
            }

        }

        /// <summary>
        /// WARNING: If there is already an existing file in the given file path that is not 
        /// a .txt file this file will be overwitten without any warning and with no option to recover it!
        /// 
        /// Writes the Date and a custom text in a custom path
        /// </summary>
        /// <param name="customText">A custom text that is printed</param>
        /// <param name="filePath">A custom file path. The path must end with ".txt" e.g. @"C\some folder\custom_error_file.txt"</param>
        public static void logCustomText(string customText, string filePath)
        {
            StreamWriter writer;

            string header = "--------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n";
            string text = DateTime.Now.ToString() + " > \r\n" + customText + "\r\n";
            string completeString = header + text + header + "\r\n\r\n";

            if (!File.Exists(filePath))
            {
                File.Create(filePath).Dispose();
            }

            using (writer = new StreamWriter(filePath, true))
            {
                writer.Write(completeString);
            }

        }

        /// <summary>
        /// WARNING: If there is already an existing file in the given file path that is not 
        /// a .txt file this file will be overwitten without any warning and with no option to recover it!
        /// 
        /// Writes the Date and a custom text in a custom path
        /// </summary>
        /// <param name="customText">A custom text that is printed</param>
        /// <param name="filePath">A custom file path. The path must end with ".txt" e.g. @"C\some folder\custom_error_file.txt"</param>
        public static string formatException(Exception ex)
        {
            return ex.Message + "\n" + ex.StackTrace;
        }

    }
}
