﻿using System;
using System.Web.UI;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Web.UI.WebControls;
using TestSuite;

namespace TestSuite.Tests
{
    [TestClass]
    public class UTestFreieSuche
    {
        /// <summary>
        /// Before you run tests on a new Sytem you have to change the AspNetDevelopmentServerHost string.
        /// For that change the string in the AspNetDevelopmentServerHost-Attribute above a TestCase.
        /// This string hast to point to the Folder with the Pressespiegel Project folder in it 
        /// (the folder for the pressespiegel.csproj project, not the pressespiegel solution)
        /// </summary>


        private Page page;
        private TestContext _testContext;
        public TestContext TestContext
        {

            get { return _testContext; }

            set { _testContext = value; }

        }

        /// <summary>
        /// This Method tests the Free Search with valid input
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/PressReview/Overview.aspx")]
        public void Test_FreeSearch()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);

                TextBox txtContent = (TextBox)ControlFinder.FindControl(page, "txtContent") as TextBox;
                TextBox txtServiceName = (TextBox)ControlFinder.FindControl(page, "txtServiceName") as TextBox;
                Table searchResults = (Table)ControlFinder.FindControl(page, "searchResults") as Table;
                Button btnSearch = (Button)ControlFinder.FindControl(page, "btnSearch") as Button;

                txtServiceName.Text = "Focus";
                txtContent.Text = "Fußball";

                //Act
                p_page.Invoke("btnCreateProfile_Click", btnSearch, EventArgs.Empty);

                //Assert
                Assert.IsTrue(searchResults.Rows.Count > 0);
            }
            catch (Exception ex)
            {
                Assert.Fail(ErrorLog.formatException(ex));
                ErrorLog.logException(ex);
            }
        }

        /// <summary>
        /// This Method tests the free Search with no input
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/PressReview/Overview.aspx")]
        public void Test_FreeSearch_NoService()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);

                TextBox txtContent = (TextBox)ControlFinder.FindControl(page, "txtContent") as TextBox;
                TextBox txtServiceName = (TextBox)ControlFinder.FindControl(page, "txtServiceName") as TextBox;
                Table searchResults = (Table)ControlFinder.FindControl(page, "searchResults") as Table;
                Button btnSearch = (Button)ControlFinder.FindControl(page, "btnSearch") as Button;
                Label labelInfo = (Label)ControlFinder.FindControl(page, "labelInfo") as Label;

                txtServiceName.Text = "";
                txtContent.Text = "";

                //Act
                p_page.Invoke("btnCreateProfile_Click", btnSearch, EventArgs.Empty);

                //Assert
                Assert.IsTrue(searchResults.Rows.Count > 0);
            }
            catch (Exception ex)
            {
                Assert.Fail(ErrorLog.formatException(ex));
                ErrorLog.logException(ex);
            }
        }

        /// <summary>
        /// This Method Tests the Free Search without input
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/PressReview/Overview.aspx")]
        public void Test_FreeSearch_NoInput()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);
                String expected = "Die Suche ergab keine Treffer.";

                TextBox txtContent = (TextBox)ControlFinder.FindControl(page, "txtContent") as TextBox;
                TextBox txtServiceName = (TextBox)ControlFinder.FindControl(page, "txtServiceName") as TextBox;
                Table searchResults = (Table)ControlFinder.FindControl(page, "searchResults") as Table;
                Button btnSearch = (Button)ControlFinder.FindControl(page, "btnSearch") as Button;
                Label labelInfo = (Label)ControlFinder.FindControl(page, "labelInfo") as Label;

                txtServiceName.Text = "";
                txtContent.Text = "";

                //Act
                p_page.Invoke("btnCreateProfile_Click", btnSearch, EventArgs.Empty);

                //Assert
                Assert.Equals(expected, labelInfo.Text);
            }
            catch (Exception ex)
            {
                Assert.Fail(ErrorLog.formatException(ex));
                ErrorLog.logException(ex);
            }
        }

    }
}
