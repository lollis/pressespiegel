﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Owin;
using Owin;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using RssService;

namespace TestSuite.UnitTests
{

    [TestClass]
    public class UTestLogin
    {

        /// <summary>
        /// Before you run tests on a new Sytem you have to change the AspNetDevelopmentServerHost string.
        /// For that change the string in the AspNetDevelopmentServerHost-Attribute above a TestCase.
        /// This string hast to point to the Folder with the Pressespiegel Project folder in it 
        /// (the folder for the pressespiegel.csproj project, not the pressespiegel solution)
        /// </summary>

        private Page page;
        private TestContext _testContext;
        public TestContext TestContext
        {

            get { return _testContext; }

            set { _testContext = value; }

        } 

        /// <summary>
        /// Tests the login function with no input
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/Account/Login.aspx")]
        public void Test_LogIn_NoInput()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);
                String expected;

                TextBox txtEmail = (TextBox) ControlFinder.FindControl(page, "Email") as TextBox;
                TextBox txtPassword = (TextBox)ControlFinder.FindControl(page, "Password") as TextBox;
                CheckBox chkRememberMe = (CheckBox)ControlFinder.FindControl(page, "RememberMe") as CheckBox;
                Button btnLogin = (Button)ControlFinder.FindControl(page, "LoginButton") as Button;
                Literal LitFailureText = (Literal)ControlFinder.FindControl(page, "FailureText") as Literal;

                expected = "Invalid login attempt";

                //Act
                p_page.Invoke("LogIn", btnLogin, EventArgs.Empty);

                //Assert
                Assert.AreEqual(expected, LitFailureText.Text);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message + "\n" + ex.StackTrace);
                ErrorLog.logException(ex);
            }
        }

        /// <summary>
        /// Tests the login function with correct input
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/Account/Login.aspx")]
        public void Test_LogIn_CorrectInput()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);
                String expected;

                TextBox txtEmail = (TextBox)ControlFinder.FindControl(page, "Email") as TextBox;
                TextBox txtPassword = (TextBox)ControlFinder.FindControl(page, "Password") as TextBox;
                CheckBox chkRememberMe = (CheckBox)ControlFinder.FindControl(page, "RememberMe") as CheckBox;
                Button btnLogin = (Button)ControlFinder.FindControl(page, "LoginButton") as Button;
                Literal LitFailureText = (Literal)ControlFinder.FindControl(page, "FailureText") as Literal;

                txtEmail.Text = "fabian.brecht@web.de";
                txtPassword.Text = "Passwort12;";
                chkRememberMe.Checked = true;

                expected = "";

                //Act
                p_page.Invoke("LogIn", btnLogin, EventArgs.Empty);

                //Assert
                Assert.AreEqual(expected, LitFailureText.Text);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message + "\n" + ex.StackTrace);
                ErrorLog.logException(ex);
            }
        }

        /// <summary>
        /// Tests the login function with correct input
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/Account/Login.aspx")]
        public void Test_LogInMoreTimes_CorrectInput()
        {
            try
            { 
                // Arrange
                int tries = 99;
                int i;

                // Act
                for (i = 0; i < tries; i++)
                {
                    Test_LogIn_CorrectInput();
                }
                // If the test fails at any loop the code will jump into the catch branch

                // Assert
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message + "\n" + ex.StackTrace);
                ErrorLog.logException(ex);
            }
        }

        /// <summary>
        /// Tests the login function with an invalid password
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/Account/Login.aspx")]
        public void Test_LogIn_WrongPassword()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);
                String expected;

                TextBox txtEmail = (TextBox)ControlFinder.FindControl(page, "Email") as TextBox;
                TextBox txtPassword = (TextBox)ControlFinder.FindControl(page, "Password") as TextBox;
                CheckBox chkRememberMe = (CheckBox)ControlFinder.FindControl(page, "RememberMe") as CheckBox;
                Button btnLogin = (Button)ControlFinder.FindControl(page, "LoginButton") as Button;
                Literal LitFailureText = (Literal)ControlFinder.FindControl(page, "FailureText") as Literal;

                txtEmail.Text = "fabian.brecht@web.de";
                txtPassword.Text = "WrongPassword";
                chkRememberMe.Checked = true;

                expected = "Invalid login attempt";

                //Act
                p_page.Invoke("LogIn", btnLogin, EventArgs.Empty);

                //Assert
                Assert.AreEqual(expected, LitFailureText.Text);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message + "\n" + ex.StackTrace);
                ErrorLog.logException(ex);
            }
        }

        /// <summary>
        /// Tests the login function with an unconfirmed e mail
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/Account/Login.aspx")]
        public void Test_LogIn_UnconfirmedEMail()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);
                String expected;

                TextBox txtEmail = (TextBox)ControlFinder.FindControl(page, "Email") as TextBox;
                TextBox txtPassword = (TextBox)ControlFinder.FindControl(page, "Password") as TextBox;
                CheckBox chkRememberMe = (CheckBox)ControlFinder.FindControl(page, "RememberMe") as CheckBox;
                Button btnLogin = (Button)ControlFinder.FindControl(page, "LoginButton") as Button;
                Literal LitFailureText = (Literal)ControlFinder.FindControl(page, "FailureText") as Literal;

                txtEmail.Text = "fabian.brecht.2@web.de";
                txtPassword.Text = "Passwort12;";
                chkRememberMe.Checked = true;

                expected = "Invalid login attempt. You must have a confirmed email address. Enter your email and password, then press 'Resend Confirmation'.";

                //Act
                p_page.Invoke("LogIn", btnLogin, EventArgs.Empty);

                //Assert
                Assert.AreEqual(expected, LitFailureText.Text);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message + "\n" + ex.StackTrace);
                ErrorLog.logException(ex);
            }
        }

    }
    
}
