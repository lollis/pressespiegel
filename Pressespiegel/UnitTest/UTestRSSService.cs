﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RssService;
using System.Collections.Generic;

namespace TestSuite.UnitTests
{
    [TestClass]
    public class UTestRSSService
    {

        /// <summary>
        /// Before you run tests on a new Sytem you have to change the AspNetDevelopmentServerHost string.
        /// For that change the string in the AspNetDevelopmentServerHost-Attribute above a TestCase.
        /// This string hast to point to the Folder with the Pressespiegel Project folder in it 
        /// (the folder for the pressespiegel.csproj project, not the pressespiegel solution)
        /// </summary>


        private TestContext _testContext;
        public TestContext TestContext
        {

            get { return _testContext; }

            set { _testContext = value; }

        }

        /// <summary>
        /// Tests the correct function of Elastic search by adding
        /// a RSSItem and then searching for it to confirm it was added correctly
        /// </summary>
        [TestMethod()]
        public void Test_ElasticSearch_AddItem()
        {
            try
            {
                // Arrange
                ElasticHandler elasticHandler;
                RssItem testItem;
                String expected;
                bool result;

                elasticHandler = new ElasticHandler();
                testItem = new RssItem()
                {
                    Id = "Id",
                    Title = "ServiceName",
                    Content = "Content",
                    FullContent = "FullContent",
                    Url = "Url",
                    Date = DateTime.Now,
                    ServiceName = "ServiceName",
                    BildUrl = "BildUrl"
                };

                expected = "ServiceName-" + DateTime.Now.ToString("dd.MM.yyyy");

                // Act
                elasticHandler.DeleteByIndex(expected);
                elasticHandler.AddFeedToNode(testItem);

                result = elasticHandler.SearchIndex(expected);

                // Assert
                Assert.IsTrue(result);
            }
            catch (Exception ex)
            {
                Assert.Fail(ErrorLog.formatException(ex));
                ErrorLog.logException(ex);
            }
        }

        [TestMethod()]
        public void Test_ElasticSearch_DeleteItem()
        {
            try
            {
                // Arrange
                ElasticHandler elasticHandler;
                RssItem testItem;
                String itemIndex;
                bool result;

                elasticHandler = new ElasticHandler();
                testItem = new RssItem()
                {
                    Id = "Id",
                    Title = "Title",
                    Content = "Content",
                    FullContent = "FullContent",
                    Url = "Url",
                    Date = new DateTime(1996, 02, 02),
                    ServiceName = "ServiceName",
                    BildUrl = "BildUrl"
                };

                itemIndex = "ServiceName-02.02.1996";

                // Act
                elasticHandler.DeleteByIndex(itemIndex);
                elasticHandler.AddFeedToNode(testItem);
                elasticHandler.DeleteByIndex(itemIndex);

                result = elasticHandler.SearchIndex(itemIndex);

                // Assert
                Assert.IsFalse(result);
            }
            catch (Exception ex)
            {
                Assert.Fail(ErrorLog.formatException(ex));
                ErrorLog.logException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        //[TestMethod()]
        //public void Test_RSSReader()
        //{
        //    try
        //    {
        //        // Arrange
        //        RssReader rssReader;
        //        List<RssItem> listItems;
        //        bool result;

        //        rssReader = new RssReader();

        //        // Act
        //        listItems = rssReader.GetItems("Focus", "http://rss.focus.de/fol/XML/rss_folnews.xml");
        //        result = (listItems.Count > 0);

        //        // Assert
        //        Assert.IsTrue(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.Fail(ErrorLog.formatException(ex));
        //        ErrorLog.logException(ex);
        //    }
        //}



    }
}
