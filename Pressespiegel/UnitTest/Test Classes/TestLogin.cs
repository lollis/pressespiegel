﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Owin;
using Owin;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using RssService;

namespace RssService.Test
{
    //public class Mock_Login : Pressespiegel.Account.Login
    //{

    //    public String Mock_FailureText { get { return FailureText.Text; } }

    //    public Mock_Login(UnitTest_Login.LoginTestArgs Args)
    //    {
    //        this.Page_Load(null, EventArgs.Empty);

    //        // TODO: Füge Informationen ein
    //        switch (Args)
    //        {
    //            case UnitTest_Login.LoginTestArgs.MethodLogicOnly:
    //                break;
    //            case UnitTest_Login.LoginTestArgs.WithUnconfirmedMail:
    //                Email.Text = "";
    //                Password.Text = "";
    //                RememberMe.Checked = false;
    //                break;
    //            case UnitTest_Login.LoginTestArgs.WithValidInformation:
    //                Email.Text = "";
    //                Password.Text = "";
    //                RememberMe.Checked = false;
    //                break;
    //            case UnitTest_Login.LoginTestArgs.WithInvalidInformation:
    //                Email.Text = "";
    //                Password.Text = "";
    //                RememberMe.Checked = false;
    //                break;
    //            default:
    //                break;

    //        }
    //    }

    //    public void Mock_LogIn()
    //    {
    //        this.LogIn(null, EventArgs.Empty);
    //    }
    //}

    [TestClass]
    public class TestLogin
    {
        private Page page;
        private TestContext _testContext;
        public TestContext TestContext
        {

            get { return _testContext; }

            set { _testContext = value; }

        }

        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/Account/Login.aspx")]
        public void Test_LogIn_NoInput()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);
                String expected;

                TextBox txtEmail = (TextBox)ControlFinder.FindControl(page, "Email") as TextBox;
                TextBox txtPassword = (TextBox)ControlFinder.FindControl(page, "Password") as TextBox;
                CheckBox chkRememberMe = (CheckBox)ControlFinder.FindControl(page, "RememberMe") as CheckBox;
                Button btnLogin = (Button)ControlFinder.FindControl(page, "LoginButton") as Button;
                Literal LitFailureText = (Literal)ControlFinder.FindControl(page, "FailureText") as Literal;

                expected = "Invalid login attempt";

                //Act
                p_page.Invoke("LogIn", btnLogin, EventArgs.Empty);

                //Assert
                Assert.AreEqual(expected, LitFailureText.Text);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message + "\n" + ex.StackTrace);
                ErrorLog.logException(ex);
            }
        }

        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/Account/Login.aspx")]
        public void Test_LogIn_CorrectInput()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);
                String expected;

                TextBox txtEmail = (TextBox)ControlFinder.FindControl(page, "Email") as TextBox;
                TextBox txtPassword = (TextBox)ControlFinder.FindControl(page, "Password") as TextBox;
                CheckBox chkRememberMe = (CheckBox)ControlFinder.FindControl(page, "RememberMe") as CheckBox;
                Button btnLogin = (Button)ControlFinder.FindControl(page, "LoginButton") as Button;
                Literal LitFailureText = (Literal)ControlFinder.FindControl(page, "FailureText") as Literal;

                txtEmail.Text = "fabian.brecht@web.de";
                txtPassword.Text = "1228BBdB8;";
                chkRememberMe.Checked = true;

                expected = "";

                //Act
                p_page.Invoke("LogIn", btnLogin, EventArgs.Empty);

                //Assert
                Assert.AreEqual(expected, LitFailureText.Text);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message + "\n" + ex.StackTrace);
                ErrorLog.logException(ex);
            }
        }

        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/Account/Login.aspx")]
        public void Test_LogIn_WrongPassword()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);
                String expected;

                TextBox txtEmail = (TextBox)ControlFinder.FindControl(page, "Email") as TextBox;
                TextBox txtPassword = (TextBox)ControlFinder.FindControl(page, "Password") as TextBox;
                CheckBox chkRememberMe = (CheckBox)ControlFinder.FindControl(page, "RememberMe") as CheckBox;
                Button btnLogin = (Button)ControlFinder.FindControl(page, "LoginButton") as Button;
                Literal LitFailureText = (Literal)ControlFinder.FindControl(page, "FailureText") as Literal;

                txtEmail.Text = "fabian.brecht@web.de";
                txtPassword.Text = "WrongPassword";
                chkRememberMe.Checked = true;

                expected = "Invalid login attempt";

                //Act
                p_page.Invoke("LogIn", btnLogin, EventArgs.Empty);

                //Assert
                Assert.AreEqual(expected, LitFailureText.Text);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message + "\n" + ex.StackTrace);
                ErrorLog.logException(ex);
            }
        }

        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/Account/Login.aspx")]
        public void Test_LogIn_UnconfirmedEMail()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);
                String expected;

                TextBox txtEmail = (TextBox)ControlFinder.FindControl(page, "Email") as TextBox;
                TextBox txtPassword = (TextBox)ControlFinder.FindControl(page, "Password") as TextBox;
                CheckBox chkRememberMe = (CheckBox)ControlFinder.FindControl(page, "RememberMe") as CheckBox;
                Button btnLogin = (Button)ControlFinder.FindControl(page, "LoginButton") as Button;
                Literal LitFailureText = (Literal)ControlFinder.FindControl(page, "FailureText") as Literal;

                txtEmail.Text = "fabian.brecht.2@web.de";
                txtPassword.Text = "1228BBdB8;";
                chkRememberMe.Checked = true;

                expected = "Invalid login attempt. You must have a confirmed email address. Enter your email and password, then press 'Resend Confirmation'.";

                //Act
                p_page.Invoke("LogIn", btnLogin, EventArgs.Empty);

                //Assert
                Assert.AreEqual(expected, LitFailureText.Text);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message + "\n" + ex.StackTrace);
                ErrorLog.logException(ex);
            }
        }

    }

}
