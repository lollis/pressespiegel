﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Owin;
using Owin;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using RssService;

namespace RssService.Test
{
    [TestClass]
    public class TestRegister
    {
        private Page page;
        private TestContext _testContext;
        public TestContext TestContext
        {

            get { return _testContext; }

            set { _testContext = value; }

        }

        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/Account/Register.aspx")]
        public void Test_CreateUser_Click_NoInput()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);
                String expected = "Invalid login attempt";

                String expected = "Invalid login attempt>. Tatsächlich:<Passwords must be at least 6 characters. " +
                    "Passwords must have at least one non letter or digit character. Passwords must have at least one digit ('0'-'9'). " +
                    "Passwords must have at least one lowercase ('a'-'z'). Passwords must have at least one uppercase ('A'-'Z').";
                
                TextBox txtUserName = (TextBox)ControlFinder.FindControl(page, "UserName") as TextBox;
                TextBox txtEmail = (TextBox)ControlFinder.FindControl(page, "Email") as TextBox;
                TextBox txtPassword = (TextBox)ControlFinder.FindControl(page, "Password") as TextBox;
                TextBox txtConfirmPassword = (TextBox)ControlFinder.FindControl(page, "ConfirmPassword") as TextBox;
                Button btnLogin = (Button)ControlFinder.FindControl(page, "RegisterButton") as Button;
                Literal LitErrorMessage = (Literal)ControlFinder.FindControl(page, "ErrorMessage") as Literal;

                //Act
                p_page.Invoke("CreateUser_Click", btnLogin, EventArgs.Empty);

                //Assert
                Assert.AreEqual(expected, LitErrorMessage.Text);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message + "\n" + ex.StackTrace);
                ErrorLog.logException(ex);
            }
        }

        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/Account/Register.aspx")]
        public void Test_CreateUser_Click_AlreadyRegistered()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);
                String expected = "Name Fabian is already taken.";

                TextBox txtUserName = (TextBox)ControlFinder.FindControl(page, "UserName") as TextBox;
                TextBox txtEmail = (TextBox)ControlFinder.FindControl(page, "Email") as TextBox;
                TextBox txtPassword = (TextBox)ControlFinder.FindControl(page, "Password") as TextBox;
                TextBox txtConfirmPassword = (TextBox)ControlFinder.FindControl(page, "ConfirmPassword") as TextBox;
                Button btnLogin = (Button)ControlFinder.FindControl(page, "RegisterButton") as Button;
                Literal LitErrorMessage = (Literal)ControlFinder.FindControl(page, "ErrorMessage") as Literal;

                txtUserName.Text = "Fabian";
                txtEmail.Text = "fabian.brecht@web.de";
                txtPassword.Text = "Passwort1;";
                txtConfirmPassword.Text = "Passwort1";

                //Act
                p_page.Invoke("CreateUser_Click", btnLogin, EventArgs.Empty);

                //Assert
                Assert.AreEqual(expected, LitErrorMessage.Text);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message + "\n" + ex.StackTrace);
                ErrorLog.logException(ex);
            }
        }

    }

}

