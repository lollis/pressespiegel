﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.UI;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Web.UI.WebControls;
using RssService.SqlLinq;
using System.Collections.Generic;

namespace TestSuite.UnitTests
{
    [TestClass]
    public class UTestPressreview
    {

        /// <summary>
        /// Before you run tests on a new Sytem you have to change the AspNetDevelopmentServerHost string.
        /// For that change the string in the AspNetDevelopmentServerHost-Attribute above a TestCase.
        /// This string hast to point to the Folder with the Pressespiegel Project folder in it 
        /// (the folder for the pressespiegel.csproj project, not the pressespiegel solution)
        /// </summary>


        private Page page;
        private TestContext _testContext;
        public TestContext TestContext
        {

            get { return _testContext; }

            set { _testContext = value; }

        }


        /// <summary>
        /// This Method tests that it is not possible to create a Profile without input,
        /// and that a reasonable error message is shown
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/PressReview/AddProfile.aspx")]
        public void Test_AddProfile_NoInput()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);
                String expected = "Bitte geben Sie einen Namen für ihr Profil ein.";

                Button btnCreateProfile = (Button)ControlFinder.FindControl(page, "btnCreateProfile") as Button;
                Label txtFailure = (Label)ControlFinder.FindControl(page, "txtFailure") as Label;

                //Act
                p_page.Invoke("Page_Load", page, EventArgs.Empty);
                p_page.Invoke("btnCreateProfile_Click", btnCreateProfile, EventArgs.Empty);

                Assert.AreEqual(expected, txtFailure.Text);
            }
            catch (Exception ex) 
            {
                Assert.Fail(ErrorLog.formatException(ex));
                ErrorLog.logException(ex);
            }
        }

        /// <summary>
        /// This Method tests the AddProfile function with valid input
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/PressReview/EditProfile.aspx")]
        public void Test_AddProfile_ValidInput()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);
                String expected = "";

                TextBox txtBoxNewProfileName = (TextBox)ControlFinder.FindControl(page, "txtBoxNewProfileName") as TextBox;
                CheckBox chkService1 = (CheckBox)ControlFinder.FindControl(page, "chkService1") as CheckBox;
                TextBox TextBox0 = (TextBox)ControlFinder.FindControl(page, "TextBox0") as TextBox;
                TextBox txtMailIntervall = (TextBox)ControlFinder.FindControl(page, "txtMailIntervall") as TextBox;
                Button btnCreateProfile = (Button)ControlFinder.FindControl(page, "btnCreateProfile") as Button;
                Label txtFailure = (Label)ControlFinder.FindControl(page, "txtFailure") as Label;

                txtBoxNewProfileName.Text = "Test Profil";
                TextBox0.Text = "TestSuchbegriff";
                chkService1.Checked = true;
                txtMailIntervall.Text = "Mo, Di, Mi, Do, Fr ?07:00;20:00";

                //Act
                p_page.Invoke("btnCreateProfile_Click", btnCreateProfile, EventArgs.Empty);

                //Assert
                Assert.AreEqual(expected, txtFailure.Text);
            }
            catch (Exception ex)
            {
                Assert.Fail(ErrorLog.formatException(ex));
                ErrorLog.logException(ex);
            }
        }

        /// <summary>
        /// This Method tests the AddProfile Function with no valid name
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/PressReview/EditProfile.aspx")]
        public void Test_AddProfile_NoName()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);
                String expected = "Bitte geben Sie einen Namen für ihr Profil ein.";

                TextBox txtProfil_Name = (TextBox)ControlFinder.FindControl(page, "txtProfil_Name") as TextBox;
                TextBox TextBox0 = (TextBox)ControlFinder.FindControl(page, "TextBox0") as TextBox;
                CheckBox chkService1 = (CheckBox)ControlFinder.FindControl(page, "chkService1") as CheckBox;
                TextBox txtMailIntervall = (TextBox)ControlFinder.FindControl(page, "txtMailIntervall") as TextBox;
                Button btnCreateProfile = (Button)ControlFinder.FindControl(page, "btnCreateProfile") as Button;
                Label txtFailure = (Label)ControlFinder.FindControl(page, "txtFailure") as Label;

                txtProfil_Name.Text = "";
                TextBox0.Text = "Test";
                chkService1.Checked = true;
                txtMailIntervall.Text = "Mo, Di, Mi, Do, Fr ?07:00;20:00";

                //Act
                p_page.Invoke("Page_Load", page, EventArgs.Empty);
                p_page.Invoke("btnCreateProfile_Click", btnCreateProfile, EventArgs.Empty);

                //Assert
                Assert.AreEqual(expected, txtFailure.Text);
            }
            catch (Exception ex)
            {
                Assert.Fail(ErrorLog.formatException(ex));
                ErrorLog.logException(ex);
            }
        }

        // <summary>
        /// This Method tests the ShowNews Function
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/PressReview/ShowProfile.aspx")]
        public void Test_ShowNews()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);

                //Act
                p_page.Invoke("Page_Load", page, EventArgs.Empty);

                //Assert
                Assert.IsTrue(true);
                // Die Ergebnisse für das Profil werden direkt beim laden der Seite angezeigt, 
                // darum gilt dieser Test als bestanden wenn keine Exception ausgelöst wird
            }
            catch (Exception ex)
            {
                Assert.Fail(ErrorLog.formatException(ex));
                ErrorLog.logException(ex);
            }
        }

        // 5. Story

        // <summary>
        /// This Method tests the Edit Search Profile Function
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/PressReview/EditProfile.aspx")]
        public void Test_EditProfile_EditName()
        {
            try
            {
                List<UserProfileProvider> ListProviders;

                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);
                UserProfile userProfile = (UserProfile)p_page.GetField("currentProfile");
                Image img = new Image();
                String expected_Name = "New Profile Name";
                String expected_Logo = Convert.ToBase64String(new FileUpload().FileBytes);
                String expected_SearchTerm = "New SearchTerm";
                UserProfileTag expected_Tag = new UserProfileTag { TagId = (long)p_page.Invoke("Tag", expected_SearchTerm), UserProfileId = userProfile.Id };  
                bool result_Name;
                bool result_LogoUrl;
                bool result_SearchTerms;
                bool result_Providers;

                TextBox txtBoxNewProfileName = (TextBox)ControlFinder.FindControl(page, "txtBoxNewProfileName") as TextBox;
                TextBox TextBox0 = (TextBox)ControlFinder.FindControl(page, "TextBox0") as TextBox;
                FileUpload fileProfileLogo2 = (FileUpload)ControlFinder.FindControl(page, "fileProfileLogo2") as FileUpload;
                Table cbProviders = (Table)ControlFinder.FindControl(page, "cbProviders") as Table;
                // The first provider checkbox in the provider table
                CheckBox chProviderCheckBox = (CheckBox)cbProviders.Rows[0].Cells[0].Controls[0] as CheckBox; 
                Button btnSaveProfileName = (Button)ControlFinder.FindControl(page, "btnSaveProfileName") as Button;
                Button btnSaveProfileLogo = (Button)ControlFinder.FindControl(page, "btnSaveProfileLogo") as Button;
                Button btnSaveSearchTerms = (Button)ControlFinder.FindControl(page, "btnSaveSearchTerms") as Button;
                Button btnSaveProviders = (Button)ControlFinder.FindControl(page, "btnSaveProviders") as Button;

                txtBoxNewProfileName.Text = "New Profile Name";
                TextBox0.Text = "New SearchTerm";
                fileProfileLogo2 = new FileUpload();
                chProviderCheckBox.Checked = true;

                //Act
                p_page.Invoke("Page_Load", page, EventArgs.Empty);
                p_page.Invoke("btnSaveProfileName_Click", btnSaveProfileName, EventArgs.Empty);
                p_page.Invoke("btnSaveProfileLogo_Click", btnSaveProfileLogo, EventArgs.Empty);
                p_page.Invoke("btnSaveSearchTerms_Click", btnSaveSearchTerms, EventArgs.Empty);
                p_page.Invoke("btnSaveProviders_Click", btnSaveProviders, EventArgs.Empty);

                result_Name = (userProfile.Name == expected_Name);
                result_LogoUrl = (userProfile.Logo == expected_Logo);
                result_SearchTerms = (userProfile.UserProfileTags.Contains(expected_Tag));
                result_Providers = (userProfile.UserProfileProviders.Count > 0);


                //Assert
                Assert.IsTrue(result_Name && result_LogoUrl && result_SearchTerms && result_Providers);
            }
            catch (Exception ex)
            {
                Assert.Fail(ErrorLog.formatException(ex));
                ErrorLog.logException(ex);
            }
        }

        // <summary>
        /// This Method tests the Delete Search Profile Function
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/PressReview/EditProfile.aspx")]
        public void Test_EditProfile_DeleteProfile()
        {
            try
            {
                //Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);

                Button btnDeleteProfileCommit = (Button)ControlFinder.FindControl(page, "btnDeleteProfileCommit") as Button;

                //Act
                p_page.Invoke("Page_Load", page, EventArgs.Empty);
                p_page.Invoke("btnDeleteProfileCommit_Click", btnDeleteProfileCommit, EventArgs.Empty);

                //Assert
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail(ErrorLog.formatException(ex));
                ErrorLog.logException(ex);
            }
        }

        //// <summary>
        ///// This Method tests the Show All Email Profiles Function
        ///// </summary>
        //[TestMethod()]
        //[HostType("ASP.NET")]
        //[AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        //[UrlToTest("http://localhost:51553/PressReview/EditReport.aspx")]
        //public void Test_EditEmailProfile()
        //{
        //    try
        //    {
        //        // Arrange
        //        page = TestContext.RequestedPage;
        //        PrivateObject p_page = new PrivateObject(page);

        //        Table profilesTable = (Table)ControlFinder.FindControl(page, "profilesTable") as Table;

        //        // Act
        //        p_page.Invoke("Page_Load", page, EventArgs.Empty);
        //        // Die verfügbaren Profile werden bereits beim laden der Seite angezeigt

        //        // Assert
        //        Assert.IsTrue(profilesTable.Rows.Count > 0);
        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.Fail(ErrorLog.formatException(ex));
        //        ErrorLog.logException(ex);
        //    }
        //}

        // <summary>
        /// This Method tests the Edit Mail Intervall Function
        /// </summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost(@"C:\Users\Fabian\Documents\Source\Repos\pressespiegel\Pressespiegel\Pressespiegel")]
        [UrlToTest("http://localhost:51553/PressReview/CreateReport.aspx")]
        public void Test_EditEmailProfile_AddMailTime()
        {
            try
            {
                // Arrange
                page = TestContext.RequestedPage;
                PrivateObject p_page = new PrivateObject(page);
                string expected = "";

                Button btnTuesday = (Button)ControlFinder.FindControl(page, "btnTuesday") as Button;
                Button btnWednesday = (Button)ControlFinder.FindControl(page, "btnWednesday") as Button;
                Button btnThursday = (Button)ControlFinder.FindControl(page, "btnThursday") as Button;
                TextBox txtBoxTime1 = (TextBox)ControlFinder.FindControl(page, "txtBoxTime1") as TextBox;
                Table cbProfiles = (Table)ControlFinder.FindControl(page, "cbProfiles") as Table;
                CheckBox cb = (CheckBox)cbProfiles.Rows[0].Cells[0].Controls[0];
                Button btnCreateReport = (Button)ControlFinder.FindControl(page, "btnCreateReport_Click") as Button;
                Label txtFailure = (Label)ControlFinder.FindControl(page, "txtFailure") as Label;
                
                cb.Checked = true;
                txtBoxTime1.Text = "14:15";

                // Act
                p_page.Invoke("Page_Load", page, EventArgs.Empty);
                p_page.Invoke("btnTuesday_Click", btnTuesday, EventArgs.Empty);
                p_page.Invoke("btnWednesday_Click", btnWednesday, EventArgs.Empty);
                p_page.Invoke("btnThursday_Click", btnThursday, EventArgs.Empty);

                p_page.Invoke("btnCreateReport_Click", btnCreateReport, EventArgs.Empty);
                


                // Assert
                Assert.Equals(txtFailure.Text, expected);
            }
            catch (Exception ex)
            {
                Assert.Fail(ErrorLog.formatException(ex));
                ErrorLog.logException(ex);
            }
        }






    }
}
