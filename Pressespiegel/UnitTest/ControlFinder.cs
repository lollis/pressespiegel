﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace TestSuite
{
    /// <summary>
    /// A class that provides static methods to find Web Controls in a Web page
    /// </summary>
    class ControlFinder
    {

        public static Control FindControl(Page page, string id)
        {
            Control FoundControl;

            foreach (Control c in page.Controls)
            {
                if (c.ID == id)
                {
                    return c;
                }

                FoundControl = FindControlRecursive(c, id);
                if (FoundControl != null) return FoundControl;
            }

            return null;
        }


        private static Control FindControlRecursive(Control root, string id)
        {
            if (root.ID == id)
            {
                return root;
            }

            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, id);
                if (t != null)
                {
                    return t;
                }
            }
            return null;
        }

    }
}
