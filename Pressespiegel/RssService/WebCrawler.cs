﻿using HtmlAgilityPack;
using System;
using System.Net;
using System.ServiceModel.Syndication;
using System.Text;
using System.Windows.Controls;
using System.Xml;
using System.Linq;
using System.Collections.Generic;

namespace RssService
{
    /// <summary>
    /// Durchsucht Webseiten nach xpath
    /// </summary>
    public class WebCrawler
    {
        /// <summary>
        /// lädt websete der url herunter und löst nach xpath auf, gibt inhalt des xpaths zurück
        /// </summary>
        /// <param name="url"></param>
        /// <param name="xPath"></param>
        /// <returns></returns>
        public string GetContentFromRssFeed(string url, string xPath)
        {
            if (string.IsNullOrEmpty(xPath))
                return null;

            using (var client = new WebClient())
            {
                string htmlPage = client.DownloadString(url);
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(htmlPage);
                var nodes = doc.DocumentNode.SelectNodes(xPath);
                if (nodes == null)
                    throw new Exception("Node Ist null");
                StringBuilder fullContent = new StringBuilder();
                foreach (HtmlNode n in nodes)
                {
                    fullContent.Append(n.InnerText.ToString());
                }
                //remove javascript tags
                doc.LoadHtml(fullContent.ToString());
                fullContent.Clear();
                foreach(var node in doc.DocumentNode.ChildNodes)
                {
                    fullContent.Append(node.InnerText);
                }
                byte[] bitFullContent = Encoding.Default.GetBytes(fullContent.ToString());
                return Encoding.UTF8.GetString(bitFullContent);
            }


        }

        /// <summary>
        /// lädt webseite der URL herunter und löst diese nach den XPaths auf, gibt inhalt des xpaths zurück
        /// </summary>
        /// <param name="url">URL zur Webseite</param>
        /// <param name="xPaths">Format: xpath§vorherigerXPath§vorherigerXPath§attribut, attribut ist letztes element</param>
        /// <returns></returns>
        public string GetPictureFromRssFeed(string url, string[] xPaths)
        {
            if (xPaths.Length == 0 || string.IsNullOrEmpty(xPaths[0]))
                return null;
            using (var client = new WebClient())
            {
                string htmlPage = client.DownloadString(url);
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(htmlPage);
                var node = doc.DocumentNode.SelectNodes(xPaths[0])[0];
                //letzter eintrag ist attribut
                for (int i = 1; i < xPaths.Length - 1; i++)
                {
                    doc.LoadHtml(node.InnerHtml);
                    node = doc.DocumentNode.SelectNodes(xPaths[i])[0];
                }
                string link = node.Attributes[xPaths[xPaths.Length - 1]].Value;
                byte[] bitFullContent = Encoding.Default.GetBytes(link);
                return Encoding.UTF8.GetString(bitFullContent);
            }
        }

    }
}

