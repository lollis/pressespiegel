﻿using Nest;
using RssService.SqlLinq;
using RssService;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Pressespiegel.DataProvider
{
    public class ElasticSearch
    {
        ElasticClient client;

        internal SearchItem SearchItem
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }
        public ElasticSearch()
        {
            Connect();
        }
        /// <summary>
        /// connects to the ElasticSearch node on localhost
        /// </summary>
        private void Connect()
        {
            Uri node = new Uri(ElasticHandler.DEFAULT_ELASTIC_HOST_URL);
            ConnectionSettings settings = new ConnectionSettings(node).DefaultIndex(ElasticHandler.DEFAULT_INDEX)
                .InferMappingFor<RssItem>(m => m
                .IdProperty(p => p.Id)
            );
            client = new ElasticClient(settings);
        }

        /// <summary>
        /// Search for one tag on one indize (freie suche)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        internal List<RssItem> Search(SearchItem item)
        {
            if (client == null)
                Connect();
            var res = client.Search<RssItem>(s => s
             .From(0)
             .Size(100)
             .Index(item.Service.ToLower() + "*")
               .Query(q => q
                   .QueryString(qs => qs
                        .DefaultOperator(Operator.And)
                        .Fields(f => f.Field("title").Field("content"))
                        .Query(item.Content)
                    )
               )
               .Sort(ss => ss.Descending(p => p.Date))
            );

            List<RssItem> items = new List<RssItem>();
            items.AddRange(res.Documents);
            return items;
        }

        /// <summary>
        /// Search for the artikels matching the Searchitem
        /// </summary>
        /// <param name="item"></param>
        /// <returns>the full artikels </returns>
        internal List<RssItem> Search2(SearchItem item)
        {
            if (client == null)
                Connect();
            string indices = CreateIndexString(item.ServiceList);
            string tags = CreateTagString(item.TagList);

            var res = client.Search<RssItem>(s => s
               .From(0)
               .Size(500)
               //.Index("*")
               .Index(indices)
                .Query(q => q
                      .QueryString(qs => qs
                        .DefaultOperator(Operator.And)
                        .Fields(f => f.Field("title").Field("content"))
                        .Query(tags)

                    )

                )
                .Sort(srt => srt
                    .Descending(p => p.Date)
                //  .Ascending(a=> a.ServiceName)
                )
            );
            List<RssItem> items = new List<RssItem>();
            items.AddRange(res.Documents);
            return items;
        }
        /// <summary>
        /// Search all item matching the Searchitem item.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>the ids of matching artikels</returns>
        internal List<string> Search3(SearchItem item)
        {
            if (client == null)
                Connect();
            string indices = CreateIndexString(item.ServiceList);
            string tags = CreateTagString(item.TagList);

            var res = client.Search<RssItem>(s => s
               .From(0)
               .Size(500)
               .Index(indices)
                .Query(q => q
                      .QueryString(qs => qs
                        .DefaultOperator(Operator.And)
                        .Fields(f => f.Field("title").Field("content").Field("fullContent"))
                        .Query(tags)
                    )
                )
                .Sort(srt => srt
                    .Descending(p => p.Date)
                )
            );
            List<string> ids = new List<string>();
            foreach (IHit<RssItem> hit in res.Hits)
            {
                ids.Add(hit.Id);
            }
            // res.Hits.ElementAt

            //string id = res.Hits.ToList();
            return ids;
        }

        /// <summary>
        /// Reads the ids out of the DB and
        /// gets the artikel in ElasticSearch by its ids
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public List<RssItem> SearchByID(long profileId)
        {
            List<RssItem> items = new List<RssItem>();
            IndizeHandler handler = new IndizeHandler(profileId);
            string idStr = handler.Indizes;
            List<string> ids = new List<string>();
            if (!string.IsNullOrEmpty(idStr))
            {
                ids = idStr.Split(';').ToList<string>();
                ids.RemoveAt(ids.IndexOf(""));
            }
            else
                return null;


            var res = client.Search<RssItem>(s => s
                     .From(0)
                     .Size(30)
                     .Index("*")
                     .Query(q => q
                            .Ids(i => i.
                            Values(ids)
                         )
                     )
                     .Sort(srt => srt
                       .Descending(p => p.Date)
                     )
                   );



            items.AddRange(res.Documents);
            return items;
        }

        public List<RssItem> SearchArchive(string indizes)
        {
            List<RssItem> items = new List<RssItem>();
            List<string> ids = new List<string>();
            if (!string.IsNullOrEmpty(indizes))
            {
                ids = indizes.Split(';').ToList<string>();
                ids.RemoveAt(ids.IndexOf(""));
            }
            else
                return null;

            var res = client.Search<RssItem>(s => s
                     .From(0)
                     .Size(30)
                     .Index("*")
                     .Query(q => q
                            .Ids(i => i.
                            Values(ids)
                         )
                     )
                     .Sort(srt => srt
                       .Descending(p => p.Date)
                     )
                   );
            items.AddRange(res.Documents);
            return items;
        }
        /// <summary>
        /// creates a string with all Search Terms.
        /// </summary>
        /// <param name="tagList"></param>
        /// <returns></returns>
        private string CreateTagString(List<string> tagList)
        {
            string tags = null;
            foreach (string tag in tagList)
            {
                if (tags == null)
                {
                    tags = tag;
                }
                else
                {
                    tags = tags + " OR " + tag;
                }
            }
            return tags;
        }

        /// <summary>
        /// creates an kommaseperated string (indizestring ) with all providers in the list
        /// </summary>
        /// <param name="indexList"></param>
        /// <returns>string </returns>
        private string CreateIndexString(List<string> indexList)
        {
            string indices = null;
            if (indexList.Count == 0)
            {
                indices = "noindex";
            }
            foreach (string list in indexList)
            {
                string index = list.ToLower() + "-" + System.DateTime.Now.ToString("dd.MM.yyyy");
                if (client.IndexExists(index).Exists) { 
                    if (indices == null)
                    {
                        indices = index;
                    }
                    else
                    {
                        indices = indices + "," + index;
                    }
                }
               // Console.Write(indices);
            }
            if (indices == null)
            {
                indices = "noindex";
            }
            return indices;
        }

        /// <summary>
        /// Get all tags and Provideres from db and Search for them in ElasticSearch
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns>the full artikel for that profile</returns>
        public List<RssItem> GetProfileContent(long profileId)
        {
            List<string> profileTags;
            List<string> profileProvider;

            using (var db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString))
            {
                var tags = from t in db.Tags
                           join upt in db.UserProfileTags on t.Id equals upt.TagId
                           where upt.UserProfileId == profileId
                           select t.TagName;
                profileTags = tags.ToList<string>();

                var prov = from p in db.Providers
                           join upp in db.UserProfileProviders on p.Id equals upp.ProviderId
                           where upp.UserProfileId == profileId
                           select p.Name;
                profileProvider = prov.ToList<string>();
            }



            // List<string> profileTags = new List<string>();
            //List<string> profileProvider = new List<string>();

            SearchItem item = new SearchItem(profileProvider, profileTags);

            //Liste von Tags
            // "SELECT TagName FROM Tag t, UserProfileTag ut WHERE t.Id = ut.TagId AND ut.UserProfileId = " + userId

            //Liste von Provider
            //SELECT p.Name FROM Provider p, UserProfilProvider upp WHERE p.Id = upp.ProviderId AND upp.UserProfilId = " +userId 
            return Search2(item);
        }
        /// <summary>
        /// Read all tags and providers from db 
        /// and Search for them in ElasticSearch
        /// </summary>
        /// <param name="profileId">the id of the profile</param>
        /// <returns>the ids of the artikels in a string List</returns>
        public List<string> GetProfileContentId(long profileId)
        {
            List<string> profileTags;
            List<string> profileProvider;

            using (var db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString))
            {
                var tags = from t in db.Tags
                           join upt in db.UserProfileTags on t.Id equals upt.TagId
                           where upt.UserProfileId == profileId
                           select t.TagName;
                profileTags = tags.ToList<string>();

                var prov = from p in db.Providers
                           join upp in db.UserProfileProviders on p.Id equals upp.ProviderId
                           where upp.UserProfileId == profileId
                           select p.Name;
                profileProvider = prov.ToList<string>();
            }



            // List<string> profileTags = new List<string>();
            //List<string> profileProvider = new List<string>();

            SearchItem item = new SearchItem(profileProvider, profileTags);

            return Search3(item);
        }

        internal List<RssItem> SearchById(List<string> IdList)
        {
            List<RssItem> resList = new List<RssItem>();

            var res = client.Get<RssItem>(IdList.ElementAt(0));
            //res.Fields.Value<string>();
            //FieldValue<string>(p => p.title);
            return resList;
        }

    }
}