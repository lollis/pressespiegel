﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pressespiegel.DataProvider
{
    public class SearchItem
    {
        internal string Service { get; set; } = "";
       // internal string Title { get; set; } = "";
        internal string Content { get; set; } = "";

        internal List<string> ServiceList { get; set; } = null;
        internal List<string> TagList { get; set; } = null;
        
        public SearchItem(string service, string content)
        {
            this.Service = service;
            //this.Title = title;
            this.Content = content;
        }
        public SearchItem(List<string> ServiceList, List<string> TagList)
        {
            this.ServiceList = new List<string>(ServiceList);
            this.TagList = new List<string>(TagList);
        }

    }
}