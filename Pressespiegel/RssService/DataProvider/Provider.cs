﻿using RssService;
using RssService.SqlLinq;
using System.Collections.Generic;
using System.Linq;

namespace Pressespiegel.DataProvider
{
    public class Provider
    {
        private string userId;
        /// <summary>
        /// Creates the class
        /// </summary>
        /// <param name="userId">The current User User.Identity.GetUserId<int>()</param>
        public Provider(string userId)
        {
            this.userId = userId;
            ElasticSearch = new ElasticSearch();
        }

        public ElasticSearch ElasticSearch
        {
            get; set;
        }

        public SearchItem SearchItem
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public RssItem RssItem
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        /// <summary>
        /// Returns all Provider Sites which the user has selected
        /// </summary>
        /// <returns></returns>
        public List<UserProfile> GetUserSites()
        {
            UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
            List<UserProfile> profiles = new List<UserProfile>();
            profiles.AddRange(from t in db.UserProfiles
                              where System.Data.Linq.SqlClient.SqlMethods.Like(t.UserId, userId)
                              select t);
            profiles.AddRange(from sp in db.SharedProfiles
                              join up in db.UserProfiles
                              on sp.ProfileId equals up.Id
                              where sp.UserId == userId
                              select up);
            return profiles;
        }

        /// <summary>
        /// Returns all EmailOrders which the user has entered for any profile
        /// </summary>
        /// <returns></returns>
        public List<EmailOrder> GetEmailOrders()
        {
            UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
            List<EmailOrder> orders = new List<EmailOrder>();
            orders.AddRange(from eo in db.EmailOrders
                            where System.Data.Linq.SqlClient.SqlMethods.Like(eo.UserId, userId)
                            select eo);
            return orders;
        }

        public List<RssItem> Search(SearchItem item)
        {
            ElasticSearch search = new ElasticSearch();
            return search.Search(item);
        }

        /// <summary>
        /// Returns all Tags which the user has entered in a profile
        /// </summary>
        /// <returns></returns>
        public List<Tag> GetProfileTags(long profileId)
        {
            UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
            List<Tag> profileTags = new List<Tag>();
            profileTags.AddRange(from upt in db.UserProfileTags
                                 join t in db.Tags on upt.TagId equals t.Id
                                 where upt.UserProfileId == profileId
                                 select t);
            return profileTags;
        }

        /// <summary>
        /// Returns all Providers which the user has selected in a profile
        /// </summary>
        /// <returns></returns>
        public List<RssService.SqlLinq.Provider> GetProfileProviders(long profileId)
        {
            UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
            List<RssService.SqlLinq.Provider> providers = new List<RssService.SqlLinq.Provider>();
            providers.AddRange(from upp in db.UserProfileProviders
                               join p in db.Providers on upp.ProviderId equals p.Id
                               where upp.UserProfileId == profileId
                               select p);
            return providers;
        }
    }
}