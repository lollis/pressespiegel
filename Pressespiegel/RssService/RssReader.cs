﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.ServiceModel.Syndication;
using System.Xml;

namespace RssService
{
    class RssReader
    {
        public RssItem RssItem
        {
            get
            {
                return new RssItem();
            }

            set
            {
            }
        }

        /// <summary>
        /// get a List of Rssitems form the url
        /// </summary>
        /// <param name="name">Name of the mediaservice</param>
        /// <param name="feedUrl">url of the mediaservice</param>
        /// <returns>a list of the current RssItems</returns>
        internal List<RssItem> GetItems(ServiceItem item)
        {
            XDocument doc;
            WebCrawler crawler = new WebCrawler();
            try
            {
                doc = XDocument.Load(item.Url);
            }
            catch (Exception e)
            {
                //Console.WriteLine(e);
                return null;
            }
            List<RssItem> items = new List<RssItem>();
            SyndicationFeed feeds = SyndicationFeed.Load(XmlReader.Create(item.Url));
            RssItem newItem;
            foreach (var feed in feeds.Items)
            {
                newItem = new RssItem();
                newItem.Title = RemoveSpaceAndTab(feed.Title.Text);
                if (feed.Summary != null)
                    newItem.Content = RemoveSpaceAndTab(feed.Summary.Text);
                if (feeds.Links != null && feed.Links.Count != 0)
                    newItem.Url = feed.Links[0].Uri.AbsoluteUri;
                newItem.Date = feed.PublishDate.DateTime;
                newItem.ServiceName = item.Service;
                items.Add(newItem);
            }

            //items.AddRange(from x in doc.Descendants("channel").Descendants("item")
            //               select new RssItem()
            //               {
            //                   Title = RemoveSpaceAndTab(x.Descendants("title").Single().Value),
            //                   Content = RemoveSpaceAndTab(x.Descendants("description").Single().Value),
            //                   Url = x.Descendants("link").Single().Value,
            //                   Date = DateTime.Parse(x.Descendants("pubDate").Single().Value),
            //                   ServiceName = item.Service
            //               });
            Parallel.ForEach(items, i =>
            {
                try
                {
                    i.FullContent = crawler.GetContentFromRssFeed(i.Url, item.XPathContent);
                }
                catch (Exception ex)
                {
                }
                if (item.XPathPicture != null && !string.IsNullOrEmpty(item.XPathPicture))
                    try
                    {
                        i.BildUrl = crawler.GetPictureFromRssFeed(i.Url, item.XPathPicture.Split('§'));
                    }
                    catch (Exception ex)
                    {

                    }
            });
            //foreach (RssItem i in items)
            //{
            //    try
            //    {
            //        i.FullContent = crawler.GetContentFromRssFeed(i.Url, item.XPathContent);
            //        i.BildUrl = crawler.GetPictureFromRssFeed(i.Url, item.XPathPicture.Split('§'));
            //    }catch(Exception ex)
            //    {

            //    }
            //}
            return items;
        }

        /// <summary>
        /// removes Space,Tabs and new Lines from str
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private String RemoveSpaceAndTab(String str)
        {
            str = str.Trim(new char[] { '\n', '\r', '\t' });
            return str;
        }
    }
}
