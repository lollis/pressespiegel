﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RssService.Archiv
{
    public class ArchivItem
    {
        public string indizes { get; set; }
        public DateTime? dateTime { get; set; }

        public ArchivItem(string indizes, DateTime? dateTime)
        {
            this.indizes = indizes;
            this.dateTime = dateTime;
        }

       
    }
}
