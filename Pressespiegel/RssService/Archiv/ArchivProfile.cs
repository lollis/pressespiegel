﻿using RssService.SqlLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RssService.Archiv
{
    public class ArchivProfile
    {
        public long profileId { get; set; }
        public List<ArchivItem> archivItems { get; set; }

        public ArchivProfile(long profileId)
        {
            this.profileId = profileId;
            archivItems = new List<ArchivItem>();
        }

        public void loadArchiv()
        {
            UserProfileViewDataContext context = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
            var res =   (
                         from t in context.Archivs
                         where t.ProfileId == profileId
                         orderby t.TimeStamp descending
                         select new { indizes = t.Indizes, timestamp = t.TimeStamp }
                        );
            foreach(var item in res)
            {              
                ArchivItem items = new ArchivItem(item.indizes, item.timestamp);
                this.archivItems.Add(items);
            }
            

        }
        

        public void addArchivItem(ArchivItem item)
        {
            this.archivItems.Add(item);
            var db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
            db.Archivs.InsertOnSubmit(new SqlLinq.Archiv()
            {
                ProfileId = profileId,
                Indizes = item.indizes,
                TimeStamp = item.dateTime
            });
            db.SubmitChanges();
        }


        public void writeCurrentIndizes(DateTime? orderTime)
        {
            UserProfileViewDataContext context = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
            var res = (from t in context.UserProfiles
                       where t.Id == this.profileId
                       select t.Elasticsearch_Indizes);
            var list = res.ToList();
            

            ArchivItem item = new ArchivItem(list[0], orderTime);

            addArchivItem(item);
        }
    }
}
