﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RssService
{
    class ArchiveItem
    {
        internal DateTime date{get; set;}
        internal string indizes { get; set; }

        internal ArchiveItem(DateTime date, string indizes)
        {
            this.date = date;
            this.indizes = indizes;
        }
    }
}
