﻿
using RssService.SqlLinq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Linq;
using System.Timers;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace RssService
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private int _updateTime;
        public int UpdateTime
        {
            get { return _updateTime; }
            set
            {
                Console.WriteLine("Update Time: " + value + "min");
                _updateTime = value;
                timer.Interval = new TimeSpan(0, value, 0).TotalMilliseconds;
                if (worker != null && !worker.IsBusy)
                    worker.RunWorkerAsync();
                if (null != this.PropertyChanged)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("UpdateTimeProperty"));
                }

            }
        }


        private TextWriter _writer = null;
        
        public static string ConnectionString
        {
            get
            {
                return @"Data Source=.\SQLEXPRESS;Initial Catalog=Pressespiegel_App;User Id=PressespiegelUser; Password=Pressespiegel_User"; ;
            }
        }
        
        //public static string ConnectionString { get { return @"Data Source=(LocalDb)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\aspnet-Pressespiegel.mdf;Integrated Security = True"; } }
        internal ElasticHandler e { get; private set; }
        internal RssReader reader { get; private set; }

        private Timer timer;

        private BackgroundWorker worker;

        public event PropertyChangedEventHandler PropertyChanged;

        public MainWindow()
        {

            AppDomain.CurrentDomain.SetData("DataDirectory", GetDataDirectoy());
            timer = new Timer();
            InitializeComponent();
            this.UpdateTime = 5;
            TopLevelContainer.DataContext = this;
            _writer = new TextBoxStreamWriter(txtConsole);
            Console.SetOut(_writer);

            Console.WriteLine("App Data Directory: " + AppDomain.CurrentDomain.GetData("DataDirectory"));
            Console.WriteLine("Connection String: " + RssService.MainWindow.ConnectionString);
            e = new ElasticHandler();
            reader = new RssReader();
            timer.Elapsed += Timer_Elapsed;
            worker = new BackgroundWorker();
            worker.DoWork += Worker_DoWork;
            timer.Start();
            InsertTestService();
            worker.RunWorkerAsync();
        }


        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            UpdateDataBase();
            Console.WriteLine("Worker finished");
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!worker.IsBusy)
                worker.RunWorkerAsync();
            else
                Console.WriteLine("worker was busy, will do the work next time");
        }

        private void UpdateDataBase()
        {
            List<ServiceItem> service = new ServiceHandler().GetServiceList();
            //List<RssItem> rssItems = new List<RssItem>();
            foreach (ServiceItem item in service)
            {
                //Console.WriteLine(item.Service + ", " +item.Url);
                List<RssItem> items = reader.GetItems(item);
                if (items != null)
                {
                    e.AddListToNode(items);
                }
            }
            new Filter().filterProfile();
        }

        private string GetDataDirectoy()
        {
            string mainDirectory = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).Split(new string[] { Assembly.GetExecutingAssembly().FullName.Split(',')[0] }, StringSplitOptions.None)[0];
            if (mainDirectory.EndsWith("\\"))
                return mainDirectory + "Pressespiegel\\App_Data\\";
            else
                return mainDirectory + "\\Pressespiegel\\App_Data\\";


        }

        private void InsertTestService()
        {
            List<Provider> toAdd = new List<Provider>();
            toAdd.Add(new Provider
            {
                Id = 0,
                Name = "TAGESSCHAU",
                Link = "http://www.tagesschau.de/xml/rss2",
                XPathContent = "//p[contains(@class, 'text small')] | //h2[contains(@class, 'subtitle small')]"
            });
            toAdd.Add(new Provider
            {
                Id = 1,
                Name = "FOCUS",
                Link = "http://rss.focus.de/fol/XML/rss_folnews.xml",
                XPathContent = "//div[contains(@class, 'textBlock')]",
                XPathPicture = "//div[contains(@class, 'mediaBlock ps-sidebar clearfix headmedia mediav2')]§//a[@href]§relx"
            });
            toAdd.Add(new Provider
            {
                Id = 2,
                Name = "SPIEGEL",
                Link = "http://www.spiegel.de/schlagzeilen/index.rss",
                XPathContent = "//div[contains(@class, 'article-section')]",
                XPathPicture = "//div[contains(@class, 'spArticleContent')]§//img[@class='spPanoImageTeaserPic']/@src§src"
            });
            AddProvider(toAdd);

            //try
            //{

            //SqlLinq.Provider prov = new Provider
            //{
            //    Id = 0,
            //    Name = "TAGESSCHAU",
            //    Link = "http://www.tagesschau.de/xml/rss2",
            //    XPathContent = "//p[contains(@class, 'text small')] | //h2[contains(@class, 'subtitle small')]"

            //};
            //UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
            //List<Provider> provList = new List<Provider>();
            //provList.AddRange(from t in db.Providers
            //                  where System.Data.Linq.SqlClient.SqlMethods.Like(t.Name, prov.Name)
            //                  select t);
            //if (provList.Count == 0)
            //    db.Providers.InsertOnSubmit(prov);
            //provList.Clear();


            //prov = new Provider
            //{
            //    Id = 1,
            //    Name = "FOCUS",
            //    Link = "http://rss.focus.de/fol/XML/rss_folnews.xml",
            //    XPathContent = "//div[contains(@class, 'textBlock')]",
            //    XPathPicture = "//div[contains(@class, 'mediaBlock ps-sidebar clearfix headmedia mediav2')]§//a[@href]§relx"
            //};

            //provList.AddRange(from t in db.Providers
            //                  where System.Data.Linq.SqlClient.SqlMethods.Like(t.Name, prov.Name)
            //                  select t);
            //if (provList.Count == 0)
            //    db.Providers.InsertOnSubmit(prov);
            //db.SubmitChanges();
            ////    db.Providers.InsertOnSubmit(prov);
            ////db.SubmitChanges();
            //}
            //catch (Exception ex)
            //{
            //    //Do nothing, because it's just for testing
            //}
        }

        private void AddProvider(List<Provider> provider)
        {
            UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
            foreach (Provider prov in provider)
            {
                try
                {
                    List<Provider> provList = new List<Provider>();
                    provList.AddRange(from t in db.Providers
                                      where System.Data.Linq.SqlClient.SqlMethods.Like(t.Name, prov.Name)
                                      select t);
                    if (provList.Count == 0)
                        db.Providers.InsertOnSubmit(prov);
                }
                catch (Exception ex)
                {

                }
            }
            db.SubmitChanges();
        }

        private void txtUpdateTime_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {

        }
    }

    public class TextBoxStreamWriter : TextWriter
    {
        TextBox _output = null;
        char lastValue;

        public TextBoxStreamWriter(TextBox output)
        {
            _output = output;
        }

        public override void Write(char value)
        {
            string text;
            if (lastValue.Equals('\n'))
                text = System.DateTime.Now.ToString("HH:mm:ss") + " |   " + value.ToString();
            else
                text = value.ToString();
            base.Write(value);
            if (Application.Current.Dispatcher.CheckAccess())
            {
                _output.AppendText(text.ToString());
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(
                  DispatcherPriority.Background,
                  new Action(() =>
                  {
                      _output.AppendText(text.ToString());
                      _output.ScrollToEnd();
                  }));
            }
            lastValue = value;
            //_output.AppendText(value.ToString()); // When character data is written, append it to the text box.
        }


        public override Encoding Encoding
        {
            get { return System.Text.Encoding.UTF8; }
        }
    }
}