﻿using Nest;
using System;

namespace RssService
{
    public class RssItem
    {

        public String Id { get; set; }
        public String Title { get; set;}
        public String Content { get; set;}
        public string FullContent { get; set; }
        public String Url { get; set; }
        public DateTime Date { get; set; }
        public String ServiceName { get; set; }
        public String BildUrl { get; set; }
      
        override
        public String ToString()
        {
            return string.Format("Title: '{0}', Content: '{1}', Url: '{2}', Date: '{3}', serviceName: '{4}', Bild: '{5}' "
                                 , Title, Content, Url, Date, ServiceName, BildUrl);
        }
    }
}
