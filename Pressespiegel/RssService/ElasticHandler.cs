﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RssService
{

    /// <summary>
    /// This Class connects to an Elasticsearch node
    /// and writes documents to the node.
    /// </summary>
    public class ElasticHandler
    {
        public const string DEFAULT_ELASTIC_HOST_URL = "http://localhost:9200";
        public const string DEFAULT_INDEX = "pressespiegel";
        private Uri node;
        private ConnectionSettings settings;
        private ElasticClient client;

        /// <summary>
        /// Konsturctor that creates a connection to the Elasticsearch node
        /// </summary>
        internal ElasticHandler()
        {
            ConnectToNode();

        }
        internal ElasticHandler(String url, String defaultIndex)
        {
            ConnectToNode();
        }

        public RssItem RssItem
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        /// <summary>
        /// Connects to a ElasticSearchNode on defuault address: localhost:9200
        /// and set the default Index to "pressespiegel"
        /// </summary>
        private void ConnectToNode()
        {
            node = new Uri(DEFAULT_ELASTIC_HOST_URL);
            settings = new ConnectionSettings(node).DefaultIndex(DEFAULT_INDEX);
            client = new ElasticClient(settings);


        }
        /// <summary>
        /// Connects to a ElasticSearchNode and set a default Index 
        /// </summary>
        /// <param name="url">connects on that url</param>
        /// <param name="defaultIndex">set the default index</param>
        private void ConnectToNode(String url, String defaultIndex)
        {
            node = new Uri(url);
            settings = new ConnectionSettings(node).DefaultIndex(defaultIndex);
            client = new ElasticClient(settings);
        }
        /// <summary>
        /// adds a Document to the Connected ElasticSearch node
        /// </summary>
        /// <param name="item">the Document which will be added</param>
        internal void AddFeedToNode(RssItem item)
        {
            
            String index = item.ServiceName.ToLower() + "-" + item.Date.ToString("dd.MM.yyyy");

            if (item.Date.ToString("dd.MM.yyyy").Equals(System.DateTime.Now.ToString("dd.MM.yyyy")))
            {
                if (!SearchTitle(item.Title, item.ServiceName, item.Date))
                {
                    var response = client.Index(item, idx => idx.Index(index).Id(item.Id));
                    if (response.OriginalException != null)
                        Console.WriteLine("!!! Exception " + response.OriginalException.Message);
                    else
                        Console.WriteLine(item.Title);
                }
            }
        }

        /// <summary>
        /// adds a list of RssFeeds to ElasticSearch
        /// </summary>
        /// <param name="list"></param>
        internal void AddListToNode(List<RssItem> list)
        {
            foreach (RssItem item in list)
            {
                AddFeedToNode(item);
            }
        }
        /// <summary>
        /// this methode deletes an index on the connected node
        /// </summary>
        /// <param name="index">the index to delete</param>
        internal void DeleteByIndex(String index)
        {
            var response = client.DeleteIndex(index);
            // Console.WriteLine(response.IsValid);
        }
        /// <summary>
        /// deletes the documents which match the querry MatchPhrase { field : str }
        /// </summary>
        /// <param name="field"></param>
        /// <param name="str"></param>
        internal void DeleteBySearch(string field, string str)
        {
            client.DeleteByQuery<RssItem>(d => d
                .Query(q => q
                    .MatchPhrase(m => m
                        .Field(field).Query(str)
                    )
                )
            );
        }

        /// <summary>
        /// search a String in the title-field
        /// </summary>
        /// <param name="title">the title to search for</param>
        /// <param name="index">the index on which the search is</param>
        /// <returns>if the title is on the index return true otherwise return false</returns>
        internal bool SearchTitle(String title, String index, DateTime date)
        {
            var res = client.Search<RssItem>(s => s
              .From(0)
              .Size(500)
              .Index(index.ToLower() + "-" + "*")
                .Query(q => q
                    .MatchPhrase(m => m
                        .Field("title").Query(title)
                     )
                )
            );

            if (res.Hits.Count() > 0 && res.IsValid)
            {
                return true;
            }
            else 
            {
                return false;
            }
        }
        /// <summary>
        /// Searches all entries on the index
        /// </summary>
        /// <param name="index">the index to search</param>
        /// <returns>true if at least one result was found</returns>
        internal bool SearchIndex(String index)
        {
            var res = client.Search<RssItem>(s => s
                .From(0)
                .Size(500)
                .Index(index + "*")
                );
            foreach (var hit in res.Documents)
            {

                Console.WriteLine(hit.Title);

            }
            if (res.Documents.Count > 1)
            {
                return true;
            }
            else return false;

        }
        internal void SearchTest()
        {
            var res = client.Search<RssItem>(s => s
                .From(0)
                .Size(500)
                .Index("*")
                .Query(q => q
                    .Terms(t => t
                        .Field(f => f.Title)
                        .Terms(new List<string> { "Bayern", "Trump" })
                    )
                )
            );

            foreach (RssItem it in res.Documents)
            {
                Console.WriteLine(it.Title);
                Console.WriteLine("");
            }
            Console.Read();
        }

        internal void CloseIndex(string index)
        {
            client.CloseIndex(index);
        }
        internal void OpenIndex(string index)
        {
            client.OpenIndex(index);
        }

        internal void CloseOldIndex()
        {

        }
    }
}
