﻿using RssService.SqlLinq;
using System;
using System.Collections.Generic;

namespace RssService
{
    class ServiceHandler
    {
        /// <summary>
        /// read all Providers with name and url from DB
        /// </summary>
        /// <returns>list of providers with name and url</returns>
        internal List<ServiceItem> GetServiceList()
        {
            var context = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
            List<ServiceItem> provider = new List<ServiceItem>();
            foreach (var prov in context.Providers)
            {
                provider.Add(new ServiceItem(prov.Name, prov.Link, prov.XPathContent, prov.XPathPicture));
            }
            return provider;
        }

    }
}
