﻿using RssService.SqlLinq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RssService
{
    internal class IndizeHandler
    {
        internal long UserProfileId { get; private set; }
        internal List<string> ProviderTags { get; private set; } = new List<string>();
        private string _Indizes;
        internal string Indizes
        {
            get
            {
                return _Indizes;
            }
            set
            {
                _Indizes = value;
                SaveIndizes();
            }
        }

        internal IndizeHandler(long UserProfileId)
        {
            this.UserProfileId = UserProfileId;
            LoadProviderTags();
        }

        private void SaveIndizes()
        {
            using (UserProfileViewDataContext context = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString))
            {
                context.Log = Console.Out;
                var userProfile = context.UserProfiles
                    .Where(w => w.Id == UserProfileId)
                    .SingleOrDefault();
                userProfile.Elasticsearch_Indizes = Indizes;
                context.SubmitChanges();
            }
        }

        private void LoadProviderTags()
        {
            UserProfileViewDataContext context = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
            List<V_ProfileIndizes> emailTags = new List<V_ProfileIndizes>();
            List<string> str = new List<string>();

            /*emailTags.AddRange(from t in context.V_EmailOrderTags
                               where t.UserProfileId==UserProfileId
                               select t);
                               */

            str.AddRange(from t in context.UserProfiles
                         where t.Id == UserProfileId
                         select t.Elasticsearch_Indizes);

            if (str.Count > 0)
                Indizes = str[0];

            foreach (V_ProfileIndizes tag in emailTags)
            {
                ProviderTags.Add(tag.TagName);
            }
            if (emailTags.Count > 0)
            {
                //Indizes=emailTags[0].Elasticsearch_Indizes;


            }

        }

    }
}
