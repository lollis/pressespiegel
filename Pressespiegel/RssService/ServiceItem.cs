﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RssService
{
    class ServiceItem
    {
        internal string Url { get; set; }
        internal string Service { get; set; }
        internal string XPathContent { get; set; }
        internal string XPathPicture { get; set; }

        internal ServiceItem(string service, string url, string xPathContent = null, string xPathPicture = null)
        {
            this.Service = service;
            this.Url = url;
            this.XPathContent = xPathContent;
            this.XPathPicture = xPathPicture;

        }

    }
}
