﻿using Pressespiegel.DataProvider;
using RssService.SqlLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RssService
{
    public class Filter
    {
        ElasticSearch e;
        public Filter()
        {
            e = new ElasticSearch();
        }
        /// <summary>
        /// Ruft für jedes Profil die methode filter() auf.
        /// </summary>
        internal void filterProfile()
        {
            UserProfileViewDataContext context = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
            List<long> profileIds = new List<long>();
            
            profileIds.AddRange(from t in context.UserProfiles
                                select t.Id);
            foreach(long id in profileIds)
            {
                filter(id);
            }
        }



        /// <summary>
        /// Sucht für die ProfilId alle aktuellen und passenden Artikel und schreibt diese in die Datenbank
        /// </summary>
        /// <param name="id">übergabe der profilid</param>
        public void filter(long id)
        {
            IndizeHandler handler = new IndizeHandler(id);
            string indizes = "";
            List<string> list = e.GetProfileContentId(id);
            foreach (string str in list)
            {
                indizes += str + ";";
            }
            //Console.WriteLine(indizes);
            handler.Indizes = indizes;
        }

    }
}
