﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Pressespiegel.Models;
using System;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Pressespiegel.Account
{
    public partial class Register : Page
    {
        protected void CreateUser_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
            var user = new ApplicationUser() { UserName = UserName.Text, Email = Email.Text, PhoneNumber = Organization.Text };
            IdentityResult result = manager.Create(user, Password.Text);
            if (result.Succeeded)
            {
                // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                string code = manager.GenerateEmailConfirmationToken(user.Id);
                string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
                manager.SendEmail(user.Id, "Bestätigung Ihre Pressespiegel-Accounts", "Hallo " + user.UserName + ",<br/>" + Environment.NewLine +
                    "Vielen Dank für Ihre Anmeldung bei Pressespiegel.<br/> Bitte bestätigen sie ihren Account indem sie diesen <a href=\"" + callbackUrl + "\">Link</a> öffnen. <br/>" + Environment.NewLine +
                    "<br> Mit freundlichen Grüßen <br/><br/> Ihr Pressespiegel-Team");
                //signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                ////Response.Redirect("~/Account/Confirm");
                //IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                //IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                Global.CreateDataBase();
                manager.AddToRole(user.Id, "User");
                Response.Redirect("~/Account/Registrated?id=" + user.Id);
            }
            else
            {
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }
        }
    }
}