﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Web;
using System.Web.UI;
using Pressespiegel;
using System.Web.Security;
using Microsoft.AspNet.Identity.EntityFramework;

/// <changelog>
/// Fabian:
///     'if(IsValid)'-Abfrage in LogIn-Methode auskommentiert
///     else Statement zu 'if(User!=null)'-Abfrage in LogIn-Methode hinzugefügt
/// </changelog>
namespace Pressespiegel.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorMessage.Visible = false;
            RegisterHyperLink.NavigateUrl = "Register";
            // Enable this once you have account confirmation enabled for password reset functionality
            ForgotPasswordHyperLink.NavigateUrl = "Forgot";
            OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];
            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            if (!String.IsNullOrEmpty(returnUrl))
            {
                RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            }
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();
            try
            {
                var noUser = manager.FindByName("thatsToCreateTheDataBase@noUser.com");

            }
            catch (Exception ex)
            {

            }
            Global.CreateDataBase();
            Global.CreateDefaultUser(Context);
            // This doen't count login failures towards account lockout

            // To enable password failures to trigger lockout, change to shouldLockout: true
            //var user = manager.FindByEmail("noEmail@mail.com");
            //var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            //var user = manager.FindByEmail(Email.Text);
            //Global.CreateDataBase();
        }

        protected void SendEmailConfirmationToken(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = manager.FindByName(Email.Text);

            if (user != null)
            {
                if (!user.EmailConfirmed)
                {
                    string code = manager.GenerateEmailConfirmationToken(user.Id);
                    string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
                    manager.SendEmail(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>.");

                    FailureText.Text = "Confirmation email sent. Please view the email and confirm your account.";
                    ErrorMessage.Visible = true;
                    ResendConfirm.Visible = false;
                }
            }
        }


        protected void LogIn(object sender, EventArgs e)
        {
            //bei SendGrid anmelden und email server configurieren
            //if (IsValid)
            //{
            // Validate the user password
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

            // This doen't count login failures towards account lockout
            // To enable password failures to trigger lockout, change to shouldLockout: true
            var user = manager.FindByEmail(Email.Text);


            //manager.AddToRole(user.Id, "User");
            if (user != null)
            {
                if (!user.EmailConfirmed)
                {
                    FailureText.Text = "Invalid login attempt. You must have a confirmed email address. Enter your email and password, then press 'Resend Confirmation'.";
                    ErrorMessage.Visible = true;
                    ResendConfirm.Visible = true;
                }
                else
                {
                    var result = signinManager.PasswordSignIn(user.UserName, Password.Text, RememberMe.Checked, shouldLockout: false);
                    switch (result)
                    {
                        case SignInStatus.Success:
                            new Pressespiegel.MsSqlDataBase().CreateMsSqlDataBase();
                            Response.Redirect("~/PressReview/Overview");
                            break;
                        case SignInStatus.LockedOut:
                            Response.Redirect("/Account/Lockout");
                            break;
                        case SignInStatus.RequiresVerification:
                            Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}",
                                                            Request.QueryString["ReturnUrl"],
                                                            RememberMe.Checked),
                                                            true);
                            break;
                        case SignInStatus.Failure:
                        default:
                            FailureText.Text = "Invalid login attempt";
                            ErrorMessage.Visible = true;
                            break;
                    }
                }
            }
            else
            {
                ErrorMessage.Visible = true;
                FailureText.Text = "Invalid login attempt";
            }

            //}
        }
    }
}