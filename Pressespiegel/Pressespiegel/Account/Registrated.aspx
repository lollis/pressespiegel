﻿<%@ Page Title="Validate your Email" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Registrated.aspx.cs" Inherits="Pressespiegel.Account.Registrated" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    Please look also in the spam folder.
    <asp:Button runat="server" ID="ResendConfirm" OnClick="SendEmailConfirmationToken" Text="Resend confirmation" CssClass="btn btn-default" />
</asp:Content>
