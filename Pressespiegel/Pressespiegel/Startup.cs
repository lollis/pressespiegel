﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Pressespiegel.Startup))]
namespace Pressespiegel
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
