﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Inside/Main.master" AutoEventWireup="true" CodeBehind="ProfileArchiv.aspx.cs" Inherits="Pressespiegel.PressReview.ShowArchive" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TabContent" runat="server">
    <div class="transparent mainBlockRadius">
        <div class="row">
            <h3 style="text-align: center;"><asp:Label runat="server" ID="lblProfileName"></asp:Label>
                - Archiv
            </h3>
                <hr style="border: solid #008CBA 2px; height: 1px; width: 95%;" />
            <asp:Table runat="server" ID="tblArchivItems" BorderWidth="0" BackColor="Transparent" HorizontalAlign="Center" CellPadding="10">

            </asp:Table>
        </div>
        <br />
    </div>
</asp:Content>
