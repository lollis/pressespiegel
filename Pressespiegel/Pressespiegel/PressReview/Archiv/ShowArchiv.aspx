﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Inside/Main.master" AutoEventWireup="true" CodeBehind="ShowArchiv.aspx.cs" Inherits="Pressespiegel.PressReview.Archiv.ShowArchiv" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TabContent" runat="server">
    <div class="transparent mainBlockRadius">
        <div class="row">
            <h3 style="text-align: center;"><asp:Label runat="server" ID="lblProfileName"></asp:Label>
                - Archiv
            </h3>
            <h5 style="text-align: center">vom<asp:Label runat="server" ID="lblDateTime"></asp:Label></h5>
                <hr style="border: solid #008CBA 2px; height: 1px; width: 95%;" />
        </div>
        <asp:Table ID="tblRssItems" runat="server" Font-Size="Medium" BackColor="Transparent"
                BorderColor="White" BorderWidth="1" ForeColor="Black" CellPadding="2"
                CellSpacing="5">               
            </asp:Table>
    </div>
</asp:Content>
