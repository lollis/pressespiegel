﻿using RssService;
using RssService.SqlLinq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pressespiegel.PressReview.Archiv
{
    public partial class ShowArchiv : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblProfileName.Text = Request.QueryString["profileName"];
            lblDateTime.Text = " " + Request.QueryString["dateTime"];
            string indizes = Request.QueryString["indizes"];
            DataProvider.ElasticSearch elastic = new DataProvider.ElasticSearch();
            List<RssItem> rssItems = new List<RssItem>();
            rssItems = elastic.SearchArchive(indizes);
            HyperLink hl;
            TableCell tcImgProv, tcImgFeed, tcHl, tcCont, tcDate, tcTable;
            Image imgProv, imgFeed;
            Label date, cont;
            Table table;
            Dictionary<string, KeyValuePair<string, string>> provDic = LoadProviderImage();
            KeyValuePair<string, string> image;
            foreach (RssItem item in rssItems)
            {
                tcImgProv = new TableCell() { Width = 60 };
                imgProv = new Image();

                provDic.TryGetValue(item.ServiceName, out image);
                //key=image, value=path

                if (!string.IsNullOrEmpty(image.Value))
                {
                    provDic.TryGetValue(item.ServiceName, out image);
                    imgProv.ImageUrl = "data:image;base64," + image.Value;
                }
                imgProv.Style.Add("width", "100%");
                tcImgProv.Controls.Add(imgProv);
                tcHl = new TableCell() { Width = 740 };
                tcDate = new TableCell();
                date = new Label();
                date.Text = item.Date.ToString("G");
                date.Font.Size = new FontUnit(10);
                tcDate.VerticalAlign = VerticalAlign.Top;
                tcDate.Controls.Add(date);
                hl = new HyperLink();
                hl.NavigateUrl = item.Url;
                hl.Text = item.Title;
                hl.Target = "_blank";

                tcHl.Controls.Add(hl);


                tcImgFeed = new TableCell() { Width = 160 };
                if (item.BildUrl != null && item.BildUrl.Length > 10)
                {
                    imgFeed = new Image();
                    imgFeed.ImageUrl = item.BildUrl;
                    imgFeed.Style.Add("width", "150px");
                    tcImgFeed.Controls.Add(imgFeed);
                    tcCont = new TableCell() { Width = 580 };
                }
                else
                {
                    tcCont = new TableCell() { Width = 740 };
                }
                cont = new Label();
                cont.Text = item.Content;
                cont.Font.Size = new FontUnit(11);
                tcCont.Controls.Add(cont);

                tblRssItems.Rows.Add(new TableRow());
                tblRssItems.Rows[tblRssItems.Rows.Count - 1].Cells.Add(tcImgProv);
                tblRssItems.Rows[tblRssItems.Rows.Count - 1].Cells.Add(tcHl);

                tblRssItems.Rows.Add(new TableRow());
                tblRssItems.Rows[tblRssItems.Rows.Count - 1].Cells.Add(new TableCell() { Width = 60 });
                tblRssItems.Rows[tblRssItems.Rows.Count - 1].Cells.Add(tcDate);

                if (item.BildUrl != null && item.BildUrl.Length > 10)
                {
                    tcTable = new TableCell();
                    table = new Table();
                    table.Rows.Add(new TableRow());
                    table.Rows[0].Cells.Add(tcImgFeed);
                    table.Rows[0].Cells.Add(tcCont);
                    tcTable.Controls.Add(table);

                    tblRssItems.Rows.Add(new TableRow());
                    tblRssItems.Rows[tblRssItems.Rows.Count - 1].Cells.Add(new TableCell() { Width = 60 });
                    tblRssItems.Rows[tblRssItems.Rows.Count - 1].Cells.Add(tcTable);

                }
                else
                {
                    tblRssItems.Rows.Add(new TableRow());
                    tblRssItems.Rows[tblRssItems.Rows.Count - 1].Cells.Add(new TableCell() { Width = 60 });
                    tblRssItems.Rows[tblRssItems.Rows.Count - 1].Cells.Add(tcCont);
                }
                tblRssItems.Rows.Add(new TableRow() { Height = 25 });
                tblRssItems.DataBind();
            }
        }

        internal Dictionary<string, KeyValuePair<string, string>> LoadProviderImage()
        {
            UserProfileViewDataContext db = new UserProfileViewDataContext(MainWindow.ConnectionString);
            Dictionary<string, KeyValuePair<string, string>> providerDic = new Dictionary<string, KeyValuePair<string, string>>();
            foreach (RssService.SqlLinq.Provider prov in db.Providers)
            {
                string path = Path.Combine(new string[] { AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), "img" + new Random().Next().ToString() + ".png" }).ToString();
                providerDic.Add(prov.Name, new KeyValuePair<string, string>(path, prov.Logo));
            }
            return providerDic;
        }
    }
}