﻿using RssService.Archiv;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pressespiegel.PressReview
{
    public partial class ShowArchive : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string profileName = Request.QueryString["profileName"];
            lblProfileName.Text = profileName + " ";
            long profileId = long.Parse(Request.QueryString["profileId"]);
            ArchivProfile archiv = new ArchivProfile(profileId);
            archiv.loadArchiv();
            int counter = 3;
            TableRow tr = new TableRow();
            TableCell tc;
            LinkButton lbtn;
            foreach(ArchivItem ai in archiv.archivItems)
            {
                if(counter == 3)
                {
                    tr = new TableRow();
                    counter = 0;
                }
                tc = new TableCell();
                lbtn = new LinkButton();
                lbtn.Text = ai.dateTime.ToString();
                lbtn.CssClass = "btn btn-primary";
                lbtn.PostBackUrl = "~/PressReview/Archiv/ShowArchiv?indizes=" + ai.indizes + "&profileName=" + profileName +
                    "&dateTime=" + ai.dateTime.ToString();
                tc.Controls.Add(lbtn);
                tr.Controls.Add(tc);
                tblArchivItems.Controls.Add(tr);
                counter++;
            }
        }
    }
}