﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Inside/Main.master" AutoEventWireup="true" CodeBehind="ArchivOverview.aspx.cs" Inherits="Pressespiegel.PressReview.ArchivOverview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TabContent" runat="server">
    <div class="transparent mainBlockRadius">
        <div class="row">
            <h3 style="text-align: center;">Archiv</h3>
                <hr style="border: solid #008CBA 2px; height: 1px; width: 95%;" />
        </div>
        <asp:PlaceHolder runat="server" ID="phArchivTbl">
        <asp:Table ID="tblArchiv" runat="server" BorderWidth="0" BackColor="Transparent" HorizontalAlign="Center">

        </asp:Table>
            </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="phNoArchivData" Visible="false">
            <div style="text-align: center">
            <h5>Hier finden Sie alle Pressespiegel Ihrer Profile wieder, die Sie per Mail zugesand bekommen haben.<br /><br />
                Bis jetzt sind keine Einträge im Archiv vorhanden.
            </h5>
                </div>
            <br />
        </asp:PlaceHolder>
        <br />
    </div>
</asp:Content>
