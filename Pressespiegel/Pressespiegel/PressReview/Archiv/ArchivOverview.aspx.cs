﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RssService.SqlLinq;
using RssService.Archiv;
using Microsoft.AspNet.Identity;

namespace Pressespiegel.PressReview
{
    public partial class ArchivOverview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataProvider.Provider provider = new DataProvider.Provider(User.Identity.GetUserId());
            List<UserProfile> profiles = new List<UserProfile>();
            profiles = provider.GetUserSites();
            TableCell tc;
            LinkButton lbtn;
            Label lbl;
            Image img;
            foreach (UserProfile up in profiles)
            {
                ArchivProfile archiv = new ArchivProfile(up.Id);
                archiv.loadArchiv();
                int itemsCount = archiv.archivItems.Count;
                if (itemsCount > 0)
                {
                    lbtn = new LinkButton();
                    lbl = new Label();
                    img = new Image();
                    lbtn.PostBackUrl = "~/PressReview/Archiv/ProfileArchiv?profileId=" + up.Id + "&profileName=" + up.Name;
                    lbtn.Text = up.Name;
                    lbtn.CssClass = "btn btn-default";
                    lbtn.ID = "lbtn_" + up.Id;
                    lbtn.Width = 300;
                    if(itemsCount == 1)
                    {
                        lbl.Text = "1 Eintrag";
                    }
                    else
                    {
                        lbl.Text = itemsCount.ToString() + " Einträge";
                    }
                    
                    tblArchiv.Rows.Add(new TableRow());
                    tc = new TableCell() { Width = 150 };
                    tc.HorizontalAlign = HorizontalAlign.Right;
                    if (up.Logo != null)
                    {     
                        img.ImageUrl = "data:image;base64," + up.Logo;
                        img.Style.Add("height", "40px");
                        tc.Controls.Add(img);
                    }
                    tblArchiv.Rows[tblArchiv.Rows.Count - 1].Cells.Add(tc);
                    tc = new TableCell() { Width = 500 };
                    tc.HorizontalAlign = HorizontalAlign.Left;
                    tc.Controls.Add(lbtn);
                    tc.HorizontalAlign = HorizontalAlign.Center;
                    tblArchiv.Rows[tblArchiv.Rows.Count - 1].Cells.Add(tc);
                    tc = new TableCell() { Width = 150 };
                    tc.Controls.Add(lbl);
                    tc.HorizontalAlign = HorizontalAlign.Left;
                    tblArchiv.Rows[tblArchiv.Rows.Count - 1].Cells.Add(tc);
                    tblArchiv.Rows.Add(new TableRow() { Height = 10 });
                }
            }
            if (tblArchiv.Rows.Count == 0)
            {
                phArchivTbl.Visible = false;
                phNoArchivData.Visible = true;
            }
        }
    }
}