﻿using Microsoft.AspNet.Identity;
using Pressespiegel.App_Inside;
using RssService.SqlLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

namespace Pressespiegel.PressReview
{
    public partial class AddProfile : System.Web.UI.Page
    {
        List<Provider> provList = new List<Provider>();

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Form.Attributes.Add("enctype", "multipart/form-data");
            Global.IsLoggedIn(System.Web.HttpContext.Current.User, Response);
            profileData.Visible = true;
            profileCreated.Visible = false;
            UserProfileViewDataContext updb = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
            provList.AddRange(from t in updb.Providers select t);
            int counter = 3;
            TableRow tr = new TableRow();
            foreach (Provider p in provList)
            {
                if (counter == 3)
                {
                    tr = new TableRow();
                    counter = 0;
                }
                CheckBox cb = new CheckBox();
                cb.Text = p.Name;
                cb.ID = "checkBox_" + p.Id;
                cb.CssClass = "roundedCB";
                cb.InputAttributes["class"] = "roundedCB";
                TableCell tc = new TableCell();
                tc.ID = "tableCell_" + p.Id;
                tc.Controls.Add(cb);
                tr.Controls.Add(tc);
                cbProviders.Controls.Add(tr);
                counter++;
            }
        }

        protected void btnPlus_Click(object sender, EventArgs e)
        {
            int i = 0;
            while (searchTerms.Rows[i].Visible == true)
            {
                i++;
            }
            btnRemove.Visible = true;
            if (i < 99)
            {
                searchTerms.Rows[i].Visible = true;
            }
        }

        protected void btnRemove_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            int i = 0;
            while (searchTerms.Rows[i].Visible == true)
            {
                i++;
            }
            searchTerms.Rows[i - 1].Visible = false;
            if (i == 2)
            {
                btnRemove.Visible = false;
            }
        }

        

        protected void btnCreateProfile_Click(object sender, EventArgs e)
        {
            txtFailure.Text = "";
            txtProfil_Name.CssClass = txtProfil_Name.CssClass.Replace("redBorder", "");
            cbProviders.Style.Add("background-color", "rgba(245, 245, 245, 0.96)");
            int i;
            for(i = 0; i < 100; i++)
            {
                foreach (TextBox tb in searchTerms.Rows[i].Cells[0].Controls.OfType<TextBox>())
                {
                    tb.CssClass = tb.CssClass.Replace("redBorder", "");
                }
            }
            
            i = 0;

            if (txtProfil_Name != null && txtProfil_Name.Text.Length > 0)
            {
                ReportWorker rp = new ReportWorker();
                if(rp.CheckOnPattern(txtProfil_Name.Text))
                {
                    if (fileProfileLogo.PostedFile != null)
                    {
                        if (fileProfileLogo.PostedFile.ContentLength > 100)
                        {
                            if (isImage(fileProfileLogo.PostedFile) == false)
                            {
                                txtFailure.Text = "Das Dateiformat für das Profillogo wird nicht unterstützt.\n" +
                                    "Unterstützte Formate: jpg, jpeg, pjpeg, png, x-png, gif.";
                                fileProfileLogo.Style.Add("background-color", "rgba(255, 0, 0, 0.25)");
                                return;
                            }
                        }
                    }
                    List<UserProfileTag> profileTags = new List<UserProfileTag>();
                    List<long> tagIds = new List<long>();
                    i = 0;
                    bool hasTag = false;
                    bool faultTerm = false;
                    //Tags in Liste einfügen    
                    while (searchTerms.Rows[i].Visible == true)
                    {
                        foreach (TextBox tb in searchTerms.Rows[i].Cells[0].Controls.OfType<TextBox>())
                        {
                            if (tb.Text != null && tb.Text.Length > 1)
                            {
                                if (rp.CheckOnPattern(tb.Text))
                                {
                                    tagIds.Add(Tag(tb.Text));
                                    hasTag = true;
                                }
                                else
                                {
                                    tb.CssClass = "redBorder";
                                    txtFailure.Text = "Ein Suchbegriff darf nur aus Groß-, Kleinbuchstaben und Ziffern bestehen.<br/>Außerdem muss er mindestens zwei Zeichen lang sein.";
                                    faultTerm = true;
                                }
                            }
                            else
                            {
                                if(tb.Text.Length != 0)
                                {
                                    faultTerm = true;
                                    tb.CssClass = "redBorder";
                                    txtFailure.Text = "Ein Suchbegriff muss aus mindestens zwei Zeichen bestehen";
                                }          
                            }
                        }
                        i++;
                    }


                    if (hasTag && !faultTerm)
                    {
                        List<CheckBox> selectedCeckBoxes = new List<CheckBox>();
                        CheckBox cb;
                        foreach (TableRow row in cbProviders.Rows)
                        {
                            foreach (TableCell cell in row.Cells)
                            {
                                cb = (CheckBox)cell.Controls[0];
                                if (cb.Checked)
                                    selectedCeckBoxes.Add(cb);
                            }
                        }
                        if (selectedCeckBoxes.Count > 0)
                        {
                            long profileId;
                            //profile erstellen
                            var db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
                            UserProfile profile = new UserProfile()
                            {
                                UserId = User.Identity.GetUserId(),
                                Name = txtProfil_Name.Text,
                                Logo = Convert.ToBase64String(fileProfileLogo.FileBytes)
                            };
                            db.UserProfiles.InsertOnSubmit(profile);
                            db.SubmitChanges();
                            profileId = profile.Id;
                            //alle Tags einfügen


                            foreach (long tagId in tagIds)
                            {
                                profileTags.Add(new UserProfileTag { TagId = tagId, UserProfileId = profileId });
                            }
                            db.UserProfileTags.InsertAllOnSubmit(profileTags);

                            List<UserProfileProvider> selectedProviders = new List<UserProfileProvider>();
                            foreach (CheckBox box in selectedCeckBoxes)
                            {
                                selectedProviders.Add(new UserProfileProvider()
                                {
                                    UserProfileId = profileId,
                                    ProviderId = long.Parse(box.ID.Split('_')[1])
                                });
                            }
                            db.UserProfileProviders.InsertAllOnSubmit(selectedProviders);
                            db.SubmitChanges();

                            labelProfilename.Text = txtProfil_Name.Text + "'";
                            profileCreated.Visible = true;
                            profileData.Visible = false;

                            //Erste Indizierung der gesuchten Artikel
                            RssService.Filter f = new RssService.Filter();
                            f.filter(profileId);
                        }
                        else
                        {
                            txtFailure.Text = "Bitte wählen Sie mindestens einen Anbieter aus.";
                            cbProviders.Style.Add("background-color", "rgba(255, 0, 0, 0.25)");
                        }
                    }
                    else
                    {
                        if (!faultTerm)
                        {
                            txtFailure.Text = "Bitte geben Sie mindestens einen Suchbegriff für das Profil ein.<br/>Dieser muss aus mindestens zwei Zeichen bestehen.";
                            foreach (TextBox tb in searchTerms.Rows[0].Cells[0].Controls.OfType<TextBox>())
                            {
                                tb.CssClass = "redBorder";
                            }
                        } 
                    }
                }
                else
                {
                    txtProfil_Name.CssClass = "redBorder";
                    txtFailure.Text = "Der Profilname darf nur aus Groß-, Kleinbuchstaben und Ziffern bestehen<br/>und muss zwischen einschließlich einem und 30 Zeichen lang sein.";
                }
            }
                
            else
            {
                txtProfil_Name.CssClass = "redBorder";
                txtFailure.Text = "Bitte geben Sie einen Namen für ihr Profil ein.";
            }

        }

        private long Tag(string tagName)
        {
            //Look if tag already exists
            using (var db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString))
            {
                var id = from t in db.Tags
                         where System.Data.Linq.SqlClient.SqlMethods.Like(t.TagName, tagName)
                         select t.Id;
                if (id.Count() > 0)
                {
                    return id.First();
                }
                else
                {
                    Tag tag = new Tag { TagName = tagName };
                    db.Tags.InsertOnSubmit(tag);
                    db.SubmitChanges();
                    return tag.Id;
                }

            }
        }

        protected void btnViewProfile_Click(object sender, EventArgs e)
        {
            List<UserProfile> userSites = new List<UserProfile>();
            DataProvider.Provider provider = new DataProvider.Provider(User.Identity.GetUserId());
            userSites = provider.GetUserSites();
            if (userSites != null)
            {
                foreach (UserProfile up in userSites)
                {
                    if (up.Name.Equals(txtProfil_Name.Text) && up.UserId.Equals(User.Identity.GetUserId()))
                    {
                        Session["profileView"] = up;
                        break;
                    }
                }
            }
            Response.Redirect("~/PressReview/ShowProfile");
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PressReview/Overview");
        }

        static internal bool isImage(System.Web.HttpPostedFile postedFile)
        {
            if (postedFile.ContentType.ToLower() != "image/jpg" &&
                   postedFile.ContentType.ToLower() != "image/jpeg" &&
                   postedFile.ContentType.ToLower() != "image/pjpeg" &&
                   postedFile.ContentType.ToLower() != "image/gif" &&
                   postedFile.ContentType.ToLower() != "image/x-png" &&
                   postedFile.ContentType.ToLower() != "image/png")
            {
                return false;
            }
            return true;
        }
    }
}