﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Inside/Main.master" AutoEventWireup="true" CodeBehind="AddProfile.aspx.cs" Inherits="Pressespiegel.PressReview.AddProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TabContent" runat="server">
    <div class="transparent mainBlockRadius">
        <asp:PlaceHolder runat="server" ID="profileData" Visible="true">
            <div class="row">
                <h3 style="text-align: center;">Neues Profil erstellen</h3>
                <hr style="border: solid #008CBA 2px; height: 1px; width: 95%;" />

                <table style="width: 700px;">
                    <tr>
                        <td>
                            <h4 style="text-indent: 30px;">Profilname:
                <asp:TextBox runat="server" ID="txtProfil_Name" MaxLength="30" ToolTip="Profilname bestehend aus Groß-, Kleinbuchstaben und Ziffern und einem bis 30 Zeichen lang" />
                            </h4>
                        </td>
                        <td class="center">
                            <h5>Laden Sie ein Logo für Ihr Profil hoch
                                <br />
                                (optional, max. 5MB)</h5>
                            <asp:FileUpload runat="server" ID="fileProfileLogo" CssClass="center" />
                        </td>
                    </tr>
                </table>

            </div>

            <hr style="border: ridge #969696 0.2px; height: 0; width: 99%;" />
            <h5 style="text-indent: 15px;">Geben Sie beliebig viele Suchbegriffe an, nach denen in diesem Profil gefiltert werden soll:</h5>
            <asp:UpdatePanel ID="upSearchTerms" runat="server">
                <ContentTemplate>
                    <asp:Table HorizontalAlign="Center" runat="server" ID="searchTerms" Font-Size="Medium" BackColor="Transparent" BorderWidth="0" ForeColor="Black" CellPadding="5"
                        CellSpacing="20">
                        <asp:TableRow Visible="true">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox0" ToolTip="Suchbegriff aus Groß-, Kleinbuchstaben und Ziffern. Mindestens zwei Zeichen lang." placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox1" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox2" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox3" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox4" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox5" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox6" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox7" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox8" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox9" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox10" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox11" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox12" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox13" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox14" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox15" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox16" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox17" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox18" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox19" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox20" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox21" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox22" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox23" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox24" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox25" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox26" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox27" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox28" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox29" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox30" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox31" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox32" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox33" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox34" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox35" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox36" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox37" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox38" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox39" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox40" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox41" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox42" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox43" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox44" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox45" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox46" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox47" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox48" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox49" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox50" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox51" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox52" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox53" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox54" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox55" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox56" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox57" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox58" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox59" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox60" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox61" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox62" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox63" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox64" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox65" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox66" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox67" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox68" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox69" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox70" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox71" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox72" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox73" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox74" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox75" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox76" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox77" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox78" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox79" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox80" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox81" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox82" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox83" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox84" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox85" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox86" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox87" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox88" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox89" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox90" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox91" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox92" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox93" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox94" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox95" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox96" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox97" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox98" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox99" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow CssClass="center">
                            <asp:TableCell>
                                <asp:ImageButton ID="btnPlus" ImageUrl="~/fonts/blue-plus-icon-6.png" OnClick="btnPlus_Click" Height="30px" Width="30px" runat="server" ToolTip="Weiteren Suchbegriff hinzufügen" />
                                <asp:ImageButton ID="btnRemove" Visible="false" ImageUrl="~/fonts/blue-minus-icon.png" OnClick="btnRemove_Click" Height="30px" Width="30px" runat="server" ToolTip="Suchbegriff entfernen" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnPlus" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnRemove" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
            <hr style="border: ridge #969696 0.2px; height: 0; width: 99%;" />
            <h5 style="text-indent: 15px;">Wählen Sie die Mediendienstleister aus, die in die Suche mit einbezogen werden sollen:</h5>
            <div style="text-align: center">
                <asp:Table HorizontalAlign="Center" Font-Size="Small" BackColor="Transparent" ForeColor="Black" CellPadding="4"
                    runat="server" ID="cbProviders" Style="text-align: left; width: 600px" CellSpacing="5">
                </asp:Table>
            </div>
            <hr style="border: ridge #969696 0.2px; height: 0; width: 99%;" />
            <br />
            <div class="center">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:Button runat="server" ID="btnCreateProfile" Text="Erstellen" CssClass="btn btn-default" OnClick="btnCreateProfile_Click" />
                        <br />
                        <asp:Label runat="server" CssClass="text-danger" ID="txtFailure"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnCreateProfile" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

            <span style="display: block; height: 20px;"></span>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="profileCreated" Visible="false">
            <div class="row center">
                <br />
                <h4 style="text-align: center;">Das Profil '<asp:Label runat="server" ID="labelProfilename"></asp:Label>
                    wurde erstellt.
                </h4>
                <br />
                <table class="center">
                    <tr>
                        <td>
                            <asp:Button CssClass="btn btn-default" runat="server" ID="btnOK" Text="OK" OnClick="btnOK_Click" />
                        </td>
                        <td>
                            <asp:Button CssClass="btn btn-default" runat="server" ID="btnViewProfile" Text="Profil Anzeigen" OnClick="btnViewProfile_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <span style="display: block; height: 20px;"></span>
        </asp:PlaceHolder>
    </div>
</asp:Content>
