﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Inside/Main.master" AutoEventWireup="true" CodeBehind="ShowProfile.aspx.cs" Inherits="Pressespiegel.PressReview.ShowProfile" %>

<%@ MasterType VirtualPath="~/App_Inside/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TabContent" runat="server">
    <div class="transparent mainBlockRadius">
        <div class="row">
            <h3 style="text-align: center;">
                <asp:Button runat="server" Visible="true" Text="Teilen" ToolTip="Benutzer innerhalb ihrer Organisation, können das Profil ebenfalls nutzen." CssClass="btn btn-default btnTeilen" ID="btnShareProfile" OnClick="btnShareProfile_Click" />
                <asp:Label runat="server" Font-Size="Medium" Visible="false" CssClass="btnTeilen" ID="lblProfileShared"></asp:Label>
                <asp:Image runat="server" ID="imageProfile" />
                <asp:Label runat="server" ID="labelProfile_Name"></asp:Label>
                <asp:Button runat="server" OnClick="btnEditProfile_Click" Text="Bearbeiten" CssClass="btn btn-default btnBearbeitein" ID="btnEditProfile" />            
            </h3>         
            <hr style="border: solid #008CBA 2px; height: 1px; width: 95%;" />
        </div>
        <br />
        <div>
            <asp:Table ID="searchResults" runat="server" Font-Size="Medium" BackColor="Transparent"
                BorderColor="White" BorderWidth="1" ForeColor="Black" CellPadding="2"
                CellSpacing="5">
            </asp:Table>
        </div>
        <div class="center">
            <asp:Label runat="server" ID="labelNoResults" Visible="false" Text="Für das Profil gibt es aktuell keine Treffer.<br/>
                In den aktuellsten Nachrichten gibt es keine Übereinstimmung mit den Suchbegriffen dieses Suchprofils."></asp:Label>
        </div>
        <span style="display: block; height: 20px;"></span>
    </div>
</asp:Content>
