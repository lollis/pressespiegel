﻿<%@ Page Title="Overview" Language="C#" MasterPageFile="~/App_Inside/Main.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.cs" Inherits="Pressespiegel.PressReview.Overview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TabContent" runat="server">
    <div class="transparent mainBlockRadius">
        <div class="center">
            <br />
            <h5>Durchsuchen Sie den Inhalt aller Mediendienstleister mit der Eingabe eines Suchbegriffes,<br />
                oder begrenzen Sie die Suche auf einen eingegebenen Mediendienstleister.
            </h5>
            <br />
            <asp:TextBox runat="server" ID="txtContent" ToolTip="Suchbegriff, bestehend aus Groß-, Kleinbuchstaben und Ziffern." placeholder="Suchbegriff"></asp:TextBox>
            <asp:TextBox runat="server" ID="txtServiceName" placeholder="Mediendienstleister"></asp:TextBox>
            <br />
                    <div class="center text-danger">
                        <asp:Label runat="server" ID="lblNoProvider" Visible="false" Text="Der angegebene Mediendienstleister ist nicht verfügbar.<br/>Gehen Sie mit der Maus über das Eingabefeld und es erscheinen alle verfügbaren Mediendienstleister."></asp:Label>
                        <br />
                        <asp:RegularExpressionValidator runat="server" ErrorMessage="Der Suchbegriff darf nur Groß-, Kleinbuchstaben und Ziffern beinhalten."
                            ControlToValidate="txtContent" ValidationExpression="^[a-zA-Z0-9äöüÄÖÜß\s]{1,100}$" CssClass="text-danger"></asp:RegularExpressionValidator>
                    </div>
                    <br />
                    <asp:Button runat="server" ID="btnSearch" Text="Los" CssClass="btn btn-default" OnClick="btnSearch_Click" />
            <br />
        </div>
        <span style="display: block; height: 20px;"></span>
        <div>
            <asp:Table ID="searchResults" runat="server" Font-Size="Medium" BackColor="Transparent"
                BorderColor="White" BorderWidth="1" ForeColor="Black" CellPadding="2"
                CellSpacing="5">
            </asp:Table>
        </div>
        <div class="center">
            <asp:Label ID="labelInfo" runat="server" Visible="false" Text="Die Suche auf Ihren eingegeben Suchbegriff ergab leider keine Treffer."></asp:Label>
            <span id="spanBottom" style="display: block; height: 15px;"></span>
        </div>
    </div>
</asp:Content>
