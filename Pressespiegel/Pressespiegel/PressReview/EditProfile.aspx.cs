﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using RssService.SqlLinq;
using Pressespiegel.App_Inside;

namespace Pressespiegel.PressReview
{
    public partial class EditProfile : System.Web.UI.Page
    {
        public UserProfile currentProfile;
        private List<Tag> profileTags;
        private List<Provider> provList, profileProviders;
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Form.Attributes.Add("enctype", "multipart/form-data");
            Global.IsLoggedIn(System.Web.HttpContext.Current.User, Response);
            if (Session["profileView"] != null)
            {
                currentProfile = (UserProfile)Session["profileView"];
                labelProfile_Name.Text = currentProfile.Name;
                if (currentProfile.Logo != null && currentProfile.Logo.Length > 10)
                {
                    imageProfile.ImageUrl = "data:image;base64," + currentProfile.Logo;
                }
                else
                {
                    imageProfile.Visible = false;
                }
                imageProfile.Style.Add("width", "10%");
                if (Session["firstCall"] == null || (bool)Session["firstCall"] == true)
                {

                    DataProvider.Provider provider = new DataProvider.Provider(currentProfile.UserId);
                    profileTags = provider.GetProfileTags(currentProfile.Id);
                    int count = profileTags.Count;
                    int i;
                    for (i = 0; i < count; i++)
                    {
                        searchTerms.Rows[i].Visible = true;
                        foreach (TextBox tb in searchTerms.Rows[i].Cells[0].Controls.OfType<TextBox>())
                        {
                            tb.Text = profileTags[i].TagName;
                        }
                    }
                    if (i < 2)
                    {
                        btnRemove.Visible = false;
                    }
                }

                Session["firstCall"] = false;
                if (cbProviders.Rows.Count == 0)
                {
                    DataProvider.Provider provider = new DataProvider.Provider(currentProfile.UserId);
                    UserProfileViewDataContext updb = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
                    provList = new List<Provider>();
                    provList.AddRange(from t in updb.Providers select t);
                    profileProviders = provider.GetProfileProviders(currentProfile.Id);
                    int counter = 3;
                    TableRow tr = new TableRow();
                    foreach (Provider p in provList)
                    {
                        if (counter == 3)
                        {
                            tr = new TableRow();
                            counter = 0;
                        }
                        CheckBox cb = new CheckBox();
                        cb.Text = p.Name;
                        cb.ID = "checkBox_" + p.Id;
                        cb.CssClass = "roundedCB";
                        cb.InputAttributes["class"] = "roundedCB";
                        foreach (Provider prov in profileProviders)
                        {
                            if (p.Id == prov.Id)
                            {
                                cb.Checked = true;
                            }
                        }
                        TableCell tc = new TableCell();
                        tc.ID = "tableCell_" + p.Id;
                        tc.Controls.Add(cb);
                        tr.Controls.Add(tc);
                        cbProviders.Controls.Add(tr);
                        counter++;
                    }
                }
                if (Session["searchTermsSaved"] != null && (bool)Session["searchTermsSaved"])
                {
                    btnSaveSearchTerms.CssClass = "btn btn-success";
                    Session["searchTermsSaved"] = false;
                }
                else
                {
                    btnSaveSearchTerms.CssClass = "btn btn-default";
                }
                if (Session["providersSaved"] != null && (bool)Session["providersSaved"])
                {
                    btnSaveProviders.CssClass = "btn btn-success";
                    Session["providersSaved"] = false;
                }
                else
                {
                    btnSaveProviders.CssClass = "btn btn-default";
                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Session["profileView"] = currentProfile;
            Response.Redirect("~/PressReview/ShowProfile");
        }

        protected void btnEditProfileName_Click(object sender, EventArgs e)
        {
            btnEditProfileName.Visible = false;
            txtBoxNewProfileName.Visible = true;
            btnSaveProfileName.Visible = true;
        }

        protected void btnEditProfileLogo2_Click(object sender, EventArgs e)
        {
            btnEditProfileLogo2.Visible = false;
            fileProfileLogo2.Visible = true;
            btnSaveProfileLogo2.Visible = true;
            labelMaxLength.Visible = true;
        }

        protected void btnSaveProfileName_Click(object sender, EventArgs e)
        {
            if (txtBoxNewProfileName.Text != null && txtBoxNewProfileName.Text.Length > 0)
            {
                ReportWorker rp = new ReportWorker();
                if (rp.CheckOnPattern(txtBoxNewProfileName.Text))
                {
                    //Neuen Profilnamen in Datenbank schreiben
                    UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
                    db.Log = Console.Out;
                    var userProfile = db.UserProfiles
                        .Where(w => w.Id == currentProfile.Id)
                        .SingleOrDefault();
                    userProfile.Name = txtBoxNewProfileName.Text;
                    db.SubmitChanges();

                    currentProfile.Name = txtBoxNewProfileName.Text;
                    labelProfile_Name.Text = txtBoxNewProfileName.Text;

                    btnEditProfileName.Visible = true;
                    txtBoxNewProfileName.Visible = false;
                    btnSaveProfileName.Visible = false;
                    Session["firstCall"] = true;
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    phEditProfileNameText.Visible = true;
                    phEditProfileName.Visible = false;
                }
            }
            else
            {
                phEditProfileNameText.Visible = true;
                phEditProfileName.Visible = false;
            }
        }

        protected void btnSaveProfileLogo2_Click(object sender, EventArgs e)
        {
            if (fileProfileLogo2.HasFile)
            {
                if (fileProfileLogo2.PostedFile.ContentLength < 10485760)
                {
                    if (isImage(fileProfileLogo2.PostedFile) == true)
                    {
                        //Neues Profillogo in Datenbank schreiben
                        UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
                        db.Log = Console.Out;
                        var userProfile = db.UserProfiles
                            .Where(w => w.Id == currentProfile.Id)
                            .SingleOrDefault();
                        userProfile.Logo = Convert.ToBase64String(fileProfileLogo2.FileBytes);
                        db.SubmitChanges();

                        currentProfile.Logo = Convert.ToBase64String(fileProfileLogo2.FileBytes);
                        imageProfile.ImageUrl = "data:image;base64," + currentProfile.Logo;

                        btnEditProfileLogo2.Visible = true;
                        fileProfileLogo2.Visible = false;
                        btnSaveProfileLogo2.Visible = false;
                        labelMaxLength.Visible = false;
                        Session["firstCall"] = true;
                        Response.Redirect(Request.RawUrl);
                    }
                    else
                    {
                        labelNoImage.Visible = true;
                        phEditProfileLogoText.Visible = true;
                        phEditProfileLogo.Visible = false;
                    }
                }
                else
                {
                    labelOversize.Visible = true;
                    phEditProfileLogo.Visible = false;
                    phEditProfileLogoText.Visible = true;
                }
            }
            else
            {
                labelNoFile.Visible = true;
                phEditProfileLogoText.Visible = true;
                phEditProfileLogo.Visible = false;
            }

        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            int i = 0;
            while (searchTerms.Rows[i].Visible == true)
            {
                i++;
            }
            searchTerms.Rows[i - 1].Visible = false;
            if (i == 2)
            {
                btnRemove.Visible = false;
            }
        }

        protected void btnPlus_Click(object sender, EventArgs e)
        {
            int i = 0;
            while (searchTerms.Rows[i].Visible == true)
            {
                i++;
            }
            btnRemove.Visible = true;
            if (i < 99)
            {
                searchTerms.Rows[i].Visible = true;
            }
        }

        protected void btnSaveSearchTerms_Click(object sender, EventArgs e)
        {
            int i;
            for(i = 0; i < 100; i++)
            {
                foreach (TextBox tb in searchTerms.Rows[i].Cells[0].Controls.OfType<TextBox>())
                {
                    tb.CssClass = tb.CssClass.Replace("redBorder", "");
                }
            }
            List<UserProfileTag> profileTags = new List<UserProfileTag>();
            List<long> tagIds = new List<long>();
            i = 0;
            bool hasTag = false;
            bool faultTerm = false;
            //Ids von eingegebenen Suchbegeriffen in Liste schreiben
            ReportWorker rp = new ReportWorker();
            while (searchTerms.Rows[i].Visible == true)
            {
                foreach (TextBox tb in searchTerms.Rows[i].Cells[0].Controls.OfType<TextBox>())
                {
                    if (tb.Text != null && tb.Text.Length > 1)
                    {
                        if (rp.CheckOnPattern(tb.Text))
                        {
                            tagIds.Add(Tag(tb.Text));
                            hasTag = true;
                        }
                        else
                        {
                            tb.CssClass = "redBorder";
                            phEditSearchTerms.Visible = false;
                            phEditSearchTermsText.Visible = true;
                            faultTerm = true;
                        }
                    }
                    else
                    {
                        if(tb.Text.Length != 0)
                        {
                            faultTerm = true;
                            tb.CssClass = "redBorder";
                        }
                    }
                }
                i++;
            }
            if (hasTag && !faultTerm)
            {
                //Referenzierung von entfernten Suchbegriffen in Datenbank aufheben
                UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
                db.Log = Console.Out;
                profileTags.AddRange(from upt in db.UserProfileTags
                                     where upt.UserProfileId == currentProfile.Id
                                     where !tagIds.Contains(upt.TagId)
                                     select upt);
                db.UserProfileTags.DeleteAllOnSubmit(profileTags);
                db.SubmitChanges();
                profileTags = new List<UserProfileTag>();
                //Neue Suchbegriffe zum Profil referenzieren
                var tags = db.UserProfileTags
                    .Where(w => w.UserProfileId == currentProfile.Id)
                    .Select(s => s.TagId);

                foreach (long tagId in tagIds)
                {
                    if (!tags.Contains(tagId))
                    {
                        profileTags.Add(new UserProfileTag { TagId = tagId, UserProfileId = currentProfile.Id });
                    }
                }
                db.UserProfileTags.InsertAllOnSubmit(profileTags);
                db.SubmitChanges();


                Session["firstCall"] = true;
                Session["searchTermsSaved"] = true;
                Response.Redirect(Request.RawUrl);
            }
            else
            {
                phEditSearchTermsText.Visible = true;
                phEditSearchTerms.Visible = false;
            }
        }

        protected void btnDeleteProfile_Click(object sender, EventArgs e)
        {
            deleteButtonVisible.Visible = false;
            deleteProfileCommit.Visible = true;
        }

        protected void btnSaveProviders_Click(object sender, EventArgs e)
        {

            List<long> providerIds = new List<long>();
            List<UserProfileProvider> profileProvider = new List<UserProfileProvider>();
            CheckBox cb;
            //Ausgewählte Provider in Liste schreiben
            foreach (TableRow row in cbProviders.Rows)
            {
                foreach (TableCell cell in row.Cells)
                {
                    cb = (CheckBox)cell.Controls[0];
                    if (cb.Checked)
                        providerIds.Add(long.Parse(cb.ID.Split('_')[1]));
                }
            }
            if (providerIds.Count > 0)
            {
                //Referenzierung von abgewählten Provider in Datenbank aufheben
                UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
                db.Log = Console.Out;
                profileProvider.AddRange(from upp in db.UserProfileProviders
                                         where upp.UserProfileId == currentProfile.Id
                                         where !providerIds.Contains(upp.ProviderId)
                                         select upp);
                db.UserProfileProviders.DeleteAllOnSubmit(profileProvider);
                db.SubmitChanges();
                //Neu gewählte Provider zum Profil referenzieren
                profileProvider = new List<UserProfileProvider>();
                var providers = db.UserProfileProviders
                    .Where(w => w.UserProfileId == currentProfile.Id)
                    .Select(s => s.ProviderId);
                foreach (long provId in providerIds)
                {
                    if (!providers.Contains(provId))
                    {
                        profileProvider.Add(new UserProfileProvider { ProviderId = provId, UserProfileId = currentProfile.Id });
                    }
                }
                db.UserProfileProviders.InsertAllOnSubmit(profileProvider);
                db.SubmitChanges();


                Session["providersSaved"] = true;
                Session["firstCall"] = true;
                Response.Redirect(Request.RawUrl);
            }
            else
            {
                phEditProviders.Visible = false;
                phEditProvidersText.Visible = true;
            }
        }

        protected bool isImage(HttpPostedFile postedFile)
        {
            if (postedFile.ContentType.ToLower() != "image/jpg" &&
                   postedFile.ContentType.ToLower() != "image/jpeg" &&
                   postedFile.ContentType.ToLower() != "image/pjpeg" &&
                   postedFile.ContentType.ToLower() != "image/gif" &&
                   postedFile.ContentType.ToLower() != "image/x-png" &&
                   postedFile.ContentType.ToLower() != "image/png")
            {
                return false;
            }
            return true;
        }

        protected void btnDeleteProfileCommit_Click(object sender, EventArgs e)
        {
            UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
            var userProfile = db.UserProfiles
                .Where(w => w.Id == currentProfile.Id)
                .SingleOrDefault();
            db.UserProfiles.DeleteOnSubmit(userProfile);
            db.SubmitChanges();

            Session["firstCall"] = true;
            Response.Redirect("~/PressReview/Overview");
        }

        protected void btnDeleteProfileUndo_Click(object sender, EventArgs e)
        {
            deleteProfileCommit.Visible = false;
            deleteButtonVisible.Visible = true;
        }

        protected void btnOKEditProfileName_Click(object sender, EventArgs e)
        {
            phEditProfileName.Visible = true;
            phEditProfileNameText.Visible = false;
        }

        protected void btnOKEditProfileLogo_Click(object sender, EventArgs e)
        {
            labelNoFile.Visible = false;
            labelNoImage.Visible = false;
            labelOversize.Visible = false;
            phEditProfileLogoText.Visible = false;
            phEditProfileLogo.Visible = true;
        }

        protected void btnOKEditSearchTerms_Click(object sender, EventArgs e)
        {
            phEditSearchTerms.Visible = true;
            phEditSearchTermsText.Visible = false;
        }

        protected void btnOKEditProviders_Click(object sender, EventArgs e)
        {
            phEditProvidersText.Visible = false;
            phEditProviders.Visible = true;
        }

        private long Tag(string tagName)
        {
            //Look if tag already exists
            using (var db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString))
            {
                var id = from t in db.Tags
                         where System.Data.Linq.SqlClient.SqlMethods.Like(t.TagName, tagName)
                         select t.Id;
                if (id.Count() > 0)
                {
                    return id.First();
                }
                else
                {
                    Tag tag = new Tag { TagName = tagName };
                    db.Tags.InsertOnSubmit(tag);
                    db.SubmitChanges();
                    return tag.Id;
                }

            }
        }
    }
}