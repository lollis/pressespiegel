﻿using System;
using System.Collections.Generic;
using RssService.SqlLinq;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Drawing;
using System.Text;
using Pressespiegel.App_Inside;

namespace Pressespiegel.PressReview
{
    public partial class CreateReport : System.Web.UI.Page
    {
        List<UserProfile> profileList = new List<UserProfile>();
        protected void Page_Load(object sender, EventArgs e)
        {
            Global.IsLoggedIn(System.Web.HttpContext.Current.User, Response);
            if (Session["firstCall"] == null || (bool)Session["firstCall"])
            {
                Session["monday"] = false;
                Session["tuesday"] = false;
                Session["wednesday"] = false;
                Session["thursday"] = false;
                Session["friday"] = false;
                Session["saturday"] = false;
                Session["sunday"] = false;
                Session["firstCall"] = false;
            }
            else
            {
                if (Session["time1"] != null && (bool)Session["time1"])
                {
                    txtBoxTime1.Focus();

                    Session["time1"] = false;
                }
                if (Session["time2"] != null && (bool)Session["time2"])
                {
                    txtBoxTime2.Focus();
                    Session["time2"] = false;
                }
                if (Session["time3"] != null && (bool)Session["time3"])
                {
                    txtBoxTime3.Focus();
                    Session["time3"] = false;
                }
            }
            txtFailure.Text = "";
            DataProvider.Provider provider = new DataProvider.Provider(HttpContext.Current.User.Identity.GetUserId());
            profileList = provider.GetUserSites();
            List<EmailOrder> orders = provider.GetEmailOrders();
            Session["countProfiles"] = profileList.Count;
            int counter = 2;
            TableRow tr = new TableRow();
            bool exists = false;
            foreach (UserProfile up in profileList)
            {
                exists = false;
                foreach (EmailOrder eo in orders)
                {
                    if (up.Id == eo.UserProfileId)
                    {
                        exists = true;
                    }
                }
                if (!exists)
                {
                    if (counter == 2)
                    {
                        tr = new TableRow();
                        counter = 0;
                    }
                    CheckBox cb = new CheckBox();
                    cb.Text = up.Name;
                    cb.ID = "checkBox_" + up.Id;
                    cb.CssClass = "roundedCB";
                    cb.InputAttributes["class"] = "roundedCB";
                    TableCell tc = new TableCell();
                    tc.ID = "tableCell_" + up.Id;
                    tc.Controls.Add(cb);
                    tr.Controls.Add(tc);
                    cbProfiles.Controls.Add(tr);
                    counter++;
                }
            }
        }

        protected void btnCreateReport_Click(object sender, EventArgs e)
        {
            txtFailure.Text = "";
            cbProfiles.Style.Add("background-color", "Transparent");
            btnMonday.CssClass = "btn btn-primary";
            btnTuesday.CssClass = "btn btn-primary";
            btnWednesday.CssClass = "btn btn-primary";
            btnThursday.CssClass = "btn btn-primary";
            btnFriday.CssClass = "btn btn-primary";
            btnSaturday.CssClass = "btn btn-primary";
            btnSunday.CssClass = "btn btn-primary";
            txtBoxTime1.CssClass = txtBoxTime1.CssClass.Replace("redBorder", "");
            txtBoxTime2.CssClass = txtBoxTime2.CssClass.Replace("redBorder", "");
            txtBoxTime3.CssClass = txtBoxTime3.CssClass.Replace("redBorder", "");
            CheckBox cb;
            List<long> profileIds = new List<long>();
            foreach (TableRow row in cbProfiles.Rows)
            {
                foreach (TableCell cell in row.Cells)
                {
                    cb = (CheckBox)cell.Controls[0];
                    if (cb.Checked)
                        profileIds.Add(long.Parse(cb.ID.Split('_')[1]));
                }
            }
            if (profileIds.Count > 0)
            {
                if (profileIds.Count < 2)
                {
                    ReportWorker reportWorker = new ReportWorker();
                    String broadcastTime = reportWorker.BuildBroadcastString((bool)Session["monday"], (bool)Session["tuesday"], (bool)Session["wednesday"], (bool)Session["thursday"]
                        , (bool)Session["friday"], (bool)Session["saturday"], (bool)Session["sunday"], txtBoxTime1.Text, txtBoxTime2.Text, txtBoxTime3.Text, true, true);
                    if (!broadcastTime.Equals("noDay"))
                    {
                        if (!broadcastTime.Equals("wrongFormat"))
                        {
                            if (!broadcastTime.Equals("noTime"))
                            {
                                if (!broadcastTime.Equals("sameTime"))
                                {
                                    UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
                                    EmailOrder order = new EmailOrder()
                                    {
                                        UserId = User.Identity.GetUserId(),
                                        UserProfileId = profileIds[0],
                                        BroadcastTime = broadcastTime
                                    };
                                    db.EmailOrders.InsertOnSubmit(order);
                                    db.SubmitChanges();

                                    phReportCreated.Visible = true;
                                    phReportingData.Visible = false;
                                    foreach (UserProfile up in profileList)
                                    {
                                        if (up.Id == profileIds[0])
                                        {
                                            lblProfileName.Text = up.Name + "'";
                                        }
                                    }
                                    StringBuilder days = new StringBuilder();
                                    bool first = true;
                                    if ((bool)Session["monday"])
                                    {
                                        days.Append("Montag");
                                        first = false;
                                    }
                                    if ((bool)Session["tuesday"])
                                    {
                                        if (first == false)
                                        {
                                            days.Append(", ");
                                        }
                                        days.Append("Dienstag");
                                        first = false;
                                    }
                                    if ((bool)Session["wednesday"])
                                    {
                                        if (first == false)
                                        {
                                            days.Append(", ");
                                        }
                                        days.Append("Mittwoch");
                                        first = false;
                                    }
                                    if ((bool)Session["thursday"])
                                    {
                                        if (first == false)
                                        {
                                            days.Append(", ");
                                        }
                                        days.Append("Donnerstag");
                                        first = false;
                                    }
                                    if ((bool)Session["friday"])
                                    {
                                        if (first == false)
                                        {
                                            days.Append(", ");
                                        }
                                        days.Append("Freitag");
                                        first = false;
                                    }
                                    if ((bool)Session["saturday"])
                                    {
                                        if (first == false)
                                        {
                                            days.Append(", ");
                                        }
                                        days.Append("Samstag");
                                        first = false;
                                    }
                                    if ((bool)Session["sunday"])
                                    {
                                        if (first == false)
                                        {
                                            days.Append(", ");
                                        }
                                        days.Append("Sonntag");
                                        first = false;
                                    }
                                    lblSelectedDays.Text = days.ToString();
                                    days = new StringBuilder();
                                    String time1 = txtBoxTime1.Text;
                                    String time2 = txtBoxTime2.Text;
                                    String time3 = txtBoxTime3.Text;
                                    first = true;
                                    if (time1 != null && time1.Length > 4)
                                    {
                                        days.Append(time1);
                                        first = false;
                                    }
                                    if (time2 != null && time2.Length > 4)
                                    {
                                        if (first == false)
                                        {
                                            days.Append(", ");
                                        }
                                        days.Append(time2);
                                        first = false;
                                    }
                                    if (time3 != null && time3.Length > 4)
                                    {
                                        if (first == false)
                                        {
                                            days.Append(", ");
                                        }
                                        days.Append(time3);
                                    }
                                    lblTime.Text = days.ToString();
                                }
                                else
                                {
                                    txtFailure.Text = "Sie haben eine Zeit mehrfach eingegeben.<br/>Bitte geben Sie eine Zeit höchstens einmal an.";
                                    txtBoxTime1.CssClass = "redBorder";
                                    txtBoxTime2.CssClass = "redBorder";
                                    txtBoxTime3.CssClass = "redBorder";
                                }
                            }
                            else
                            {
                                txtBoxTime1.CssClass = "redBorder";
                                txtBoxTime2.CssClass = "redBorder";
                                txtBoxTime3.CssClass = "redBorder";
                                txtFailure.Text = "Bitte geben Sie mindestens eine Uhrzeit an,<br/>diese muss dem Format hh:mm entsprechen.";
                            }
                        }
                        else
                        {
                            txtFailure.Text = "Mindestens eine Eingabe der Uhrzeit entspricht nicht dem Format hh:mm,<br/>oder keiner reellen Uhrzeit (00:00 bis 23:59).<br/>" + 
                                "Bitte geben Sie jeweils zwei Ziffern für die Stunden (h) und Minuten (m) ein,<br/>getrennt durch einen Doppelpunkt.";
                            txtBoxTime1.CssClass = "redBorder";
                            txtBoxTime2.CssClass = "redBorder";
                            txtBoxTime3.CssClass = "redBorder";
                        }                 
                    }
                    else
                    {
                        txtFailure.Text = "Bitte wählen Sie mindestens einen Tag aus.";
                        btnMonday.CssClass = "btn btn-primary redBorder";
                        btnTuesday.CssClass = "btn btn-primary redBorder";
                        btnWednesday.CssClass = "btn btn-primary redBorder";
                        btnThursday.CssClass = "btn btn-primary redBorder";
                        btnFriday.CssClass = "btn btn-primary redBorder";
                        btnSaturday.CssClass = "btn btn-primary redBorder";
                        btnSunday.CssClass = "btn btn-primary redBorder";
                    }
                }
                else
                {
                    txtFailure.Text = "Bitte wählen Sie genau eines Ihrer Profile aus.";
                    cbProfiles.Style.Add("background-color", "rgba(255, 0, 0, 0.25)");
                }
            }
            else
            {
                txtFailure.Text = "Bitte wählen Sie genau eines Ihrer Profile aus.";
                cbProfiles.Style.Add("background-color", "rgba(255, 0, 0, 0.25)");
            }
        }

        protected void btnMonday_Click(object sender, EventArgs e)
        {
            if (Session["monday"] != null && (bool)Session["monday"] == true)
            {
                btnMonday.BackColor = ColorTranslator.FromHtml("#428bca");
                btnMonday.Style.Add("color", "white");
                Session["monday"] = false;
            }
            else
            {
                btnMonday.BackColor = Color.White;
                btnMonday.Style.Add("color", "black");
                Session["monday"] = true;
            }
        }

        protected void btnTuesday_Click(object sender, EventArgs e)
        {
            if (Session["tuesday"] != null && (bool)Session["tuesday"] == true)
            {
                btnTuesday.BackColor = ColorTranslator.FromHtml("#428bca");
                btnTuesday.Style.Add("color", "white");
                Session["tuesday"] = false;
            }
            else
            {
                btnTuesday.BackColor = Color.White;
                btnTuesday.Style.Add("color", "black");
                Session["tuesday"] = true;
            }
        }

        protected void btnWednesday_Click(object sender, EventArgs e)
        {
            if (Session["wednesday"] != null && (bool)Session["wednesday"] == true)
            {
                btnWednesday.BackColor = ColorTranslator.FromHtml("#428bca");
                btnWednesday.Style.Add("color", "white");
                Session["wednesday"] = false;
            }
            else
            {
                btnWednesday.BackColor = Color.White;
                btnWednesday.Style.Add("color", "black");
                Session["wednesday"] = true;
            }
        }

        protected void btnThursday_Click(object sender, EventArgs e)
        {
            if (Session["thursday"] != null && (bool)Session["thursday"] == true)
            {
                btnThursday.BackColor = ColorTranslator.FromHtml("#428bca");
                btnThursday.Style.Add("color", "white");
                Session["thursday"] = false;
            }
            else
            {
                btnThursday.BackColor = Color.White;
                btnThursday.Style.Add("color", "black");
                Session["thursday"] = true;
            }
        }

        protected void btnFriday_Click(object sender, EventArgs e)
        {
            if (Session["friday"] != null && (bool)Session["friday"] == true)
            {
                btnFriday.BackColor = ColorTranslator.FromHtml("#428bca");
                btnFriday.Style.Add("color", "white");
                Session["friday"] = false;
            }
            else
            {
                btnFriday.BackColor = Color.White;
                btnFriday.Style.Add("color", "black");
                Session["friday"] = true;
            }
        }

        protected void btnSaturday_Click(object sender, EventArgs e)
        {
            if (Session["saturday"] != null && (bool)Session["saturday"] == true)
            {
                btnSaturday.BackColor = ColorTranslator.FromHtml("#428bca");
                btnSaturday.Style.Add("color", "white");
                Session["saturday"] = false;
            }
            else
            {
                btnSaturday.BackColor = Color.White;
                btnSaturday.Style.Add("color", "black");
                Session["saturday"] = true;
            }
        }

        protected void btnSunday_Click(object sender, EventArgs e)
        {
            if (Session["sunday"] != null && (bool)Session["sunday"] == true)
            {
                btnSunday.BackColor = ColorTranslator.FromHtml("#428bca");
                btnSunday.Style.Add("color", "white");
                Session["sunday"] = false;
            }
            else
            {
                btnSunday.BackColor = Color.White;
                btnSunday.Style.Add("color", "black");
                Session["sunday"] = true;
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            Session["firstCall"] = true;
            Response.Redirect("~/PressReview/Overview");
        }
    }
}