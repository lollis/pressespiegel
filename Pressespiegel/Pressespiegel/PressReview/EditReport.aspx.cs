﻿using Pressespiegel.App_Inside;
using RssService.SqlLinq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pressespiegel.PressReview
{
    public partial class ShowReport : System.Web.UI.Page
    {
        EmailOrder currentOrder = new EmailOrder();
        UserProfile currentProfile = new UserProfile();
        protected void Page_Load(object sender, EventArgs e)
        {
            Global.IsLoggedIn(System.Web.HttpContext.Current.User, Response);
            currentOrder = (EmailOrder)Session["orderView"];
            currentProfile = (UserProfile)Session["profileView"];
            lblProfileName.Text = currentProfile.Name + " ";
            if (Session["firstCall"] == null || (bool)Session["firstCall"])
            {
                String broadcastTime = currentOrder.BroadcastTime;
                GetTimeFromBroadcastTime(broadcastTime.Split('?')[1]);
                GetDaysFromBroadcastTime(broadcastTime.Split('?')[0]);

                Session["firstCall"] = false;
            }
            else
            {
                if (Session["time1"] != null && (bool)Session["time1"])
                {
                    txtBoxTime1.Focus();

                    Session["time1"] = false;
                }
                if (Session["time2"] != null && (bool)Session["time2"])
                {
                    txtBoxTime2.Focus();
                    Session["time2"] = false;
                }
                if (Session["time3"] != null && (bool)Session["time3"])
                {
                    txtBoxTime3.Focus();
                    Session["time3"] = false;
                }
            }
            if (Session["daysSaved"] != null && (bool)Session["daysSaved"])
            {
                btnSaveDays.CssClass = "btn btn-success";
                Session["daysSaved"] = false;
            }
            else
            {
                btnSaveDays.CssClass = "btn btn-default";
            }
            if (Session["timeSaved"] != null && (bool)Session["timeSaved"])
            {
                btnSaveTime.CssClass = "btn btn-success";
                Session["timeSaved"] = false;
            }
            else
            {
                btnSaveTime.CssClass = "btn btn-default";
            }
        }

        private void GetTimeFromBroadcastTime(String time)
        {
            txtBoxTime1.Text = time.Split(';')[0];
            if (time.Length > 8)
            {
                txtBoxTime2.Text = time.Split(';')[1];
                if (time.Length > 14)
                {
                    txtBoxTime3.Text = time.Split(';')[2];
                }
            }
        }

        private void GetDaysFromBroadcastTime(String days)
        {
            if (days.Contains("Mo"))
            {
                btnMonday.BackColor = Color.White;
                btnMonday.Style.Add("color", "black");
                Session["monday"] = true;
            }
            else
            {
                Session["monday"] = false;
            }
            if (days.Contains("Di"))
            {
                btnTuesday.BackColor = Color.White;
                btnTuesday.Style.Add("color", "black");
                Session["tuesday"] = true;
            }
            else
            {
                Session["tuesday"] = false;
            }
            if (days.Contains("Mi"))
            {
                btnWednesday.BackColor = Color.White;
                btnWednesday.Style.Add("color", "black");
                Session["wednesday"] = true;
            }
            else
            {
                Session["wednesday"] = false;
            }
            if (days.Contains("Do"))
            {
                btnThursday.BackColor = Color.White;
                btnThursday.Style.Add("color", "black");
                Session["thursday"] = true;
            }
            else
            {
                Session["thursday"] = false;
            }
            if (days.Contains("Fr"))
            {
                btnFriday.BackColor = Color.White;
                btnFriday.Style.Add("color", "black");
                Session["friday"] = true;
            }
            else
            {
                Session["friday"] = false;
            }
            if (days.Contains("Sa"))
            {
                btnSaturday.BackColor = Color.White;
                btnSaturday.Style.Add("color", "black");
                Session["saturday"] = true;
            }
            else
            {
                Session["saturday"] = false;
            }
            if (days.Contains("So"))
            {
                btnSunday.BackColor = Color.White;
                btnSunday.Style.Add("color", "black");
                Session["sunday"] = true;
            }
            else
            {
                Session["sunday"] = false;
            }
        }

        protected void btnMonday_Click(object sender, EventArgs e)
        {
            if (Session["monday"] != null && (bool)Session["monday"] == true)
            {
                btnMonday.BackColor = ColorTranslator.FromHtml("#428bca");
                btnMonday.Style.Add("color", "white");
                Session["monday"] = false;
            }
            else
            {
                btnMonday.BackColor = Color.White;
                btnMonday.Style.Add("color", "black");
                Session["monday"] = true;
            }
        }

        protected void btnTuesday_Click(object sender, EventArgs e)
        {
            if (Session["tuesday"] != null && (bool)Session["tuesday"] == true)
            {
                btnTuesday.BackColor = ColorTranslator.FromHtml("#428bca");
                btnTuesday.Style.Add("color", "white");
                Session["tuesday"] = false;
            }
            else
            {
                btnTuesday.BackColor = Color.White;
                btnTuesday.Style.Add("color", "black");
                Session["tuesday"] = true;
            }
        }

        protected void btnWednesday_Click(object sender, EventArgs e)
        {
            if (Session["wednesday"] != null && (bool)Session["wednesday"] == true)
            {
                btnWednesday.BackColor = ColorTranslator.FromHtml("#428bca");
                btnWednesday.Style.Add("color", "white");
                Session["wednesday"] = false;
            }
            else
            {
                btnWednesday.BackColor = Color.White;
                btnWednesday.Style.Add("color", "black");
                Session["wednesday"] = true;
            }
        }

        protected void btnThursday_Click(object sender, EventArgs e)
        {
            if (Session["thursday"] != null && (bool)Session["thursday"] == true)
            {
                btnThursday.BackColor = ColorTranslator.FromHtml("#428bca");
                btnThursday.Style.Add("color", "white");
                Session["thursday"] = false;
            }
            else
            {
                btnThursday.BackColor = Color.White;
                btnThursday.Style.Add("color", "black");
                Session["thursday"] = true;
            }
        }

        protected void btnFriday_Click(object sender, EventArgs e)
        {
            if (Session["friday"] != null && (bool)Session["friday"] == true)
            {
                btnFriday.BackColor = ColorTranslator.FromHtml("#428bca");
                btnFriday.Style.Add("color", "white");
                Session["friday"] = false;
            }
            else
            {
                btnFriday.BackColor = Color.White;
                btnFriday.Style.Add("color", "black");
                Session["friday"] = true;
            }
        }

        protected void btnSaturday_Click(object sender, EventArgs e)
        {
            if (Session["saturday"] != null && (bool)Session["saturday"] == true)
            {
                btnSaturday.BackColor = ColorTranslator.FromHtml("#428bca");
                btnSaturday.Style.Add("color", "white");
                Session["saturday"] = false;
            }
            else
            {
                btnSaturday.BackColor = Color.White;
                btnSaturday.Style.Add("color", "black");
                Session["saturday"] = true;
            }
        }

        protected void btnSunday_Click(object sender, EventArgs e)
        {
            if (Session["sunday"] != null && (bool)Session["sunday"] == true)
            {
                btnSunday.BackColor = ColorTranslator.FromHtml("#428bca");
                btnSunday.Style.Add("color", "white");
                Session["sunday"] = false;
            }
            else
            {
                btnSunday.BackColor = Color.White;
                btnSunday.Style.Add("color", "black");
                Session["sunday"] = true;
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            Session["firstCall"] = true;
            Response.Redirect("~/PressReview/Overview");
        }

        protected void btnSaveDays_Click(object sender, EventArgs e)
        {
            ReportWorker reportWorker = new ReportWorker();
            String broadcastTime = reportWorker.BuildBroadcastString((bool)Session["monday"], (bool)Session["tuesday"], (bool)Session["wednesday"], (bool)Session["thursday"]
                , (bool)Session["friday"], (bool)Session["saturday"], (bool)Session["sunday"], txtBoxTime1.Text, txtBoxTime2.Text, txtBoxTime3.Text, true, false);
            if (!broadcastTime.Equals("noDay"))
            {
                String time = currentOrder.BroadcastTime.Split('?')[1];
                String newBroadcastTime = broadcastTime + " ?" + time;
                UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
                var emailOrder = db.EmailOrders
                    .Where(w => w.Id == currentOrder.Id)
                    .SingleOrDefault();
                emailOrder.BroadcastTime = newBroadcastTime;
                db.SubmitChanges();
                Session["orderView"] = emailOrder;
                Session["daysSaved"] = true;
                Session["firstCall"] = true;
                Response.Redirect(Request.RawUrl);
            }
            else
            {
                phEditDaysData.Visible = false;
                phEditDaysFail.Visible = true;
            }
        }

        protected void btnSaveTime_Click(object sender, EventArgs e)
        {
            ReportWorker reportWorker = new ReportWorker();
            String broadcastTime = reportWorker.BuildBroadcastString((bool)Session["monday"], (bool)Session["tuesday"], (bool)Session["wednesday"], (bool)Session["thursday"]
                , (bool)Session["friday"], (bool)Session["saturday"], (bool)Session["sunday"], txtBoxTime1.Text, txtBoxTime2.Text, txtBoxTime3.Text, false, true);
            if (!broadcastTime.Equals("noTime"))
            {
                if (!broadcastTime.Equals("wrongFormat"))
                {
                    if (!broadcastTime.Equals("sameTime"))
                    {
                        String days = currentOrder.BroadcastTime.Split('?')[0];
                        String newBroadcastTime = days + " ?" + broadcastTime;
                        UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
                        var emailOrder = db.EmailOrders
                            .Where(w => w.Id == currentOrder.Id)
                            .SingleOrDefault();
                        emailOrder.BroadcastTime = newBroadcastTime;
                        db.SubmitChanges();
                        Session["orderView"] = emailOrder;
                        Session["timeSaved"] = true;
                        Session["firstCall"] = true;
                        Response.Redirect(Request.RawUrl);
                    }
                    else
                    {
                        lblEditTimeFail.Text = "Speichern nicht erfolgreich.<br/>Sie haben eine Zeit mehrfach eingegeben,<br/>bitte geben Sie eine Zeit höchstens einmal an.";
                        phEditTimeData.Visible = false;
                        phEditTimeFail.Visible = true;
                    }
                }
                else
                {
                    lblEditTimeFail.Text = "Speichern nicht erfolgreich.<br/>Mindestens eine Eingabe der Uhrzeit entspricht nicht dem Format hh:mm,<br/>oder keiner reellen Uhrzeit (00:00 bis 23:59).<br/>" +
                                "Bitte geben Sie jeweils zwei Ziffern für die Stunden (h) und Minuten (m) ein,<br/>getrennt durch einen Doppelpunkt.";
                    phEditTimeData.Visible = false;
                    phEditTimeFail.Visible = true;
                }
            }
            else
            {
                lblEditTimeFail.Text = "Speichern nicht erfolgreich.<br/>Bitte geben Sie mindestens eine Uhrzeit an,<br/>diese muss dem Format hh:mm entsprechen.";
                phEditTimeFail.Visible = true;
                phEditTimeData.Visible = false;
            }
        }

        protected void btnDeleteReport_Click(object sender, EventArgs e)
        {
            phDeleteReport.Visible = false;
            phDeleteReportCommit.Visible = true;
        }

        protected void btnOKEditDays_Click(object sender, EventArgs e)
        {
            phEditDaysData.Visible = true;
            phEditDaysFail.Visible = false;
            Session["firstCall"] = true;
            Response.Redirect(Request.RawUrl);
        }

        protected void btnOKEditTime_Click(object sender, EventArgs e)
        {
            phEditTimeData.Visible = true;
            phEditTimeFail.Visible = false;
            Session["firstCall"] = true;
            Response.Redirect(Request.RawUrl);
        }

        protected void btnDeleteReportCommit_Click(object sender, EventArgs e)
        {
            UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
            var emailOrder = db.EmailOrders
                .Where(w => w.Id == currentOrder.Id)
                .SingleOrDefault();
            db.EmailOrders.DeleteOnSubmit(emailOrder);
            db.SubmitChanges();

            Session["firstCall"] = true;
            Response.Redirect("~/PressReview/Overview");
        }

        protected void btnDeleteReportUndo_Click(object sender, EventArgs e)
        {
            phDeleteReport.Visible = true;
            phDeleteReportCommit.Visible = false;
        }
    }
}