﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Pressespiegel.DataProvider;
using RssService;
using RssService.SqlLinq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.IO;

namespace Pressespiegel.PressReview
{
    public partial class ShowProfile : System.Web.UI.Page
    {
        public UserProfile currentProfile;
        protected void Page_Load(object sender, EventArgs e)
        {
            Global.IsLoggedIn(System.Web.HttpContext.Current.User, Response);

            labelNoResults.Visible = false;
            if (Session["profileView"] != null)
            {
                currentProfile = (UserProfile)Session["profileView"];
                labelProfile_Name.Text = currentProfile.Name;
                if (currentProfile.Logo != null && currentProfile.Logo.Length > 10)
                {
                    imageProfile.ImageUrl = "data:image;base64," + currentProfile.Logo;
                }
                else
                {
                    imageProfile.Visible = false;
                }
                imageProfile.Style.Add("width", "10%");
                if (currentProfile.IsPublic)
                {
                    btnShareProfile.Visible = false;
                    lblProfileShared.Visible = true;
                    UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
                    List<SharedProfiles> subsProfiles = new List<SharedProfiles>();
                    subsProfiles.AddRange(from sp in db.SharedProfiles
                                          where sp.UserId == Context.User.Identity.GetUserId()
                                          where sp.ProfileId == currentProfile.Id
                                          select sp);
                    if(subsProfiles.Count == 0)
                    {
                        lblProfileShared.Text = "Geteilt";
                    }
                    else
                    {  
                        lblProfileShared.Text = "Abonniert";
                        btnEditProfile.Text = "Deabonnieren";
                        btnEditProfile.ToolTip = "Löschen Sie das abonnierte Profil aus Ihrer Liste. Es wird anschließend wieder unter 'Öffentliche Profile' zum abonnieren angezeigt.";
                    }
                }
            }
            ElasticSearch search = new ElasticSearch();
            //List<RssItem> items = search.GetProfileContent(currentProfile.Id);
            List<RssItem> items = search.SearchByID(currentProfile.Id);
            HyperLink hl;
            TableCell tcImgProv, tcImgFeed, tcHl, tcCont, tcDate, tcTable;
            Image imgProv, imgFeed;
            Label date, cont;
            Table table;
            Dictionary<string, KeyValuePair<string, string>> provDic = LoadProviderImage();
            KeyValuePair<string, string> image;
            if (items != null && items.Count > 0)
            {
                foreach (RssItem item in items)
                {
                    tcImgProv = new TableCell() { Width = 60 };
                    imgProv = new Image();

                    provDic.TryGetValue(item.ServiceName, out image);
                    //key=image, value=path

                    if (!string.IsNullOrEmpty(image.Value))
                    {
                        provDic.TryGetValue(item.ServiceName, out image);
                        imgProv.ImageUrl = "data:image;base64," + image.Value;
                    }
                    imgProv.Style.Add("width", "100%");
                    tcImgProv.Controls.Add(imgProv);
                    tcHl = new TableCell() { Width = 740 };
                    tcDate = new TableCell();
                    date = new Label();
                    date.Text = item.Date.ToString("G");
                    date.Font.Size = new FontUnit(10);
                    tcDate.VerticalAlign = VerticalAlign.Top;
                    tcDate.Controls.Add(date);
                    hl = new HyperLink();
                    hl.NavigateUrl = item.Url;
                    hl.Text = item.Title;
                    hl.Target = "_blank";

                    tcHl.Controls.Add(hl);


                    tcImgFeed = new TableCell() { Width = 160 };
                    if (item.BildUrl != null && item.BildUrl.Length > 10)
                    {
                        imgFeed = new Image();
                        imgFeed.ImageUrl = item.BildUrl;
                        imgFeed.Style.Add("width", "150px");
                        tcImgFeed.Controls.Add(imgFeed);
                        tcCont = new TableCell() { Width = 580 };
                    }
                    else
                    {
                        tcCont = new TableCell() { Width = 740 };
                    }
                    cont = new Label();
                    cont.Text = item.Content;
                    cont.Font.Size = new FontUnit(11);
                    tcCont.Controls.Add(cont);

                    searchResults.Rows.Add(new TableRow());
                    searchResults.Rows[searchResults.Rows.Count - 1].Cells.Add(tcImgProv);
                    searchResults.Rows[searchResults.Rows.Count - 1].Cells.Add(tcHl);

                    searchResults.Rows.Add(new TableRow());
                    searchResults.Rows[searchResults.Rows.Count - 1].Cells.Add(new TableCell() { Width = 60 });
                    searchResults.Rows[searchResults.Rows.Count - 1].Cells.Add(tcDate);

                    if (item.BildUrl != null && item.BildUrl.Length > 10)
                    {
                        tcTable = new TableCell();
                        table = new Table();
                        table.Rows.Add(new TableRow());
                        table.Rows[0].Cells.Add(tcImgFeed);
                        table.Rows[0].Cells.Add(tcCont);
                        tcTable.Controls.Add(table);

                        searchResults.Rows.Add(new TableRow());
                        searchResults.Rows[searchResults.Rows.Count - 1].Cells.Add(new TableCell() { Width = 60 });
                        searchResults.Rows[searchResults.Rows.Count - 1].Cells.Add(tcTable);

                    }
                    else
                    {
                        searchResults.Rows.Add(new TableRow());
                        searchResults.Rows[searchResults.Rows.Count - 1].Cells.Add(new TableCell() { Width = 60 });
                        searchResults.Rows[searchResults.Rows.Count - 1].Cells.Add(tcCont);
                    }
                    searchResults.Rows.Add(new TableRow() { Height = 25 });
                    searchResults.DataBind();
                }
            }
            else
            {
                labelNoResults.Visible = true;
            }
        }

        internal Dictionary<string, KeyValuePair<string, string>> LoadProviderImage()
        {
            UserProfileViewDataContext db = new UserProfileViewDataContext(MainWindow.ConnectionString);
            Dictionary<string, KeyValuePair<string, string>> providerDic = new Dictionary<string, KeyValuePair<string, string>>();
            foreach (RssService.SqlLinq.Provider prov in db.Providers)
            {
                string path = Path.Combine(new string[] { AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), "img" + new Random().Next().ToString() + ".png" }).ToString();
                providerDic.Add(prov.Name, new KeyValuePair<string, string>(path, prov.Logo));
            }
            return providerDic;
        }

        protected void btnEditProfile_Click(object sender, EventArgs e)
        {
            if (btnEditProfile.Text.Equals("Bearbeiten"))
            {
                Session["profileView"] = currentProfile;
                Session["firstCall"] = true;
                Response.Redirect("~/PressReview/EditProfile");
            }
            else
            {
                using (var db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString))
                {
                    var sharedProfile = db.SharedProfiles
                        .Where(w => w.UserId == User.Identity.GetUserId())
                        .Where(w => w.ProfileId == currentProfile.Id)
                        .SingleOrDefault();
                    db.SharedProfiles.DeleteOnSubmit(sharedProfile);
                    db.SubmitChanges();
                }
                Session["firstCall"] = true;
                Response.Redirect("~/PressReview/Overview");
            }
        }

        protected void btnShareProfile_Click(object sender, EventArgs e)
        {
            UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
            var userProfile = db.UserProfiles
                .Where(w => w.Id == currentProfile.Id)
                .SingleOrDefault();
            userProfile.IsPublic = true;
            db.SubmitChanges();
            btnShareProfile.Visible = false;
            lblProfileShared.Visible = true;
            lblProfileShared.Text = "Profil geteilt";
        }

      

        /// <summary>
        /// Findet alle Öffentlichen Profile auf die der bunutzer zugriff hat
        /// </summary>
        /// <returns>Öffentliche Profile</returns>
        private List<V_FindSharedProfile> GetSharedProfiles()
        {
            using (var db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString))
            {
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var user = manager.FindById(User.Identity.GetUserId());
                string orga = user.PhoneNumber.ToLower();
                var accessableProfiles = from t in db.V_FindSharedProfiles
                                         where System.Data.Linq.SqlClient.SqlMethods.Equals(t.Organization, user.PhoneNumber)
                                         select t;
                List<V_FindSharedProfile> profiles = new List<V_FindSharedProfile>();
                profiles.AddRange(accessableProfiles);
                return profiles;
            }
        }

      
    }
}