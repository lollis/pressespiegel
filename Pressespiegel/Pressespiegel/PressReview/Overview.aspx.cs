﻿using Microsoft.AspNet.Identity;
using Pressespiegel.DataProvider;
using RssService;
using RssService.SqlLinq;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using System.Linq;
using System.Text;
using System.IO;

namespace Pressespiegel.PressReview
{
    public partial class Overview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Global.IsLoggedIn(System.Web.HttpContext.Current.User, Response);
            labelInfo.Visible = false;
            lblNoProvider.Visible = false;
            UserProfileViewDataContext db = new UserProfileViewDataContext(MainWindow.ConnectionString);
            List<string> provList = new List<string>();
            provList.AddRange(from p in db.Providers select p.Name);
            StringBuilder provNames = new StringBuilder();
            foreach (string s in provList)
            {
                provNames.Append(s + "\n");
            }
            txtServiceName.ToolTip = provNames.ToString();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            bool containsProv = false;
            bool provAvailable = false;
            if (txtServiceName.Text != null && txtServiceName.Text.Length > 0)
            {
                containsProv = true;
                UserProfileViewDataContext db = new UserProfileViewDataContext(MainWindow.ConnectionString);
                List<string> provList = new List<string>();
                provList.AddRange(from p in db.Providers select p.Name);
                StringBuilder provNames = new StringBuilder();
                string provName = txtServiceName.Text;
                foreach (string s in provList)
                {
                    if (provName.Equals(s, StringComparison.InvariantCultureIgnoreCase))
                    {
                        provAvailable = true;
                    }
                }
            }
            if ((containsProv && provAvailable) || !containsProv)
            {
                DataProvider.Provider prov = new DataProvider.Provider(User.Identity.GetUserId());
                List<RssItem> items = prov.Search(new SearchItem(txtServiceName.Text, txtContent.Text));
                HyperLink hl;
                TableCell tcImgProv, tcImgFeed, tcHl, tcCont, tcDate, tcTable;
                Image imgProv, imgFeed;
                Label date, cont;
                Table table;

                //UserProfileViewDataContext db = new UserProfileViewDataContext(MainWindow.ConnectionString);
                //Dictionary<string, string> providerDic = db.Providers.Select(t => new { t.Name, t.Logo }).ToDictionary(t => t.Name, t => t.Logo);
                //string imgProvider;

                Dictionary<string, KeyValuePair<string, string>> provDic = LoadProviderImage();
                KeyValuePair<string, string> image;
                if (items != null && items.Count > 0)
                {
                    foreach (RssItem item in items)
                    {
                        tcImgProv = new TableCell() { Width = 60 };
                        imgProv = new Image();

                        provDic.TryGetValue(item.ServiceName, out image);
                        //key=image, value=path

                        if (!string.IsNullOrEmpty(image.Value))
                        {
                            provDic.TryGetValue(item.ServiceName, out image);
                            imgProv.ImageUrl = "data:image;base64," + image.Value;
                        }
                        imgProv.Style.Add("width", "100%");
                        tcImgProv.Controls.Add(imgProv);
                        tcHl = new TableCell() { Width = 740 };
                        tcDate = new TableCell();
                        date = new Label();
                        date.Text = item.Date.ToString("G");
                        date.Font.Size = new FontUnit(10);
                        tcDate.VerticalAlign = VerticalAlign.Top;
                        tcDate.Controls.Add(date);
                        hl = new HyperLink();
                        hl.NavigateUrl = item.Url;
                        hl.Text = item.Title;
                        hl.Target = "_blank";

                        tcHl.Controls.Add(hl);


                        tcImgFeed = new TableCell() { Width = 160 };
                        if (item.BildUrl != null && item.BildUrl.Length > 10)
                        {
                            imgFeed = new Image();
                            imgFeed.ImageUrl = item.BildUrl;
                            imgFeed.Style.Add("width", "150px");
                            tcImgFeed.Controls.Add(imgFeed);
                            tcCont = new TableCell() { Width = 580 };
                        }
                        else
                        {
                            tcCont = new TableCell() { Width = 740 };
                        }
                        cont = new Label();
                        cont.Text = item.Content;
                        cont.Font.Size = new FontUnit(11);
                        tcCont.Controls.Add(cont);

                        searchResults.Rows.Add(new TableRow());
                        searchResults.Rows[searchResults.Rows.Count - 1].Cells.Add(tcImgProv);
                        searchResults.Rows[searchResults.Rows.Count - 1].Cells.Add(tcHl);

                        searchResults.Rows.Add(new TableRow());
                        searchResults.Rows[searchResults.Rows.Count - 1].Cells.Add(new TableCell() { Width = 60 });
                        searchResults.Rows[searchResults.Rows.Count - 1].Cells.Add(tcDate);

                        if (item.BildUrl != null && item.BildUrl.Length > 10)
                        {
                            tcTable = new TableCell();
                            table = new Table();
                            table.Rows.Add(new TableRow());
                            table.Rows[0].Cells.Add(tcImgFeed);
                            table.Rows[0].Cells.Add(tcCont);
                            tcTable.Controls.Add(table);
                            
                            searchResults.Rows.Add(new TableRow());
                            searchResults.Rows[searchResults.Rows.Count - 1].Cells.Add(new TableCell() { Width = 60 });
                            searchResults.Rows[searchResults.Rows.Count - 1].Cells.Add(tcTable);
                            
                        }
                        else
                        {
                            searchResults.Rows.Add(new TableRow());
                            searchResults.Rows[searchResults.Rows.Count - 1].Cells.Add(new TableCell() { Width = 60 });
                            searchResults.Rows[searchResults.Rows.Count - 1].Cells.Add(tcCont);
                        }
                        searchResults.Rows.Add(new TableRow() { Height = 25 });
                        searchResults.DataBind();
                    }
                }
                else
                {
                    labelInfo.Visible = true;
                }
            }
            else
            {
                lblNoProvider.Visible = true;
            }
        }

        internal Dictionary<string, KeyValuePair<string, string>> LoadProviderImage()
        {
            UserProfileViewDataContext db = new UserProfileViewDataContext(MainWindow.ConnectionString);
            Dictionary<string, KeyValuePair<string, string>> providerDic = new Dictionary<string, KeyValuePair<string, string>>();
            foreach (RssService.SqlLinq.Provider prov in db.Providers)
            {
                string path = Path.Combine(new string[] { AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), "img" + new Random().Next().ToString() + ".png" }).ToString();
                providerDic.Add(prov.Name, new KeyValuePair<string, string>(path, prov.Logo));
            }
            return providerDic;
        }

        public System.Drawing.Image resizeImage(System.Drawing.Image imgToResize, System.Drawing.Size size)
        {
            return (System.Drawing.Image)(new System.Drawing.Bitmap(imgToResize, size));
        }
    }
}