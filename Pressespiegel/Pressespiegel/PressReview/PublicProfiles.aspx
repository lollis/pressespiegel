﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Inside/Main.master" AutoEventWireup="true" CodeBehind="PublicProfiles.aspx.cs" Inherits="Pressespiegel.PressReview.PublicProfiles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TabContent" runat="server">
    <div class="transparent mainBlockRadius">
        <div class="row">
            <h3 style="text-align: center;">
                Öffentliche Profile           
            </h3>         
            <hr style="border: solid #008CBA 2px; height: 1px; width: 95%;" />
        </div>
        <asp:PlaceHolder runat="server" ID="phTblProfiles">
        <asp:Table runat="server" ID="tblPublicProfiles" BorderWidth="0" BackColor="Transparent" HorizontalAlign="Center">

        </asp:Table>
            </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="phNoProfiles" Visible="false">
            <div class="center">
            <h5>Hier finden sie alle Profile, die innerhalb ihrer Organisation geteilt wurden.<br /><br />
                Im Moment stehen keine geteilten Profile zur Verfügung.
            </h5>
                </div>
        </asp:PlaceHolder>
        <br />
    </div>
</asp:Content>
