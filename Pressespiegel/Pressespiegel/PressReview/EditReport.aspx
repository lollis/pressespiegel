﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Inside/Main.master" AutoEventWireup="true" CodeBehind="EditReport.aspx.cs" Inherits="Pressespiegel.PressReview.ShowReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TabContent" runat="server">
    <div class="transparent mainBlockRadius">
        <div class="row">
            <h3 style="text-align: center">
                <asp:Label runat="server" ID="lblProfileName"></asp:Label>
                - Dauerauftrag
            </h3>
            <hr style="border: solid #008CBA 2px; height: 1px; width: 95%;" />
        </div>
        <h5 style="text-indent: 15px;">Ändern Sie die Tage, an denen Sie den aktuellen Pressespiegel erhalten möchten:</h5>
        <br />
        <asp:PlaceHolder runat="server" ID="phEditDaysData">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="text-center">
                        <h6>Weiß hinterlegte Tage (durch klicken) gehören mit zu der Auswahl.
                        </h6>
                        <div class="btn-group">
                            <asp:Button CssClass="btn btn-primary" ID="btnMonday" OnClick="btnMonday_Click" runat="server" Text="Montag" />
                            <asp:Button CssClass="btn btn-primary" ID="btnTuesday" OnClick="btnTuesday_Click" runat="server" Text="Dienstag" />
                            <asp:Button CssClass="btn btn-primary" ID="btnWednesday" OnClick="btnWednesday_Click" runat="server" Text="Mittwoch" />
                            <asp:Button CssClass="btn btn-primary" ID="btnThursday" OnClick="btnThursday_Click" runat="server" Text="Donnerstag" />
                            <asp:Button CssClass="btn btn-primary" ID="btnFriday" OnClick="btnFriday_Click" runat="server" Text="Freitag" />
                            <asp:Button CssClass="btn btn-primary" ID="btnSaturday" OnClick="btnSaturday_Click" runat="server" Text="Samstag" />
                            <asp:Button CssClass="btn btn-primary" ID="btnSunday" OnClick="btnSunday_Click" runat="server" Text="Sonntag" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <div style="text-indent: 20%">
                <asp:Button runat="server" ID="btnSaveDays" CssClass="btn btn-default" Text="Speichern" OnClick="btnSaveDays_Click" />
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="phEditDaysFail" Visible="false">
            <div style="text-align: center" class="text-danger">
                <h5>Speichern nicht erfolgreich.<br />
                    Bitte wählen Sie mindestens einen Tag aus.
                </h5>
                <asp:Button runat="server" ID="btnOKEditDays" OnClick="btnOKEditDays_Click" CssClass="btn btn-default" Text="OK" />
            </div>
        </asp:PlaceHolder>

        <hr style="border: ridge #969696 0.2px; height: 0; width: 99%;" />
        <h5 style="text-indent: 15px;">Ändern Sie die Uhrzeiten, zu denen Sie an den ausgewählten Tagen jeweils einen 
            Pressespiegel erhalten möchten:</h5>
        <br />
        <asp:PlaceHolder runat="server" ID="phEditTimeData">
            <div style="text-align: center;">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:Table runat="server" BackColor="Transparent" HorizontalAlign="Center" BorderWidth="0" CellSpacing="35">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:TextBox Width="50px" placeholder="hh:mm" runat="server" ID="txtBoxTime1" MaxLength="5"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox runat="server" Width="50px" placeholder="hh:mm" ID="txtBoxTime2" MaxLength="5"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox runat="server" Width="50px" placeholder="hh:mm" ID="txtBoxTime3" MaxLength="5"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <br />
            <div style="text-indent: 20%">
                <asp:Button runat="server" ID="btnSaveTime" CssClass="btn btn-default" Text="Speichern" OnClick="btnSaveTime_Click" />
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="phEditTimeFail" Visible="false">
            <div style="text-align: center" class="text-danger">
                <h5>
                    <asp:Label runat="server" ID="lblEditTimeFail"></asp:Label>
                </h5>
                <asp:Button runat="server" ID="btnOKEditTime" OnClick="btnOKEditTime_Click" CssClass="btn btn-default" Text="OK" />
            </div>
        </asp:PlaceHolder>
        <hr style="border: ridge #969696 0.2px; height: 0; width: 99%;" />
        <div style="text-align: center">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:PlaceHolder runat="server" ID="phDeleteReport">
                        <asp:Button ID="btnDeleteReport" runat="server" OnClick="btnDeleteReport_Click" Text="Auftrag löschen" BackColor="#ff9191" CssClass="btn btn-default" />
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="phDeleteReportCommit" Visible="false">
                        <h5>Möchten Sie den Dauerauftrag wirklich löschen?</h5>
                        <asp:Button runat="server" ID="btnDeleteReportCommit" Text="Ja" OnClick="btnDeleteReportCommit_Click" CssClass="btn btn-default margin10px" />
                        <asp:Button runat="server" ID="btnDeleteReportUndo" OnClick="btnDeleteReportUndo_Click" Text="Nein" CssClass="btn btn-default margin10px" />
                    </asp:PlaceHolder>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <br />
    </div>
</asp:Content>
