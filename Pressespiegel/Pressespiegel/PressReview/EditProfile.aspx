﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Inside/Main.master" AutoEventWireup="true" CodeBehind="EditProfile.aspx.cs" Inherits="Pressespiegel.PressReview.EditProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TabContent" runat="server">
    <div class="transparent mainBlockRadius">
        <div class="row">
            <h3 style="text-align: center;">
                <asp:Image runat="server" ID="imageProfile" />
                <asp:Label runat="server" ID="labelProfile_Name"></asp:Label>
                <asp:Button runat="server" Text="Zurück" OnClick="btnBack_Click" CssClass="btn btn-default btnBearbeitein" ID="btnBack" />
            </h3>
            <hr style="border: solid #008CBA 2px; height: 1px; width: 95%;" />

            <table style="width: 700px;">
                <tr>
                    <td style="text-indent: 10%">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:PlaceHolder runat="server" ID="phEditProfileName">
                                    <asp:Button CssClass="btn btn-default" runat="server" ID="btnEditProfileName" OnClick="btnEditProfileName_Click" Text="Profilnamen bearbeiten" />
                                    <asp:TextBox Visible="false" runat="server" MaxLength="30" ID="txtBoxNewProfileName" placeholder="Neuer Profilname" ToolTip="Profilname bestehend aus Groß-, Kleinbuchstaben und Ziffern und einem bis 30 Zeichen lang"></asp:TextBox>
                                    <asp:Button Visible="false" CssClass="btn btn-default" runat="server" ID="btnSaveProfileName" OnClick="btnSaveProfileName_Click" Text="Speichern" />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder runat="server" ID="phEditProfileNameText" Visible="false">
                                    <div style="text-align: center">
                                        <h5 class="text-danger">Speichern nicht erfolgreich.<br />
                                            Der Profilname darf nur aus Groß-,<br />
                                            Kleinbuchstaben und Ziffern bestehen.<br />
                                            Außerdem muss er zwischen einschließlich<br />
                                            einem und 30 Zeichen lang sein.
                                        </h5>
                                        <asp:Button Text="OK" runat="server" ID="btnOKEditProfileName" CssClass="btn btn-default" OnClick="btnOKEditProfileName_Click" />
                                    </div>
                                </asp:PlaceHolder>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td style="text-align: right">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:PlaceHolder runat="server" ID="phEditProfileLogo">
                                    <asp:Button CssClass="btn btn-default" runat="server" ID="btnEditProfileLogo2" OnClick="btnEditProfileLogo2_Click" Text="Profillogo bearbeiten" />
                                    <asp:FileUpload Visible="false" runat="server" ID="fileProfileLogo2" />
                                    <asp:Button Visible="false" CssClass="btn btn-default" runat="server" ID="btnSaveProfileLogo2" OnClick="btnSaveProfileLogo2_Click" Text="Speichern" />
                                    <br />
                                    <div style="text-align: center">
                                        <asp:Label ID="labelMaxLength" Text="(Max. 5MB)" Visible="false" runat="server"></asp:Label>
                                    </div>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder runat="server" ID="phEditProfileLogoText" Visible="false">
                                    <div style="text-align: center; color: red">
                                        <asp:Label CssClass="text-danger" Visible="false" runat="server" ID="labelNoFile" Text="Speichern nicht erfolgreich.<br/>Bitte wählen Sie eine Datei aus."></asp:Label>
                                        <asp:Label CssClass="text-danger" runat="server" Visible="false" ID="labelOversize" Text="Speichern nicht erfolgreich.<br/>Bitte wählen Sie eine kleinere Datei aus"></asp:Label>
                                        <asp:Label CssClass="text-danger" runat="server" ID="labelNoImage" Visible="false" Text="Speichern nicht erfolgreich.<br/>Unterstützte Formate: jpg, jpeg, pjpeg, png, x-png, gif"></asp:Label>
                                        <br />
                                        <asp:Button runat="server" ID="btnOKEditProfileLogo" Text="OK" CssClass="btn btn-default" OnClick="btnOKEditProfileLogo_Click" />
                                    </div>
                                </asp:PlaceHolder>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnSaveProfileLogo2" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>
        <hr style="border: ridge #969696 0.2px; height: 0; width: 99%;" />
        <h5 style="text-indent: 15px;">Suchbegriffe Löschen und Hinzufügen:</h5>
        <asp:PlaceHolder runat="server" ID="phEditSearchTerms">
            <asp:UpdatePanel ID="upSearchTerms" runat="server">
                <ContentTemplate>
                    <asp:Table HorizontalAlign="Center" runat="server" ID="searchTerms" Font-Size="Medium" BackColor="Transparent" BorderWidth="0" ForeColor="Black" CellPadding="5"
                        CellSpacing="20">
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox0" ToolTip="Suchbegriff aus Groß-, Kleinbuchstaben und Ziffern. Mindestens zwei Zeichen lang." placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox1" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox2" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox3" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox4" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox5" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox6" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox7" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox8" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox9" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox10" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox11" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox12" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox13" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox14" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox15" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox16" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox17" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox18" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox19" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox20" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox21" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox22" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox23" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox24" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox25" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox26" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox27" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox28" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox29" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox30" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox31" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox32" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox33" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox34" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox35" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox36" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox37" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox38" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox39" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox40" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox41" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox42" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox43" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox44" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox45" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox46" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox47" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox48" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox49" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox50" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox51" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox52" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox53" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox54" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox55" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox56" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox57" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox58" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox59" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox60" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox61" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox62" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox63" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox64" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox65" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox66" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox67" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox68" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox69" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox70" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox71" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox72" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox73" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox74" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox75" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox76" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox77" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox78" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox79" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox80" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox81" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox82" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox83" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox84" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox85" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox86" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox87" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox88" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox89" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox90" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox91" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox92" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox93" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox94" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox95" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox96" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox97" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox98" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Visible="false">
                            <asp:TableCell>
                                <asp:TextBox runat="server" ID="TextBox99" placeholder="Suchbegriff"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow CssClass="center">
                            <asp:TableCell>
                                <asp:ImageButton ID="btnPlus" ImageUrl="~/fonts/blue-plus-icon-6.png" OnClick="btnPlus_Click" Height="30px" Width="30px" runat="server" ToolTip="Weiteren Suchbegriff hinzufügen" />
                                <asp:ImageButton ID="btnRemove" ImageUrl="~/fonts/blue-minus-icon.png" OnClick="btnRemove_Click" Height="30px" Width="30px" runat="server" ToolTip="Suchbegriff entfernen" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnPlus" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnRemove" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>

            <div style="text-indent: 20%">
                <asp:Button runat="server" ID="btnSaveSearchTerms" CssClass="btn btn-default" Text="Speichern" OnClick="btnSaveSearchTerms_Click" />
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="phEditSearchTermsText" Visible="false">
            <br />
            <div style="text-align: center" class="text-danger">
                <h5>Speichern nicht erfolgreich.<br />
                    Bitte geben Sie mindestens einen Suchbegriff ein, diese dürfen nur aus Groß-, Kleinbuchstaben und Ziffern bestehen<br />
                    und müssen mindestens zwei Zeichen lang sein.
                </h5>
                <asp:Button runat="server" ID="btnOKEditSearchTerms" OnClick="btnOKEditSearchTerms_Click" CssClass="btn btn-default" Text="OK" />
            </div>
        </asp:PlaceHolder>
        <hr style="border: ridge #969696 0.2px; height: 0; width: 99%;" />
        <h5 style="text-indent: 15px;">Mediendienstleister, die mit in die Suche einbezogen werden:</h5>
        <asp:PlaceHolder runat="server" ID="phEditProviders">
            <div style="text-align: center">
                <asp:Table HorizontalAlign="Center" Font-Size="Small" BackColor="Transparent" ForeColor="Black" CellPadding="4"
                    runat="server" ID="cbProviders" Style="text-align: left; width: 600px" CellSpacing="5">
                </asp:Table>
            </div>
            <div style="text-indent: 20%">
                <asp:Button runat="server" ID="btnSaveProviders" CssClass="btn btn-default" Text="Speichern" OnClick="btnSaveProviders_Click" />
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="phEditProvidersText" Visible="false">
            <div style="text-align: center" class="text-danger">
                <br />
                <h5>Speichern nicht erfolgreich.<br />
                    Wählen Sie mindestens einen Mediendienstleister aus,<br />
                    der mit in die Suche einbezogen werden soll.
                </h5>
                <asp:Button runat="server" Text="OK" ID="btnOKEditProviders" OnClick="btnOKEditProviders_Click" CssClass="btn btn-default" />
            </div>
        </asp:PlaceHolder>
        <hr style="border: ridge #969696 0.2px; height: 0; width: 99%;" />
        <div style="text-align: center">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:PlaceHolder runat="server" ID="deleteButtonVisible">
                        <asp:Button ID="btnDeleteProfile" runat="server" BackColor="#ff9191" OnClick="btnDeleteProfile_Click" Text="Profil Löschen" CssClass="btn btn-default" />
                        
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="deleteProfileCommit" Visible="false">
                        <h5>Möchten Sie das Profil wirklich löschen?</h5>
                        <h5>Falls dieses Profil geteilt ist, wird es bei anderen Benutzern,<br />
                            die dieses Profil abonniert haben ebenfalls gelöscht.
                        </h5>
                        <asp:Button runat="server" ID="btnDeleteProfileCommit" Text="Ja" OnClick="btnDeleteProfileCommit_Click" CssClass="btn btn-default margin10px" />
                        <asp:Button runat="server" ID="btnDeleteProfileUndo" OnClick="btnDeleteProfileUndo_Click" Text="Nein" CssClass="btn btn-default margin10px" />

                    </asp:PlaceHolder>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <br />
    </div>
</asp:Content>
