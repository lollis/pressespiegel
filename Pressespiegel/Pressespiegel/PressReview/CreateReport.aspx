﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Inside/Main.master" AutoEventWireup="true" CodeBehind="CreateReport.aspx.cs" Inherits="Pressespiegel.PressReview.CreateReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TabContent" runat="server">
    <div class="transparent mainBlockRadius">
        <asp:PlaceHolder runat="server" ID="phReportingData" Visible="true">
            <div class="row">
                <h3 style="text-align: center">Dauerauftrag einrichten
                </h3>
                <hr style="border: solid #008CBA 2px; height: 1px; width: 95%;" />
            </div>
            <h5 style="text-indent: 15px;">Wählen Sie eines Ihrer Profile aus, von dem Sie einen aktuellen Pressespiegel erhalten möchten:</h5>
            <div style="text-align: center">
                <asp:Table HorizontalAlign="Center" Font-Size="Small" BackColor="Transparent" ForeColor="Black" CellPadding="4"
                    runat="server" ID="cbProfiles" Style="text-align: left; width: 600px" CellSpacing="5">
                </asp:Table>
            </div>
            <hr style="border: ridge #969696 0.2px; height: 0; width: 99%;" />
            <h5 style="text-indent: 15px;">Wählen Sie aus, an welchen Tagen und zu welcher Uhrzeit Sie den aktuellen Pressespiegel erhalten möchten:</h5>
            <br />
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="text-center">
                        <h6>Weiß hinterlegte Tage (durch klicken) gehören mit zu der Auswahl.
                        </h6>
                        <div class="btn-group">
                            <asp:Button CssClass="btn btn-primary" ID="btnMonday" OnClick="btnMonday_Click" runat="server" Text="Montag" />
                            <asp:Button CssClass="btn btn-primary" ID="btnTuesday" OnClick="btnTuesday_Click" runat="server" Text="Dienstag" />
                            <asp:Button CssClass="btn btn-primary" ID="btnWednesday" OnClick="btnWednesday_Click" runat="server" Text="Mittwoch" />
                            <asp:Button CssClass="btn btn-primary" ID="btnThursday" OnClick="btnThursday_Click" runat="server" Text="Donnerstag" />
                            <asp:Button CssClass="btn btn-primary" ID="btnFriday" OnClick="btnFriday_Click" runat="server" Text="Freitag" />
                            <asp:Button CssClass="btn btn-primary" ID="btnSaturday" OnClick="btnSaturday_Click" runat="server" Text="Samstag" />
                            <asp:Button CssClass="btn btn-primary" ID="btnSunday" OnClick="btnSunday_Click" runat="server" Text="Sonntag" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <hr style="border: ridge #969696 0.2px; height: 0; width: 99%;" />
            <h5 style="text-indent: 15px;">Geben Sie bis zu drei Uhrzeiten an, zu denen Sie an den ausgewählten Tagen jeweils einen 
            Pressespiegel des gewählten</h5>
            <h5 style="text-indent: 15px;">Profils erhalten möchten:</h5>
            <br />
            <div style="text-align: center;">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:Table runat="server" BackColor="Transparent" HorizontalAlign="Center" BorderWidth="0" CellSpacing="35">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:TextBox Width="50px" placeholder="hh:mm" runat="server" MaxLength="5" ID="txtBoxTime1"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox runat="server" Width="50px" placeholder="hh:mm" ID="txtBoxTime2" MaxLength="5"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox runat="server" Width="50px" placeholder="hh:mm" ID="txtBoxTime3" MaxLength="5"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <hr style="border: ridge #969696 0.2px; height: 0; width: 99%;" />
            <div style="text-align: center">
                <asp:Button ID="btnCreateReport" runat="server" OnClick="btnCreateReport_Click" Text="Auftrag einrichten" CssClass="btn btn-default" />
                <br />
                <asp:Label runat="server" CssClass="text-danger" ID="txtFailure"></asp:Label>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="phReportCreated" Visible="false">
            <div class="row center">
                <br />
                <h4 style="text-align: center;">Der Dauerauftrag zu dem Profil '<asp:Label ID="lblProfileName" runat="server"></asp:Label>
                    wurde erstellt.<br />
                    <br />
                    Sie erhalten den Pressespiegel an folgenden Tagen:<br />
                    <asp:Label runat="server" ID="lblSelectedDays"></asp:Label>.<br />
                    <br />
                    Zu folgender Uhrzeit:
                    <asp:Label runat="server" ID="lblTime"></asp:Label>.
                </h4>
                <br />
                <asp:Button runat="server" CssClass="btn btn-default" ID="btnOK" Text="OK" OnClick="btnOK_Click" />
                <br />
            </div>
        </asp:PlaceHolder>
        <br />
    </div>
</asp:Content>
