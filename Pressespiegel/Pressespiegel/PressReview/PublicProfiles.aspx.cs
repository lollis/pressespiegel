﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using RssService.SqlLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pressespiegel.PressReview
{
    public partial class PublicProfiles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Global.IsLoggedIn(System.Web.HttpContext.Current.User, Response);
            List<UserProfile> userSites = new List<UserProfile>();
            DataProvider.Provider provider = new DataProvider.Provider(HttpContext.Current.User.Identity.GetUserId());
            userSites = provider.GetUserSites();
            bool subscribed;
            TableCell tcBtnAdd, tcLbl;
            Label lbl;
            Button btnAdd;
            foreach (V_FindSharedProfile sharedP in GetSharedProfiles())
            {
                subscribed = false;
                foreach (V_SubscribedProfile subsP in FindSubscribedProfiles())
                {
                    if (sharedP.ProfileId == subsP.ProfileId)
                    {
                        subscribed = true;
                    }
                }
                foreach (UserProfile up in userSites)
                {
                    if (sharedP.ProfileId == up.Id)
                    {
                        subscribed = true;
                    }
                }
                if (subscribed == false)
                {
                    lbl = new Label();
                    lbl.Text = sharedP.ProfileName;
                    btnAdd = new Button();
                    btnAdd.ID = "btnAdd_" + sharedP.ProfileId;
                    btnAdd.Text = "Abonnieren";
                    btnAdd.CssClass = "btn btn-success";
                    btnAdd.Click += new EventHandler(btnAdd_OnClick);
                    btnAdd.ToolTip = "Fügen Sie das Profil ihrem Account hinzu. Sie finden es anschließend unter ihren Profilen.";
                    tcLbl = new TableCell() { Width = 400 };
                    tcLbl.Controls.Add(lbl);
                    tcLbl.Style.Add("text-align", "center");
                    tcBtnAdd = new TableCell() { Width = 200 };
                    tcBtnAdd.Controls.Add(btnAdd);;
                    tblPublicProfiles.Rows.Add(new TableRow());
                    tblPublicProfiles.Rows[tblPublicProfiles.Rows.Count - 1].Cells.Add(tcLbl);
                    tblPublicProfiles.Rows[tblPublicProfiles.Rows.Count - 1].Cells.Add(tcBtnAdd);
                }
            }
            if(tblPublicProfiles.Rows.Count == 0)
            {
                phNoProfiles.Visible = true;
                phTblProfiles.Visible = false;
            }
        }

        private void btnAdd_OnClick(object sender, EventArgs e)
        {
            Button btnAdd = (Button)sender;
            string profileId = btnAdd.ID.Split('_')[1];
            AddSharedProfile(long.Parse(profileId));
            Response.Redirect("~/PressReview/PublicProfiles");
        }

        /// <summary>
        /// Füge öffentliches Profil zu eigenen Profilen hinzu (abonnieren)
        /// </summary>
        /// <param name="profileId">Id des zu hinzufügenden Profiels</param>
        private void AddSharedProfile(long profileId)
        {
            using (var db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString))
            {
                db.SharedProfiles.InsertOnSubmit(new SharedProfiles()
                {
                    UserId = User.Identity.GetUserId(),
                    ProfileId = profileId
                });

                db.SubmitChanges();
            }
        }

        /// <summary>
        /// Finde alle abbonierten Profile des Nutzers
        /// </summary>
        /// <returns>abbonierte Profile</returns>
        private List<V_SubscribedProfile> FindSubscribedProfiles()
        {
            using (var db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString))
            {
                var readout = from t in db.V_SubscribedProfiles
                              where System.Data.Linq.SqlClient.SqlMethods.Equals(t.UserId, User.Identity.GetUserId())
                              select t;
                List<V_SubscribedProfile> subscribedProfiles = new List<V_SubscribedProfile>();
                subscribedProfiles.AddRange(readout);
                return subscribedProfiles;
            }
        }

        /// <summary>
        /// Findet alle Öffentlichen Profile auf die der bunutzer zugriff hat
        /// </summary>
        /// <returns>Öffentliche Profile</returns>
        private List<V_FindSharedProfile> GetSharedProfiles()
        {
            using (var db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString))
            {
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var user = manager.FindById(User.Identity.GetUserId());
                if (string.IsNullOrEmpty(user.PhoneNumber))
                    return new List<V_FindSharedProfile>(); ;
                string orga = user.PhoneNumber.ToLower();
                var accessableProfiles = from t in db.V_FindSharedProfiles
                                         where System.Data.Linq.SqlClient.SqlMethods.Equals(t.Organization, user.PhoneNumber)
                                         select t;
                List<V_FindSharedProfile> profiles = new List<V_FindSharedProfile>();
                profiles.AddRange(accessableProfiles);
                return profiles;
            }
        }
    }
}