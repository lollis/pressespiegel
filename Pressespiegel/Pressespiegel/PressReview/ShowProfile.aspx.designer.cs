﻿//------------------------------------------------------------------------------
// <automatisch generiert>
//     Dieser Code wurde von einem Tool generiert.
//
//     Änderungen an dieser Datei können fehlerhaftes Verhalten verursachen und gehen verloren, wenn
//     der Code neu generiert wird.
// </automatisch generiert>
//------------------------------------------------------------------------------

namespace Pressespiegel.PressReview {
    
    
    public partial class ShowProfile {
        
        /// <summary>
        /// btnShareProfile-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnShareProfile;
        
        /// <summary>
        /// lblProfileShared-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblProfileShared;
        
        /// <summary>
        /// imageProfile-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image imageProfile;
        
        /// <summary>
        /// labelProfile_Name-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label labelProfile_Name;
        
        /// <summary>
        /// btnEditProfile-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnEditProfile;
        
        /// <summary>
        /// searchResults-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Table searchResults;
        
        /// <summary>
        /// labelNoResults-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label labelNoResults;
        
        /// <summary>
        /// Master-Eigenschaft
        /// </summary>
        /// <remarks>
        /// Automatisch generierte Eigenschaft
        /// </remarks>
        public new Pressespiegel.App_Inside.Main Master {
            get {
                return ((Pressespiegel.App_Inside.Main)(base.Master));
            }
        }
    }
}
