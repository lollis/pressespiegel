﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Pressespiegel.App_Inside
{
    public class ReportWorker
    {
        /// <summary>
        /// Builds a string in the accepted format for the mailservice, based on the given parameters.
        /// It is possible to only get the days or times returned in the string, if only one term should be edit.
        /// </summary>
        /// <param name="mo">bool if monday is selected</param>
        /// <param name="tu">bool if tuesday is selected</param>
        /// <param name="we">bool if wednesday is selected</param>
        /// <param name="th">bool if thursday is selected</param>
        /// <param name="fr">bool if friday is selected</param>
        /// <param name="sa">bool if saturday is selected</param>
        /// <param name="su">bool if sunday is selected</param>
        /// <param name="time1">time to send mail</param>
        /// <param name="time2">time to send mail</param>
        /// <param name="time3">time to send mail</param>
        /// <param name="daysOnly">bool if days should edit</param>
        /// <param name="timeOnly">bool if time should be edit</param>
        /// <returns string="broadcastTime">Includes days and/or times on which the mails should be send in the appropiate format.
        /// In case of a mistake, contains a specific error message</returns>
        public string BuildBroadcastString(bool mo, bool tu, bool we, bool th, bool fr, bool sa, bool su, string time1, string time2, string time3, bool daysOnly, bool timeOnly)
        {
            StringBuilder broadcastTime = new StringBuilder();
            bool daySelected = false;
            broadcastTime.Clear();
            if (daysOnly)//If days should be included in the returned string
            {
                if (mo)
                {
                    broadcastTime.Append("Mo");
                    daySelected = true;
                }
                if (tu)
                {
                    if (daySelected)
                    {
                        broadcastTime.Append(", Di");
                    }
                    else
                    {
                        broadcastTime.Append("Di");
                        daySelected = true;
                    }
                }
                if (we)
                {
                    if (daySelected)
                    {
                        broadcastTime.Append(", Mi");
                    }
                    else
                    {
                        broadcastTime.Append("Mi");
                        daySelected = true;
                    }
                }
                if (th)
                {
                    if (daySelected)
                    {
                        broadcastTime.Append(", Do");
                    }
                    else
                    {
                        broadcastTime.Append("Do");
                        daySelected = true;
                    }
                }
                if (fr)
                {
                    if (daySelected)
                    {
                        broadcastTime.Append(", Fr");
                    }
                    else
                    {
                        broadcastTime.Append("Fr");
                        daySelected = true;
                    }
                }
                if (sa)
                {
                    if (daySelected)
                    {
                        broadcastTime.Append(", Sa");
                    }
                    else
                    {
                        broadcastTime.Append("Sa");
                        daySelected = true;
                    }
                }
                if (su)
                {
                    if (daySelected)
                    {
                        broadcastTime.Append(", So");
                    }
                    else
                    {
                        broadcastTime.Append("So");
                        daySelected = true;
                    }
                }
                if (!daySelected)//If no day is selected returns the string "noDay"
                {
                    broadcastTime = new StringBuilder("noDay");
                    return broadcastTime.ToString();
                }
            }
         
            if(daysOnly && timeOnly)//Appends seperator between days and times to the string if days and times are included
            {
                broadcastTime.Append(" ?");
            }
            bool timeSelected = false;

            if (timeOnly)//If times should be included in the returned string
            {
                if (time1 != null && time1.Length > 0)
                {
                    if (!CheckTimeOnPattern(time1))
                    {
                        broadcastTime = new StringBuilder("wrongFormat");
                        return broadcastTime.ToString();
                    }
                    broadcastTime.Append(time1 + ";");
                    timeSelected = true;
                    if (time2 != null && time2.Length > 0)
                    {
                        if (!CheckTimeOnPattern(time2))
                        {
                            broadcastTime = new StringBuilder("wrongFormat");
                            return broadcastTime.ToString();
                        }
                        if (time1.Equals(time2))//If two times are equal, returns the string "sameTime"
                        {
                            broadcastTime = new StringBuilder("sameTime");
                            return broadcastTime.ToString();
                        }
                    }
                    if (time3 != null && time3.Length > 0)
                    {
                        if (!CheckTimeOnPattern(time3))
                        {
                            broadcastTime = new StringBuilder("wrongFormat");
                            return broadcastTime.ToString();
                        }
                        if (time1.Equals(time3))
                        {
                            broadcastTime = new StringBuilder("sameTime");
                            return broadcastTime.ToString();
                        }
                    }
                }
                if (time2 != null && time2.Length > 0)
                {
                    if (!CheckTimeOnPattern(time2))
                    {
                        broadcastTime = new StringBuilder("wrongFormat");
                        return broadcastTime.ToString();
                    }
                    broadcastTime.Append(time2 + ";");
                    timeSelected = true;
                    if (time3 != null && time3.Length > 0)
                    {
                        if (!CheckTimeOnPattern(time3))
                        {
                            broadcastTime = new StringBuilder("wrongFormat");
                            return broadcastTime.ToString();
                        }
                        if (time2.Equals(time3))
                        {
                            broadcastTime = new StringBuilder("sameTime");
                            return broadcastTime.ToString();
                        }
                    }
                }
                if (time3 != null && time3.Length > 0)
                {
                    if (!CheckTimeOnPattern(time3))
                    {
                        broadcastTime = new StringBuilder("wrongFormat");
                        return broadcastTime.ToString();
                    }
                    broadcastTime.Append(time3 + ";");
                    timeSelected = true;
                }
                if (!timeSelected)//If no time is entered returns the string "noTime"
                {
                    broadcastTime = new StringBuilder("noTime");
                    return broadcastTime.ToString();
                }
            }
            
            return broadcastTime.ToString();
        }

        /// <summary>
        /// Checks the entered time from the user on fixed regular expressions to avoid errors.
        /// </summary>
        /// <param name="newTime">Entered string from the user</param>
        /// <returns bool>returns true if the entered string matches on the reg expression,
        /// returns false if not</returns>
        public bool CheckTimeOnPattern(string newTime)
        {
            String pattern = @"^(0|1)\d\:[0-5]\d$|^2[0-3]\:[0-5]\d$";
            Regex reg = new Regex(pattern);
            if (!reg.IsMatch(newTime))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// checks if the given string matches on the given pattern
        /// </summary>
        /// <param name="value">entered string from the user</param>
        /// <returns>treu if value matches, false if not</returns>
        public bool CheckOnPattern(string value)
        {
            string pattern = "^[a-zA-Z0-9äöüÄÖÜß\\s]{1,100}$";
            Regex reg = new Regex(pattern);
            if (reg.IsMatch(value))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}