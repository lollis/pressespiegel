﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pressespiegel.DataProvider;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using RssService.SqlLinq;

namespace Pressespiegel.App_Inside
{
    public partial class Main : System.Web.UI.MasterPage
    {
        private UserProfile currentProfile;
        private EmailOrder currentOrder;
        private List<EmailOrder> emailOrders = new List<EmailOrder>();
        private List<UserProfile> userSites = new List<UserProfile>();
        protected void Page_Load(object sender, EventArgs e)
        {

            DataProvider.Provider provider = new DataProvider.Provider(HttpContext.Current.User.Identity.GetUserId());
            userSites = provider.GetUserSites();
            emailOrders = provider.GetEmailOrders();
            LinkButton lb;
            Panel panel;
            if (userSites != null)
            {
                foreach (UserProfile up in userSites)
                {
                    lb = new LinkButton();
                    panel = new Panel();
                    lb.Text = up.Name;
                    lb.ID = "profile_" + up.Id;
                    lb.Click += new EventHandler(LinkButton_OnClick);
                    panel.Controls.Add(lb);
                    placeHolderUserSites.Controls.Add(panel);
                }
            }
            lb = new LinkButton();
            panel = new Panel();
            lb.Text = "Neues Suchprofil";
            lb.CssClass = "staticLink";
            lb.Click += new EventHandler(NewProfile_OnClick);
            panel.Controls.Add(lb);
            placeHolderUserSites.Controls.Add(panel);

            lb = new LinkButton();
            panel = new Panel();
            lb.CssClass = "staticLink";
            lb.Text = "Öffentliche Profile";
            lb.Click += new EventHandler(PublicProfiles_OnClick);
            panel.Controls.Add(lb);
            placeHolderUserSites.Controls.Add(panel);

            if (emailOrders != null)
            {
                foreach(EmailOrder eo in emailOrders)
                {
                    lb = new LinkButton();
                    panel = new Panel();
                    foreach(UserProfile up in userSites)
                    {
                        if (eo.UserProfileId.Equals(up.Id))
                        {
                            lb.Text = up.Name;
                            lb.ID = "report_" + eo.Id;
                            lb.Click += new EventHandler(LinkButton2_OnClick);
                        }
                    }
                    panel.Controls.Add(lb);
                    placeHolderReports.Controls.Add(panel);
                }
            }
            lb = new LinkButton();
            panel = new Panel();
            lb.CssClass = "staticLink";
            lb.Text = "Neuer Report";
            lb.Click += new EventHandler(NewReport_OnClick);
            panel.Controls.Add(lb);
            placeHolderReports.Controls.Add(panel);
        }

        public static explicit operator Main(Page v)
        {
            throw new NotImplementedException();
        }

        private void LinkButton2_OnClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            foreach(EmailOrder eo in emailOrders)
            {
                if (long.Parse(lb.ID.Split('_')[1]).Equals(eo.Id))
                {
                    currentOrder = eo;
                    foreach(UserProfile up in userSites)
                    {
                        if (eo.UserProfileId.Equals(up.Id))
                        {
                            currentProfile = up;
                        }
                    }
                    break;
                }
            }
            Session["profileView"] = currentProfile;
            Session["orderView"] = currentOrder;
            Session["firstCall"] = true;
            Response.Redirect("~/PressReview/EditReport");
        }

        private void LinkButton_OnClick(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            foreach (UserProfile up in userSites)
            {
                if (long.Parse(lb.ID.Split('_')[1]).Equals(up.Id))
                {
                    currentProfile = up;
                    break;
                }
            }
            Session["profileView"] = currentProfile;
            Response.Redirect("~/PressReview/ShowProfile");
        }

        private void PublicProfiles_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PressReview/PublicProfiles");
        }

        private void NewProfile_OnClick(object sender, EventArgs e)
        {
            Session["time1"] = false;
            Session["time2"] = false;
            Session["time3"] = false;
            Session["firstCall"] = true;
            Response.Redirect("~/PressReview/AddProfile");
        }

        private void NewReport_OnClick(object sender, EventArgs e)
        {
            Session["time1"] = false;
            Session["time2"] = false;
            Session["time3"] = false;
            Session["firstCall"] = true;
            Response.Redirect("~/PressReview/CreateReport");
        }
    }
}