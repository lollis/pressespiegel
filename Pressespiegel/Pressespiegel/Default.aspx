﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Pressespiegel._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Pressespiegel</h1>
        <p><a href="http://141.72.233.73/basic/web/index.php" class="btn btn-primary btn-lg">Dokumentation &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Unser Team</h2>
            <p>
               Robert Meissner<br />
                Giuseppe Sansone<br />
                Raphael Nonnenmann<br />
                Finn Bökenkamp<br />
                Fabian Brecht<br />
                Jan-Ruben Schmid<br />
            </p>
        </div>
    </div>

</asp:Content>
