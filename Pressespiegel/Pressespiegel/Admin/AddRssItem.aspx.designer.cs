﻿//------------------------------------------------------------------------------
// <automatisch generiert>
//     Dieser Code wurde von einem Tool generiert.
//
//     Änderungen an dieser Datei können fehlerhaftes Verhalten verursachen und gehen verloren, wenn
//     der Code neu generiert wird.
// </automatisch generiert>
//------------------------------------------------------------------------------

namespace Pressespiegel.App_Inside {
    
    
    public partial class AddRssItem {
        
        /// <summary>
        /// Content-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder Content;
        
        /// <summary>
        /// txtProvName-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtProvName;
        
        /// <summary>
        /// txtRssLink-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtRssLink;
        
        /// <summary>
        /// lblErrorLogo-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblErrorLogo;
        
        /// <summary>
        /// fileRssLogo-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.FileUpload fileRssLogo;
        
        /// <summary>
        /// txtRepUrl-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtRepUrl;
        
        /// <summary>
        /// txtXpathContent-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtXpathContent;
        
        /// <summary>
        /// btnGetContent-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnGetContent;
        
        /// <summary>
        /// lblXPathContent-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox lblXPathContent;
        
        /// <summary>
        /// txtXPathPicture-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtXPathPicture;
        
        /// <summary>
        /// btnGetPicture-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnGetPicture;
        
        /// <summary>
        /// lblXPathPicture-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox lblXPathPicture;
        
        /// <summary>
        /// btnAddFeed-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnAddFeed;
        
        /// <summary>
        /// Success-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder Success;
        
        /// <summary>
        /// btnOK-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnOK;
    }
}
