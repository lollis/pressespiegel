﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pressespiegel.Admin
{
    public partial class UserOverview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Global.IsAdmin(System.Web.HttpContext.Current.User, Response);
            if (string.IsNullOrEmpty(SqlDsUsers.ConnectionString) || !SqlDsUsers.ConnectionString.Equals(RssService.MainWindow.ConnectionString))
                SqlDsUsers.ConnectionString = RssService.MainWindow.ConnectionString;
        }
    }
}