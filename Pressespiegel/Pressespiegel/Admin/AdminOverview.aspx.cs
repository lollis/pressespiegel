﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pressespiegel.Admin
{
    public partial class Admins : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Global.IsAdmin(System.Web.HttpContext.Current.User, Response);
            if (string.IsNullOrEmpty(SqlDsAddAdmin.ConnectionString) || !SqlDsAddAdmin.ConnectionString.Equals(RssService.MainWindow.ConnectionString))
                SqlDsAddAdmin.ConnectionString = RssService.MainWindow.ConnectionString;
        }

        /// <summary>
        /// Füge buttons zur view hinzu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvOverview_DataBound(object sender, EventArgs e)
        {
            GridView gv = (GridView)sender;
            foreach (GridViewRow row in gv.Rows)
            {
                Label lblIsUser = row.FindControl("lblIsUser") as Label;
                if (lblIsUser != null)
                {
                    if (lblIsUser.Text.Equals("1"))
                    {
                        Button btnAdmin = row.FindControl("btnAddAdmin") as Button;
                        if (btnAdmin != null)
                        {
                            btnAdmin.Visible = true;

                        }
                    }
                    else
                    {
                        Button btnUser = row.FindControl("btnRemoveAdmin") as Button;
                        if (btnUser != null)
                            btnUser.Visible = true;
                    }

                }
            }
        }


        /// <summary>
        /// Gebe User Admin rechter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddAdmin_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow gc = (GridViewRow)btn.NamingContainer;
            string userId = ((Label)gc.FindControl("lblUserId")).Text;
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            manager.AddToRole(userId, "Admin");
            gvOverview.DataBind();
        }

        /// <summary>
        /// Nehme Administrator administrative Rechte
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemoveAdmin_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow gc = (GridViewRow)btn.NamingContainer;
            string userId = ((Label)gc.FindControl("lblUserId")).Text;
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            manager.RemoveFromRole(userId, "Admin");
            gvOverview.DataBind();
        }
    }
}