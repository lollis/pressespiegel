﻿using Pressespiegel.PressReview;
using RssService.SqlLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pressespiegel.Admin
{
    public partial class EditRssItems : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Global.IsAdmin(System.Web.HttpContext.Current.User, Response);
            if (string.IsNullOrEmpty(SqlDsRemoveMSP.ConnectionString) || !SqlDsRemoveMSP.ConnectionString.Equals(RssService.MainWindow.ConnectionString))
                SqlDsRemoveMSP.ConnectionString = RssService.MainWindow.ConnectionString;
            txtFailure.Visible = false;
        }


        protected void GvRemoveMSP_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            SqlDsRemoveMSP.Delete();
        }

        protected void GvRemoveMSP_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow gvr = GvRemoveMSP.Rows[e.RowIndex];
            FileUpload upload = (FileUpload)gvr.FindControl("fileProfileLogo");
            long id = long.Parse(((Label)gvr.FindControl("lblEditId")).Text);
            int maxFileLength = 1024 * 80; //80 kbyte = 81920 Byte
            if (upload.HasFile)
            {
                if (upload.PostedFile.ContentLength < maxFileLength)
                {
                    if (AddProfile.isImage(upload.PostedFile))
                    {
                        using (UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString))
                        {
                            Provider prov = (from t in db.Providers
                                             where t.Id == id
                                             select t).FirstOrDefault();


                            prov.Id = long.Parse(((Label)gvr.FindControl("lblEditId")).Text);
                            prov.Name = ((TextBox)gvr.FindControl("txtName")).Text;
                            prov.Link = ((TextBox)gvr.FindControl("txtLink")).Text;
                            prov.Logo = Convert.ToBase64String(upload.FileBytes);
                            prov.XPathContent = ((TextBox)gvr.FindControl("txtXPathContent")).Text;
                            prov.XPathPicture = ((TextBox)gvr.FindControl("txtXPathPicture")).Text;
                            db.SubmitChanges();
                        }
                        return;
                    }
                    else
                    {
                        txtFailure.Text = "Das Dateiformat für das Profillogo wird nicht unterstützt.\n" +
                            "Unterstützte Formate: jpg, jpeg, pjpeg, png, x-png, gif.";
                        return;
                    }
                }
                else
                {
                    txtFailure.Text = "Das Logo wurde nicht aktualisiert, es darf maximal eine Größe von 80 KByte haben.";
                    txtFailure.Visible = true;
                }
            }
            SqlDsRemoveMSP.Update();
        }
    }
}