﻿<%@ Page Title="Neuen Mediendienstleister hinzufügen" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddRssItem.aspx.cs" Inherits="Pressespiegel.App_Inside.AddRssItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <br />
    <asp:PlaceHolder runat="server" ID="Content" Visible="true">
        <h3>Rss-Feed</h3>
        <asp:TextBox CssClass="form-control" placeholder="Provider Name" runat="server" ID="txtProvName"></asp:TextBox>
        <asp:TextBox CssClass="form-control" placeholder="RSS URL" runat="server" ID="txtRssLink" Width="700"></asp:TextBox>
        <br />
        <br />
        <asp:Label runat="server" ID="lblErrorLogo" CssClass="text-danger" Visible="false"/>
        <p>
            <asp:Label runat="server" Text="Logo: ">
                <br />
                <asp:FileUpload runat="server" ID="fileRssLogo" CssClass="btn btn-default" />
            </asp:Label>
        </p>
        <br />
        <br />
        <br />
        <h3>XPath</h3>
        <asp:Label Text="Bitte eine Representative URL zum RSS-Feed eingeben, hieraus werden die XPaths gelesen" runat="server"></asp:Label>
        <br />
        <asp:TextBox CssClass="form-control" ID="txtRepUrl" placeholder="Representative URL" runat="server"></asp:TextBox>
        <h4>Content</h4>
        <asp:TextBox CssClass="form-control" placeholder="X-Path Content" runat="server" ID="txtXpathContent"></asp:TextBox>
        <asp:Button ID="btnGetContent" runat="server" Text="X-Path von Content ausgeben" CssClass="btn btn-default" OnClick="btnGetContent_Click" />
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:TextBox ReadOnly="true" ID="lblXPathContent" TextMode="multiline" Style="overflow: auto; width: 800px; height: 300px;" runat="server"></asp:TextBox>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnGetContent" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
        <h4>Picture URL</h4>
        <asp:TextBox CssClass="form-control" placeholder="X-Path Picture" runat="server" ID="txtXPathPicture"></asp:TextBox>
        <asp:Button ID="btnGetPicture" runat="server" Text="X-Path von Bild-URL ausgeben" CssClass="btn btn-default" OnClick="btnGetPicture_Click" />
        <br />
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:TextBox ReadOnly="true" ID="lblXPathPicture" TextMode="multiline" Style="overflow: auto; width: 800px; height: 50px;" runat="server"></asp:TextBox>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnGetPicture" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
        <br />
        <br />
        <asp:Button runat="server" ID="btnAddFeed" Text="Add" CssClass="btn btn-default" OnClick="btnAddFeed_Click" />
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="Success" Visible="false">
        <asp:Literal runat="server" Text="Feed hinzugefügt!" />
        <asp:Button runat="server" ID="btnOK" Text="OK" CssClass="btn btn-default" OnClick="btnOK_Click" />
    </asp:PlaceHolder>
</asp:Content>
