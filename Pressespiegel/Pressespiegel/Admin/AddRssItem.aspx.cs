﻿//using SendGrid.SmtpApi;
using RssService;
using RssService.SqlLinq;
using System;

namespace Pressespiegel.App_Inside
{
    public partial class AddRssItem : System.Web.UI.Page
    {
        public object Conversations { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Global.IsAdmin(System.Web.HttpContext.Current.User, Response);
            Success.Visible = false;
            Content.Visible = true;
        }

        /// <summary>
        /// Füge RssFeed hinzu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddFeed_Click(object sender, EventArgs e)
        {
            int maxFileLength = 1024 * 80; //80 kbyte = 81920 Byte
            if (fileRssLogo.HasFile)
            {
                if(fileRssLogo.PostedFile.ContentLength > maxFileLength)
                {
                    lblErrorLogo.Visible = true;
                    lblErrorLogo.Text = "Das Logo darf maximal eine Größe von 80 KByte haben.";
                }
                else
                {
                    UserProfileViewDataContext db = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString);
                    Provider prov = new Provider
                    {
                        Name = txtProvName.Text,
                        Link = txtRssLink.Text,
                        Logo = Convert.ToBase64String(fileRssLogo.FileBytes),
                        XPathContent = txtXpathContent.Text,
                        XPathPicture = txtXPathPicture.Text
                    };
                    db.Providers.InsertOnSubmit(prov);
                    db.SubmitChanges();
                    Success.Visible = true;
                    Content.Visible = false;
                    txtProvName.Text = String.Empty;
                    txtRssLink.Text = String.Empty;
                    fileRssLogo.Dispose();
                }
            }
            else
            {
                lblErrorLogo.Visible = true;
                lblErrorLogo.Text = "Bitte ein Logo für den Mediendienstleister auswählen.";
            }   
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            Content.Visible = true;
            Success.Visible = false;
        }

        protected void btnGetPicture_Click(object sender, EventArgs e)
        {
            WebCrawler crawler = new WebCrawler();
            try
            {
                lblXPathPicture.Text = crawler.GetPictureFromRssFeed(txtRepUrl.Text, txtXPathPicture.Text.Split('§'));
            }
            catch (Exception ex)
            {
                lblXPathPicture.Text = ex.Message;
            }
        }

        protected void btnGetContent_Click(object sender, EventArgs e)
        {
            WebCrawler crawler = new WebCrawler();
            try
            {
                lblXPathContent.Text = crawler.GetContentFromRssFeed(txtRepUrl.Text, txtXpathContent.Text);
            }
            catch (Exception ex)
            {
                lblXPathContent.Text = ex.Message;
            }
        }
    }
}