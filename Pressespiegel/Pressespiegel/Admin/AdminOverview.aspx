﻿<%@ Page Title="Admin Overview" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminOverview.aspx.cs" Inherits="Pressespiegel.Admin.Admins" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%:Title %></h2>
    <h4>User needs log in again that changes take effect</h4>
    <asp:GridView ID="gvOverview" runat="server" AllowPaging="True" OnDataBound="gvOverview_DataBound" AllowSorting="True" CellPadding="4" DataSourceID="SqlDsAddAdmin" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:TemplateField HeaderText="Edit Rights">
                <ItemTemplate>
                    <asp:Label ID="lblIsUser" runat="server" Text='<%# Bind("IsUser") %>' Style="visibility: hidden;"></asp:Label>
                    <asp:Button Text="Add Admin" ID="btnAddAdmin" runat="server" Visible="false" OnClick="btnAddAdmin_Click" />
                    <asp:Button Text="Remove Admin" ID="btnRemoveAdmin" runat="server" Visible="false" OnClick="btnRemoveAdmin_Click" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="UserName" HeaderText="UserName" ReadOnly="True" SortExpression="UserName" />
            <asp:BoundField DataField="Email" HeaderText="Email" ReadOnly="True" SortExpression="Email" />
            <asp:TemplateField HeaderText="User Id">
                <ItemTemplate>
                    <asp:Label ID="lblUserId" runat="server" Text='<%# Bind("ID") %>' Font-Size="XX-Small" Style="visibility: visible;"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDsAddAdmin" runat="server" ConnectionString="Data Source=.\sqlexpress;Initial Catalog=Pressespiegel_App;Integrated Security=True" ProviderName="System.Data.SqlClient" SelectCommand="SELECT COUNT(*) AS IsUser, AspNetUsers.ID,AspNetUsers.UserName,  AspNetUsers.Email FROM AspNetRoles INNER JOIN AspNetUserRoles ON AspNetRoles.Id = AspNetUserRoles.RoleId INNER JOIN AspNetUsers ON AspNetUserRoles.UserId = AspNetUsers.Id GROUP BY AspNetUsers.ID, AspNetUsers.UserName, AspNetUsers.Email HAVING (COUNT(*) = 1) 
UNION ALL
SELECT COUNT(*) AS IsUser,AspNetUsers.ID, AspNetUsers.UserName,  AspNetUsers.Email FROM AspNetRoles INNER JOIN AspNetUserRoles ON AspNetRoles.Id = AspNetUserRoles.RoleId INNER JOIN AspNetUsers ON AspNetUserRoles.UserId = AspNetUsers.Id GROUP BY AspNetUsers.ID, AspNetUsers.UserName, AspNetUsers.Email HAVING (COUNT(*) = 2) "></asp:SqlDataSource>

</asp:Content>
