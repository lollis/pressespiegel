﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Pressespiegel.Models;
using RssService.SqlLinq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.UI;

namespace Pressespiegel
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //CreateDataBase();

        }
        /// <summary>
        /// Checks if the User is logged in, if not a redirect to the login page follows
        /// </summary>
        /// <param name="user">System.Web.HttpContext.Current.User</param>
        /// <param name="Response">Response, required because methode is static</param>
        static internal void IsLoggedIn(IPrincipal user, HttpResponse Response)
        {
            if (user == null || !user.Identity.IsAuthenticated)
                Response.Redirect("~/Account/Login");
        }

        /// <summary>
        /// Checks if the User is admin and logged in, if not a redirect to the NoAdmin page follows
        /// </summary>
        /// <param name="user">System.Web.HttpContext.Current.User</param>
        /// <param name="Response">Response, required because methode is static</param>
        static internal void IsAdmin(IPrincipal user, HttpResponse Response)
        {
            if (user == null || !user.Identity.IsAuthenticated || !user.IsInRole("Admin"))
            {
                Response.Redirect("~/Account/NoAdmin");
            }
        }

        /// <summary>
        /// Versucht alle tabellen, views, roles der Datebank zu erstellen
        /// </summary>
        public static void CreateDataBase()
        {
            try
            {
                new MsSqlDataBase().CreateMsSqlDataBase();
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Finde Control von oberfläche
        /// </summary>
        /// <param name="root">Control, in der die zu suchende enthalten sein muss</param>
        /// <param name="id">ID der zu findenden Control</param>
        /// <returns></returns>
        internal static Control FindControlRecursive(Control root, string id)
        {
            if (root.ID == id)
                return root;
            return root.Controls.Cast<Control>()
                .Select(c => FindControlRecursive(c, id))
                .FirstOrDefault(c => c != null);
        }
        /// <summary>
        /// Erstellt Die user Admin und Van Hoof
        /// </summary>
        /// <param name="Context"></param>
        static internal void CreateDefaultUser(HttpContext Context)
        {
            try
            {
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
                var user = new ApplicationUser() { UserName = "Admin", Email = ConfigurationManager.AppSettings["emailServiceUserName"], EmailConfirmed = true, PhoneNumber = "Pressespiegel" };
                IdentityResult result = manager.Create(user, "@" + ConfigurationManager.AppSettings["emailServicePassword"]);
                if (result.Succeeded)
                {
                    manager.AddToRole(user.Id, "User");
                    manager.AddToRole(user.Id, "Admin");
                }
                var vanHoofAdmin = new ApplicationUser()
                {
                    UserName = "Van Hoof",
                    Email = "vanHoof@noMail.com",
                    EmailConfirmed = true
                };
                IdentityResult hoofRes = manager.Create(vanHoofAdmin, "Presespiegel@Team4");
                if (hoofRes.Succeeded)
                {
                    manager.AddToRole(vanHoofAdmin.Id, "User");
                    manager.AddToRole(vanHoofAdmin.Id, "Admin");
                }
            }
            catch (Exception ex)
            {

            }
        }
    }

    internal class MsSqlDataBase
    {
        /// <summary>
        /// Erstellt alle tabellen, rollen und views der Datenbank
        /// </summary>
        internal void CreateMsSqlDataBase()
        {
            CreateAllTables();
            CreateAllViews();
            CreateUserRoles();
        }

        /// <summary>
        /// Benutzerrollen user und admin werden hinzugefügt, incl default admin
        /// </summary>
        private void CreateUserRoles()
        {
            SqlConnection con = new SqlConnection(RssService.MainWindow.ConnectionString);
            List<SqlCommand> commands = new List<SqlCommand>();
            commands.Add(new SqlCommand(CreateUserRole(1, "User")));
            commands.Add(new SqlCommand(CreateUserRole(2, "Admin")));
            foreach (SqlCommand cmd in commands)
            {
                cmd.Connection = con;
                cmd.Connection.Open();
                try
                {
                    cmd.ExecuteReader();

                }
                catch (Exception ex)
                {
                    // do nothing
                }
                cmd.Connection.Close();
            }
        }
        /// <summary>
        /// Erstelt benutzerrollen
        /// </summary>
        /// <param name="id">ID, muss eindeutig sein</param>
        /// <param name="roleName">Rollenname</param>
        /// <returns></returns>
        private string CreateUserRole(int id, string roleName)
        {
            return " IF NOT EXISTS(SELECT * FROM AspNetRoles WHERE Name = '" + roleName + "')" + Environment.NewLine +
             "BEGIN INSERT INTO AspNetRoles(Id, Name) VALUES('" + id + "', '" + roleName + "') END";
        }



        private void AddAllForeignKeys()
        {

        }
        /// <summary>
        /// Erstellt alle views der Datenbank
        /// </summary>
        private void CreateAllViews()
        {
            SqlConnection con = new SqlConnection(RssService.MainWindow.ConnectionString);
            List<SqlCommand> commands = new List<SqlCommand>();
            commands.Add(new SqlCommand(CreateVEmailOrderTag()));
            commands.Add(new SqlCommand(CreateVEmailOrderPdf()));
            commands.Add(new SqlCommand(CreateVSubscribedProfiles()));
            commands.Add(new SqlCommand(CreateVFindSharedProfiles()));
            foreach (SqlCommand cmd in commands)
            {
                cmd.Connection = con;
                cmd.Connection.Open();
                try
                {
                    cmd.ExecuteReader();

                }
                catch (Exception ex)
                {
                    // do nothing
                }
                cmd.Connection.Close();
            }
        }


        #region Views
        /// <summary>
        /// Erstellt sql command zum erstellen der View EmailOrderPdf
        /// </summary>
        /// <returns>Sql Command</returns>
        private string CreateVEmailOrderPdf()
        {
            string viewName = "V_EmailOrderPdf";
            string cmd = "CREATE VIEW[dbo].[" + viewName + "] " + Environment.NewLine +
            "AS SELECT UserProfile.Id AS ProfileId,  AspNetUsers.UserName AS UserName, AspNetUsers.Email AS Email, UserProfile.Name AS ProfileName, EmailOrder.Id AS EmailOrderId" + Environment.NewLine +
            "From EmailOrder,UserProfile, AspNetUsers" + Environment.NewLine +
            "WHERE EmailOrder.UserProfileId=UserProfile.Id AND EmailOrder.UserId=AspNetUsers.Id ";
            return cmd;
        }

        private string CreateVSubscribedProfiles()
        {
            string cmd = " CREATE VIEW V_SubscribedProfiles AS SELECT AspNetUsers.PhoneNumber AS Organization, AspNetUsers.Id AS UserId, UserProfile.Name AS ProfileName, UserProfile.Id AS ProfileId" + Environment.NewLine +
            "FROM AspNetUsers, UserProfile, SharedProfiles" + Environment.NewLine +
            "WHERE AspNetUsers.Id = SharedProfiles.UserId AND SharedProfiles.ProfileId = UserProfile.Id AND UserProfile.IsPublic = 1";
            return cmd;
        }

        private string CreateVFindSharedProfiles()
        {
            string cmd = "Create View V_FindSharedProfiles AS SELECT DISTINCT dbo.AspNetUsers.PhoneNumber AS Organization, dbo.UserProfile.Name AS ProfileName, dbo.UserProfile.Id AS ProfileId" + Environment.NewLine +
                            "FROM dbo.AspNetUsers CROSS JOIN dbo.UserProfile WHERE (UserProfile.UserId=AspNetUsers.Id) AND (dbo.UserProfile.IsPublic = 1) AND(dbo.AspNetUsers.PhoneNumber IS NOT NULL)";
            return cmd;
        }
        /// <summary>
        /// Erstellt sql command zum erstellen der View VEmailOrderTag
        /// </summary>
        /// <returns>Sql Command</returns>
        private string CreateVEmailOrderTag()
        {
            string viewName = "V_ProfileIndizes";
            string cmd = "CREATE VIEW[dbo].[" + viewName + "]" + Environment.NewLine +
            "AS SELECT UserProfile.Id AS UserProfileId, Tag.TagName AS TagName, UserProfile.Elasticsearch_Indizes AS Elasticsearch_Indizes" + Environment.NewLine +
            "From Tag, UserProfileTag, UserProfile" + Environment.NewLine +
            "WHERE Tag.Id = UserProfileTag.TagId AND UserProfileTag.UserProfileId = UserProfile.Id";
            return cmd;
        }


        #endregion

        /// <summary>
        /// Erstellt alle benötigten tabellen
        /// </summary>
        public void CreateAllTables()
        {
            CreateTable(typeof(UserProfile));
            CreateTable(typeof(Provider));
            CreateTable(typeof(UserProfileProvider));
            CreateTable(typeof(Tag));
            CreateTable(typeof(UserProfileTag));
            CreateTable(typeof(EmailOrder));
            CreateTable(typeof(Archiv));
            CreateTable(typeof(SharedProfiles));
        }
        /// <summary>
        /// Erstelle Table mit SqlLinq Tabelle
        /// </summary>
        /// <param name="linqTableClass"></param>
        public void CreateTable(Type linqTableClass)
        {
            try
            {
                using (var tempDc = new UserProfileViewDataContext(RssService.MainWindow.ConnectionString))
                {
                    var metaTable = tempDc.Mapping.GetTable(linqTableClass);
                    var typeName = "System.Data.Linq.SqlClient.SqlBuilder";
                    var type = typeof(DataContext).Assembly.GetType(typeName);
                    var bf = BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.InvokeMethod;
                    var sql = type.InvokeMember("GetCreateTableCommand", bf, null, null, new[] { metaTable });
                    var sqlAsString = sql.ToString();
                    tempDc.ExecuteCommand(sqlAsString);
                }
            }
            catch (SqlException ex)
            {

            }
        }
    }
}